#!/bin/bash
brew update
flutter upgrade
flutter pub upgrade
(cd android ; bundle update)
(cd ios ; pod update ; bundle update ;  bundle exec fastlane snapshot update)
