// Flutter
import 'package:flutter/widgets.dart';
// MVP
import 'package:deiser_app/mvp/mvpPresenter.dart';
import 'package:deiser_app/mvp/mvpState.dart';

abstract class MVPView extends StatefulWidget {
  // Attributes
  final MVPPresenter presenter;

  // Constructors
  const MVPView({this.presenter, Key key}) : super(key: key);

  // Getters
  Widget get widget => this;
  BuildContext get context => presenter.state.context ?? createState().context;
  bool get mounted => presenter.state.mounted ?? createState().mounted;

  // Methods
  @override
  State<StatefulWidget> createState() {
    final MVPState state = MVPState(this, presenter);

    presenter?.state = state;
    return state;
  }

  Widget build(BuildContext context);
}
