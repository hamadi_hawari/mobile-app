// Flutter
import 'package:flutter/widgets.dart';
// MVP
import 'package:deiser_app/mvp/mvpState.dart';
// Firebase
import 'package:deiser_app/myFirebase.dart';
//import 'package:firebase_analytics/firebase_analytics.dart';
//import 'package:firebase_analytics/observer.dart';

class MVPPresenter {
  // Attributes
  MVPState state;
  final String _name;
  // Singleton
  // final FirebaseAnalytics _analytics = MyFirebase.analytics;
  // final FirebaseAnalyticsObserver _observer = MyFirebase.observer;

  // Constructors
  MVPPresenter(this._name);

  // Getters
  Widget get widget => state?.view;
  BuildContext get context => state?.context;
  bool get mounted => state?.mounted;
  //FirebaseAnalytics get analytics => _analytics;
  //FirebaseAnalyticsObserver get observer => _observer;
  String get name => _name;

  void initState() {
    // _analytics?.setCurrentScreen(screenName: _name);
  }

  void dispose() {}

  void update() => setState(() {});

  void setState(VoidCallback callback) => state?.reState(callback);

  void refresh() => state?.reState(() {});
}
