// Flutter
import 'package:flutter/widgets.dart';
// MVP
import 'package:deiser_app/mvp/mvpView.dart';
import 'package:deiser_app/mvp/mvpPresenter.dart';

class MVPState extends State<MVPView>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  // Attributes
  final MVPView view;
  final MVPPresenter _presenter;

  // Constructors
  MVPState(
    this.view,
    this._presenter,
  );

  // Methods
  @override
  void initState() {
    super.initState();
    _presenter?.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void deactivate() {
    WidgetsBinding.instance.addObserver(this);
    super.deactivate();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _presenter?.dispose();
    WidgetsBinding.instance.addObserver(this);
    super.dispose();
  }

  void reState(VoidCallback callback) {
    setState(callback);
  }

  @override
  Widget build(BuildContext context) {
    return view.build(context);
  }
}
