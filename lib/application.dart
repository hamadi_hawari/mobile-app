import 'dart:ui';

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:deiser_app/redux/reduxModel.dart';
import 'package:redux/redux.dart';
import 'package:redux_persist/redux_persist.dart';

class Application {
  static Router router;
  static Persistor<ReduxModel> persisted;
  static Store<ReduxModel> store;
  static VoidCallback localeChanged;
  static Iterable<Locale> supportedLocales = <Locale>[
    const Locale('en', 'US'), // English
    const Locale('de', 'DE'), // german
  ];
}
