import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/pages/adminDashboard/lastActivities.dart';
import 'package:deiser_app/pages/fillForm/Stepper.dart';
import 'package:deiser_app/pages/fillForm/fillFormView.dart';
import 'package:deiser_app/pages/fillForm/stepperView.dart';
import 'package:deiser_app/pages/formManager/checksubmit.dart';
import 'package:deiser_app/pages/formManager/sumbitsView.dart';
import 'package:deiser_app/services/formServices.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pdf/widgets.dart' as pw;

import 'editformView.dart';

String user_role = 'admin';
List<String> submittedForms = [];
List<String> nameDocs = [];
List<String> nameDocsuser = [];
Map<String, dynamic> projectForms = {};

class FormCell extends StatefulWidget {
  final project;
  final userRole;
  const FormCell({Key key, this.project, this.userRole}) : super(key: key);
  @override
  _FormCellState createState() => _FormCellState(project, userRole);
}

Future<void> getrole() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  user_role = prefs.getString("role");
}

Future<void> getsubmittedForms() async {
  //print('here now');
  submittedForms.clear();
  QuerySnapshot querySnapshot =
      await Firestore.instance.collection("FormValues").getDocuments();
  var list = querySnapshot.documents;
  //print('maybe here now');
  for (var item in list) {
    submittedForms.add(item.documentID);
  }
  //print('left now');
}

class _FormCellState extends State<FormCell> {
  final DocumentSnapshot project;
  final String userRole;
  _FormCellState(this.project, this.userRole);
  Future<void> getsubmittedForms() async {
    //print('here now');
    submittedForms.clear();
    QuerySnapshot querySnapshot =
        await Firestore.instance.collection("FormValues").getDocuments();
    var list = querySnapshot.documents;
    //print('maybe here now');
    for (var item in list) {
      submittedForms.add(item.documentID);
    }
    //print('left now');
  }

  int tabs = 2;
  StreamSubscription nameSub;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    projectForms = project.data['forms'];
    getrole();
    if (userRole == 'not used') {
      setState(() {
        tabs = 3;
      });
    }
    nameDocs = [];
    nameSub =
        Firestore.instance.collection('FormValues').snapshots().listen((data) {
      //print('listener Fired at line 39 main.dart');
      data.documentChanges.forEach((x) {
        setState(() {
          nameDocs.add(x.document.documentID);
          //   print(x.document.data['data']);
        });
      });
    });
    //getsubmittedForms();

    getrole();
  }

  @override
  Widget build(BuildContext context) {
    final ValueNotifier<String> name = ValueNotifier<String>("");

    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(65.0), // here the desired height
            child: AppBar(
              elevation: 2.0,
              backgroundColor: Color(0xffec6500),
              title: Text('Formulare',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize: 20.0)),
              actions: <Widget>[
                Align(
                  alignment: Alignment.centerRight,
                  child: Material(
                    color: Color(0xffec6500),
                    child: IconButton(
                      tooltip: 'Speichern',
                      icon: new Icon(Icons.save_alt_outlined),
                      color: Colors.white,
                      onPressed: () => _createPDF(project.documentID),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                  ),
                )
              ],
            )),
        body: StaggeredGridView.count(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          crossAxisCount: 2,
          crossAxisSpacing: 12.0,
          mainAxisSpacing: 12.0,
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          children: <Widget>[
            DefaultTabController(
              length: tabs,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: TabBar(
                        indicatorColor: Colors.orange,
                        labelColor: Colors.black,
                        labelStyle: TextStyle(fontSize: 15),
                        tabs: [
                          Tab(text: "Offen"),
                          Tab(text: "Eingereicht"),
                          if (user_role == 'not used now')
                            Tab(text: "Übermittelt"),
                        ]),
                  ),
                  Container(
                    //Add this to give height
                    height: MediaQuery.of(context).size.height,
                    child: TabBarView(children: [
                      Container(
                        child: ValueListenableBuilder(
                          valueListenable: name,
                          builder: (BuildContext context, value, Widget child) {
                            return StreamBuilder<QuerySnapshot>(
                                stream: Firestore.instance
                                    .collection("Forms")
                                    .snapshots(),
                                builder: (BuildContext context,
                                    AsyncSnapshot<QuerySnapshot> snapshot) {
                                  // print('here $submittedForms');
                                  if (!snapshot.hasData)
                                    return new Text("There is no expense");
                                  return ListView(
                                    children:
                                        snapshot.data.documents.map((document) {
                                      if ((project.data['forms']
                                                  [document.documentID] ==
                                              true) &&
                                          (nameDocs.any((element) => element
                                                  .contains(project.documentID +
                                                      document.documentID)) ==
                                              false)) {
                                        return cellinner(
                                            context, document, project);
                                      }
                                      return Center(
                                        child: Container(
                                          width: 1,
                                          height: 1,
                                        ),
                                      );
                                    }).toList(),
                                  );
                                });
                          },
                        ),
                      ),
                      Container(
                        child: ValueListenableBuilder(
                          valueListenable: name,
                          builder: (BuildContext context, value, Widget child) {
                            return StreamBuilder<QuerySnapshot>(
                                stream: Firestore.instance
                                    .collection("Forms")
                                    .snapshots(),
                                builder: (BuildContext context,
                                    AsyncSnapshot<QuerySnapshot> snapshot) {
                                  if (!snapshot.hasData)
                                    return new Text("There is no expense");
                                  return ListView(
                                    children:
                                        snapshot.data.documents.map((document) {
                                      if ((project.data['forms']
                                                  [document.documentID] ==
                                              true) &&
                                          (nameDocs.any((element) => element
                                                  .contains(project.documentID +
                                                      document.documentID)) ==
                                              true)) {
                                        return cellinner(
                                            context, document, project);
                                      }
                                      return Center(
                                        child: Container(
                                          width: 1,
                                          height: 1,
                                        ),
                                      );
                                    }).toList(),
                                  );
                                });
                          },
                        ),
                      ),
                      if (user_role == 'not used now')
                        Container(
                          child: ValueListenableBuilder(
                            valueListenable: name,
                            builder:
                                (BuildContext context, value, Widget child) {
                              return StreamBuilder<QuerySnapshot>(
                                  stream: Firestore.instance
                                      .collection("FormValues")
                                      .snapshots(),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<QuerySnapshot> snapshot) {
                                    if (!snapshot.hasData)
                                      return new Text("There is no expense");
                                    return Expanded(
                                        child: ListView(
                                      itemExtent: 185,
                                      children: snapshot.data.documents
                                          .map((document) {
                                        if ((projectForms[
                                                    document.data['form']] ==
                                                true) &&
                                            (document.data['project'] ==
                                                project.documentID)) {
                                          // print(project.documentID);
                                          return new Text(
                                              "There is no expense");
                                          // subcell(context, document,
                                          //   project.documentID);
                                        }
                                        return Center(
                                          child: Container(
                                            width: 1,
                                            height: 1,
                                          ),
                                        );
                                      }).toList(),
                                    ));
                                  });
                            },
                          ),
                        ),
                    ]),
                  ),
                ],
              ),
            ),
            //ProjectCellItem(),
            // ProjectCellItem(
            //   name: search.text,
            //  ),
          ],
          staggeredTiles: [
            StaggeredTile.extent(2, 1000.0),
            //StaggeredTile.extent(2, 500.0),
            // StaggeredTile.extent(2, 150.0),
          ],
        ));
  }

  valueLineContent(String val, String val2) {
    final baseColor = PdfColors.cyan;
    if (val == 'weather') {
      print(val2);
    }
    return pw.Expanded(
        child: pw.Column(children: [
      pw.Container(
        alignment: pw.Alignment.centerLeft,
        padding: const pw.EdgeInsets.only(bottom: 2),
        child: pw.Text(
          val,
          style: pw.TextStyle(
            color: baseColor,
            fontSize: 20,
          ),
        ),
      ),
      pw.Container(
        alignment: pw.Alignment.centerLeft,
        padding: const pw.EdgeInsets.only(bottom: 2),
        child: pw.Paragraph(
          text: val2.toString(),
        ),
      ),
    ]));
  }

  Future<void> _createPDF(String projectName) async {
    //Create a PDF document.
    var documents = pw.Document();

    final Future<QuerySnapshot> documentfireStore =
        Firestore.instance.collection("FormValues").getDocuments();
    await documentfireStore.then<dynamic>((QuerySnapshot snapshot) async {
      for (int i = 0; i < snapshot.documents.length; i++) {
        if (snapshot.documents[i].documentID.contains(projectName)) {
          //Add page and draw text to the page.

          documents.addPage(pw.Page(
            build: (pw.Context context) => pw.Center(
              child: pw.Row(
                  crossAxisAlignment: pw.CrossAxisAlignment.start,
                  children: [
                    pw.Expanded(
                        child: pw.Column(children: [
                      // for (var i = 0; i < 2; i++)
                      for (int j = 0;
                          j <
                              Map<String, String>.from(
                                      snapshot.documents[i].data["data"])
                                  .length;
                          j++)
                        valueLineContent(
                            Map<String, String>.from(
                                    snapshot.documents[i].data["data"])
                                .keys
                                .elementAt(j)
                                .toString(),
                            snapshot.documents[i].data["data"].values
                                .elementAt(j)
                                .toString())
                    ])),
                  ]),
            ),
          ));
        }
      }
    });

    //Save the document

    Directory directory = await getExternalStorageDirectory();
    String path = directory.path;
//Get directory path
    final file = File('$path/$projectName.pdf');
    await file.writeAsBytes(await documents.save());
//Open the PDF document in mobile
    OpenFile.open('$path/$projectName.pdf');
  }

  Widget cellinner(
      BuildContext context, DocumentSnapshot form, DocumentSnapshot project) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => Fillform(
                form: form,
                projectName: project.documentID,
              ),
            ));
      },

      child: Card(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Container(
                          width: 200.0,
                          height: 25.0,
                          child: Text(removeTrailing(form.documentID),
                              style: TextStyle(
                                  color: Colors.black, fontSize: 20.0)))),
                ],
              ),
              if (user_role == 'admin')
                Align(
                  alignment: Alignment.topRight,
                  child: Material(
                    color: Colors.white,
                    shadowColor: Colors.white,
                    child: IconButton(
                      icon: new Icon(Icons.drag_indicator),
                      color: Colors.orange,
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SubmitsView(
                                form: form,
                                project: project,
                              ),
                            ));
                      },
                    ),
                  ),
                ),
              /*  if (user_role == 'admin')
                Align(
                  alignment: Alignment.topRight,
                  child: Material(
                    color: Colors.white,
                    shadowColor: Colors.white,
                    child: IconButton(
                      icon: new Icon(Icons.delete_forever_rounded),
                      color: Colors.orange,
                      onPressed: () {
                        showAlertDialog(context, form.documentID, project);
                      },
                    ),
                  ),
                ),*/
            ]),
      ),

      /// Item image
    );

    /// Review
    /// _showPopupMenu(){
  }

  String removeTrailing(String name) {
    return name.substring(0, name.indexOf('_'));
  }

  showAlertDialog(BuildContext context, String id, DocumentSnapshot project) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        FormManager().deleteaffected(id, project);
        Navigator.of(context, rootNavigator: true).pop('dialog');
        setState(() {
          projectForms[id] = false;
        });
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text("Sind Sie sicher?"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

showAlertDialogt(BuildContext context, String id, DocumentSnapshot project) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      FormManager().deleteaffected(id, project);
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    content: Text("Sind Sie sicher?"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

showAlertDialogtNormal(
    BuildContext context, String id, DocumentSnapshot project) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      FormManager().delete(id);
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    content: Text("Sind Sie sicher?"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

Widget cell(
    BuildContext context, DocumentSnapshot form, DocumentSnapshot project) {
  return GestureDetector(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Fillform(
              form: form,
              projectName: project.documentID,
            ),
          ));
    },

    child: Card(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Container(
                        width: 200.0,
                        height: 25.0,
                        child: Text(form.documentID,
                            style: TextStyle(
                                color: Colors.black, fontSize: 20.0)))),
              ],
            ),
            if (user_role == 'admin')
              Align(
                alignment: Alignment.topRight,
                child: Material(
                  color: Colors.white,
                  shadowColor: Colors.white,
                  child: IconButton(
                    icon: new Icon(Icons.edit_rounded),
                    color: Colors.orange,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  MultiFormFieldedit(forms: form)));
                    },
                  ),
                ),
              ),
            if (user_role == 'admin')
              Align(
                alignment: Alignment.topRight,
                child: Material(
                  color: Colors.white,
                  shadowColor: Colors.white,
                  child: IconButton(
                    icon: new Icon(Icons.delete_forever_rounded),
                    color: Colors.orange,
                    onPressed: () {
                      showAlertDialogt(context, form.documentID, project);
                      //setState(() {});
                    },
                  ),
                ),
              ),
          ]),
    ),

    /// Item image
  );

  /// Review
  /// _showPopupMenu(){
}

Widget cellNormal(
    BuildContext context, DocumentSnapshot form, DocumentSnapshot project) {
  return GestureDetector(
    onTap: () {
      /* Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Fillform(
              form: form,
              projectName: project.documentID,
            ),
          ));*/
    },

    child: Card(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Container(
                        width: 200.0,
                        height: 25.0,
                        child: Text(removeTrailing(form.documentID),
                            style: TextStyle(
                                color: Colors.black, fontSize: 20.0)))),
              ],
            ),
            if (user_role == 'admin')
              Align(
                alignment: Alignment.topRight,
                child: Material(
                  color: Colors.white,
                  shadowColor: Colors.white,
                  child: IconButton(
                    icon: new Icon(Icons.edit_rounded),
                    color: Colors.orange,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditForm(forms: form)));
                    },
                  ),
                ),
              ),
            if (user_role == 'admin')
              Align(
                alignment: Alignment.topRight,
                child: Material(
                  color: Colors.white,
                  shadowColor: Colors.white,
                  child: IconButton(
                    icon: new Icon(Icons.delete_forever_rounded),
                    color: Colors.orange,
                    onPressed: () {
                      showAlertDialogtNormal(context, form.documentID, project);
                      //setState(() {});
                    },
                  ),
                ),
              ),
          ]),
    ),

    /// Item image
  );

  /// Review
  /// _showPopupMenu(){
}

String removeTrailing(String name) {
  return name.substring(0, name.indexOf('_'));
}

Widget subcell(
    BuildContext context, DocumentSnapshot form, String projectName) {
  return GestureDetector(
    /*onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CheckSubmit(
              form: form,
              projectName: projectName,
            ),
          ));
    },*/
    child: Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.all(16.0),
              child: Container(
                  width: 200.0,
                  height: 25.0,
                  child: Text(form.data['form'],
                      style: TextStyle(color: Colors.black, fontSize: 20.0)))),
          Padding(
              padding: EdgeInsets.all(16.0),
              child: Container(
                  width: 200.0,
                  height: 40.0,
                  child: Text('eingereicht von : ' + form.data['data']['user'],
                      style: TextStyle(color: Colors.black, fontSize: 15.0)))),
          if (user_role == 'admin')
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Material(
                    color: Colors.white,
                    shadowColor: Colors.white,
                    child: Text(form.data['data']['time'],
                        style: TextStyle(color: Colors.black)),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 50),
                  child: Material(
                    color: Colors.white,
                    shadowColor: Colors.white,
                    child: Text(form.data['data']['date'],
                        style: TextStyle(color: Colors.black)),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Material(
                    color: Colors.white,
                    shadowColor: Colors.white,
                    child: FlatButton(
                      color: Color.fromRGBO(246, 220, 234, 0.16),
                      child: Text('Löschen',
                          style: TextStyle(
                              color: Color.fromRGBO(235, 94, 108, 1))),
                      onPressed: () {
                        showAlertDialogForm(context, form.documentID);
                      },
                    ),
                  ),
                ),
              ],
            )
        ],
      ),
    ),
  );

  /// Item image

  /// Review
  /// _showPopupMenu(){
}

showAlertDialogForm(BuildContext context, String id) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      FormManager().deletesubmit(id);
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    content: Text("Sind Sie sicher?"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
/*
 new StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('Forms').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) return new Text("There is no expense");
          // print(snapshot.data.documents.last.documentID);

          return ListView(
            children: snapshot.data.documents.map((document) {
              // if (project.data['forms'][document.documentID] == true)
              return Center(
                child: Container(
                  width: MediaQuery.of(context).size.width / 1,
                  height: MediaQuery.of(context).size.height / 6,
                  child: cell(context, document, project.documentID),
                ),
              );
            }).toList(),
          );
        });
        */
