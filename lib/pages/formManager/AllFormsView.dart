import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/pages/formManager/dynamic_textfields.dart';
import 'package:deiser_app/pages/formManager/formView.dart';
import 'package:deiser_app/pages/formManager/dropDownOptions.dart';
import 'package:deiser_app/pages/projectPage/addProjectWidget.dart';
import 'package:flutter/material.dart';
import 'FormPresenter.dart';
import 'package:deiser_app/mvp/mvpView.dart';

import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'FormCellWidget.dart';

/// This is the stateless widget that the main application instantiates.
class FormPageView extends MVPView {
  // Attributes
  final FormPagePresenter _presenter;

  // Constructor
  FormPageView(this._presenter) : super(presenter: _presenter);
  final ValueNotifier<String> name = ValueNotifier<String>("");
  TextEditingController search = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    double cHeight = MediaQuery.of(context).size.height * 0.75;

    return Scaffold(
        appBar: AppBar(
          elevation: 2.0,
          backgroundColor: Color(0xffec6500),
          title: Text('Forms',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 20.0)),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
              ),
            )
          ],
        ),
        body: StaggeredGridView.count(
          crossAxisCount: 2,
          crossAxisSpacing: 12.0,
          mainAxisSpacing: 12.0,
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          children: <Widget>[
            Container(
                width: 50.0,
                child: Row(
                  children: [
                    new Flexible(
                      child: TextField(
                        controller: search,
                        onChanged: (value) {
                          name.value = value;
                        },
                        decoration: InputDecoration(
                            icon: Icon(Icons.search),
                            // border: new OutlineInputBorder(
                            //    borderSide:
                            //       new BorderSide(color: Colors.white)),
                            hintText: 'Search '),
                      ),
                    ),
                    IconButton(
                      icon: new Icon(Icons.add_box_sharp),
                      iconSize: 45,
                      onPressed: () => Navigator.of(context)
                          .push(MaterialPageRoute(builder: (_) => Multiform()
                              // ProjectScreen()
                              )),
                      color: Color(0xffec6500),
                    )
                  ],
                )),
            ValueListenableBuilder(
              valueListenable: name,
              builder: (BuildContext context, value, Widget child) {
                return StreamBuilder<QuerySnapshot>(
                    stream: (value.toString() != "" && value.toString() != null)
                        ? Firestore.instance
                            .collection('Forms')
                            .where('name',
                                isGreaterThanOrEqualTo: value.toString())
                            .where('name', isLessThan: value.toString() + 'z')
                            .snapshots()
                        : Firestore.instance.collection("Forms").snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (!snapshot.hasData) return new Text("");
                      return ListView(
                        children: snapshot.data.documents.map((document) {
                          return Center(
                            child: Container(
                              width: MediaQuery.of(context).size.width / 1.1,
                              height: MediaQuery.of(context).size.height / 8,
                              child: cellNormal(context, document, document),
                            ),
                          );
                        }).toList(),
                      );
                    });
              },
            ),
          ],
          staggeredTiles: [
            StaggeredTile.extent(2, 50.0),
            StaggeredTile.extent(2, cHeight),
          ],
        ));
  }

  Widget _buildTile(Widget child, {Function() onTap}) {
    return Material(
        elevation: 5.0,
        color: Colors.white,
        borderRadius: BorderRadius.circular(12.0),
        shadowColor: Color(0x802196F3),
        child: InkWell(
            // Do onTap() if it isn't null, otherwise do print()
            onTap: onTap != null
                ? () => onTap()
                : () {
                    print('Not set yet');
                  },
            child: child));
  }
}
