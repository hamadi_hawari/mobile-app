import 'package:deiser_app/pages/formManager/checkBoxOptions%20.dart';
import 'package:deiser_app/pages/formManager/dropDownOptions.dart';
import 'package:deiser_app/pages/formManager/sectionOptions.dart';
import 'package:flutter/material.dart';
import 'field.dart';
import 'user.dart';

typedef OnDelete();

class FieldForm extends StatefulWidget {
  final Field field;
  final state = _FieldFormState();
  final OnDelete onDelete;

  FieldForm({Key key, this.field, this.onDelete}) : super(key: key);
  @override
  _FieldFormState createState() => state;

  bool isValid() => state.validate();
}

class _FieldFormState extends State<FieldForm> {
  final form = GlobalKey<FormState>();
  TextEditingController fieldController = new TextEditingController();
  bool checkedValue = false;

  String _mySelection = "2";

  List<DropdownMenuItem<String>> items = [];

  @override
  Widget build(BuildContext context) {
    //widget.field.typeName = this.field.typeName;
    //widget.field.drop = this.field.drop;
    return Padding(
      padding: EdgeInsets.all(16),
      child: Material(
        elevation: 1,
        clipBehavior: Clip.antiAlias,
        borderRadius: BorderRadius.circular(8),
        child: Form(
          key: form,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              AppBar(
                leading: Icon(Icons.verified_user),
                elevation: 0,
                title: Text('Feld Details'),
                backgroundColor: Color(0xffec6500),
                centerTitle: true,
                actions: <Widget>[
                  IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: widget.onDelete,
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: TextFormField(
                  initialValue: widget.field.name,
                  onSaved: (val) {
                    widget.field.name = val;
                    // print(widget.field.name);
                  },
                  validator: (val) =>
                      val.length > 3 ? null : 'Field name is invalid',
                  decoration: InputDecoration(
                    labelText: 'Feldname',
                    hintText: 'Feldname eingeben',
                    isDense: true,
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(left: 16, right: 16, bottom: 5),
                  child: DropdownButtonFormField<String>(
                    onSaved: (val) {
                      setState(() {
                        if (val == 'text field') {
                          widget.field.typeName = val;
                          widget.field.type = new TextFormField(
                            decoration: InputDecoration(
                              labelText: 'Feldname',
                              hintText: 'Feldname eingeben',
                              isDense: true,
                            ),
                          );
                        }
                        if (val == 'drop down menu') {
                          widget.field.typeName = val;

                          /*   widget.field.type = DropdownButton<String>(
                            onChanged: (val) {},
                            items: [],
                            value: '',
                          );*/
                        }
                        if (val == 'check box') {
                          widget.field.typeName = val;
                        }
                      });
                    },
                    value: 'text field',
                    items: [
                      'text field',
                      'date picker',
                      'drop down',
                      'check box',
                      'text',
                      'section'
                    ].map<DropdownMenuItem<String>>(
                      (String val) {
                        return DropdownMenuItem(
                          child: Text(val),
                          value: val,
                        );
                      },
                    ).toList(),
                    onChanged: (val) async {
                      // showAlertDialog(context);
                      if (val == 'date picker') {
                        widget.field.typeName = val;

                        /*  widget.field.drop = await Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => new MyForm()),
                        );*/
                        //as List<String>;

                        //    print('list ${widget.field.drop}');
                      }
                      if (val == 'check box') {
                        widget.field.typeName = val;

                        widget.field.drop = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => new MyFormBox()),
                        );
                        //as List<String>;

                      }
                      if (val == 'drop down') {
                        widget.field.typeName = val;

                        widget.field.drop = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => new MyFormBox()),
                        );
                        //as List<String>;

                      }
                      if (val == 'text') {
                        widget.field.typeName = val;
                      }
                      if (val == 'section') {
                        widget.field.typeName = val;
                        widget.field.drop = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => new MySectionBox()),
                        );
                      }
                    },
                    decoration: InputDecoration(
                      labelText: 'Typ',
                    ),
                  )),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 5),
                child: TextFormField(
                  initialValue: widget.field.section,
                  onSaved: (val) {
                    widget.field.section = val;
                  },
                  validator: (val) =>
                      val.length > 0 ? null : 'Field is invalid',
                  decoration: InputDecoration(
                    labelText: 'section number',
                    hintText: 'enter section number',
                    isDense: true,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 5),
                child: CheckboxListTile(
                  title: Text("erforderlich"),
                  value: checkedValue,
                  onChanged: (newValue) {
                    setState(() {
                      checkedValue = newValue;
                      widget.field.requiredField = newValue;
                    });
                  },
                  controlAffinity:
                      ListTileControlAffinity.leading, //  <-- leading Checkbox
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ///form validator
  bool validate() {
    var valid = form.currentState.validate();
    if (valid) form.currentState.save();
    return valid;
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        print(fieldController.text);

        widget.field.dropOptions = (fieldController.text);
        print(widget.field.dropOptions.length);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Drop down"),
      content: Text("Felder hier einstellen"),
      actions: [
        Container(
            width: 250,
            child: Column(
              children: [
                TextFormField(controller: fieldController, onSaved: (val) {}),
                okButton
              ],
            ))
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<List<String>> navigateAndDisplaySelection(BuildContext context) async {
    //print('got inside');
    List<String> result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => new MyForm()),
    );
    items = result
        .map((item) =>
            DropdownMenuItem(child: Text(item), value: item.toString()))
        .toList();
    _mySelection = result[0];
    //print('my result $result');
    return result;
  }

  Future<List<String>> navigateAndDisplaySelectionBox(
      BuildContext context) async {
    //print('got inside');
    List<String> result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => new MyFormBox()),
    );

    return result;
  }
}
