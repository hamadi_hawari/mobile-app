import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/mvp/mvpView.dart';
import 'package:deiser_app/pages/adminDashboard/adminDashboardView.dart';
import 'package:deiser_app/pages/formManager/field.dart';
import 'package:deiser_app/pages/projectPage/projectPresenter.dart';
import 'package:deiser_app/pages/projectPage/projectView.dart';
import 'package:deiser_app/services/formServices.dart';
import 'package:flutter/material.dart';
import 'package:deiser_app/pages/formManager/dynamic_textfields.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CheckSubmit extends MVPView {
  // This widget is the root of your application.
  final DocumentSnapshot form;
  final DocumentSnapshot forms;

  final String projectName;
  const CheckSubmit({
    Key key,
    this.form,
    this.projectName,
    this.forms,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Color(0xffec6500),
        title: new Text(projectName + "/" + removeTrailing(form.data['form'])),
      ),
      body: new MyHomePage(
        title: form.documentID,
        form: form,
        projectName: projectName,
        forms: forms,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.form, this.projectName, this.forms})
      : super(key: key);
  final DocumentSnapshot form;
  final DocumentSnapshot forms;

  final String title;
  final String projectName;

  @override
  _MyHomePageState createState() =>
      new _MyHomePageState(form, projectName, forms);
}

class _MyHomePageState extends State<MyHomePage> {
  final String projectName;

  int _currentstep = 0;
  final DocumentSnapshot form;
  final DocumentSnapshot forms;

  List<Step> spr = List<Step>();
  _MyHomePageState(this.form, this.projectName, this.forms);
  Map<String, String> dataEntry = Map<String, String>();

  @override
  void initState() {
    super.initState();
  }

  int getOrder(String name) {
    if (name.contains('_')) {
      final startIndex = name.indexOf('_');

      return int.parse(name.substring(startIndex + 1));
    } else
      return 0;
  }

  @override
  Widget build(BuildContext context) {
    double c_width = MediaQuery.of(context).size.width * 0.8;

    List<dynamic> vals = form.data['data'].values.toList();
    List<dynamic> keys = form.data['data'].keys.toList();
    Map<String, dynamic> orders = forms.data['orders'][0];
    var del = [];
    orders.forEach((key, value) {
      if (keys.contains(key) == false) {
        del.add(key);
      }
    });
    orders.removeWhere((key, value) => del.contains(key));
    print('hereeeeeeeeeeeee');
    print(keys);
    print(vals);

    Map<String, dynamic> newmap = {};

    int indexicon = keys.indexOf('icon');

    String iconval = vals.elementAt(indexicon);
    keys.removeAt(indexicon);

    vals.removeAt(indexicon);
    List<String> sortedKeys = orders.keys.toList(growable: false)
      ..sort((k1, k2) => orders[k1].compareTo(orders[k2]));
    Map<String, dynamic> sortedMap = new Map.fromIterable(sortedKeys,
        key: (k) => k, value: (k) => vals.elementAt(keys.indexOf(k)));

    for (int i = 0; i < keys.length; i++) {
      if (sortedMap[keys[i]] == null) {
        sortedMap[keys[i]] = vals.elementAt(keys.indexOf(keys[i]));
      }
    }
    //sortedMap.forEach((k, v) => {newmap[k] = document[k]});
    return new Scaffold(
      body: new Container(
        child: new ListView.builder(
          itemCount: sortedMap.length - 1,
          itemBuilder: (_, i) => ListTile(
            contentPadding: EdgeInsets.only(bottom: 20, left: 10, right: 10),
            //leading: ,
            title: Container(
              child: Row(
                children: [
                  Container(
                      width: c_width,
                      padding: EdgeInsets.only(
                          top: 20, left: 0, right: 0, bottom: 20),
                      child: Text(
                        removeTrailing(sortedMap.keys.elementAt(i)),
                        maxLines: 20,
                        style: TextStyle(fontSize: 15),
                      )),
                  if ((sortedMap.keys.elementAt(i) == 'weather') ||
                      (sortedMap.keys.elementAt(i) == 'Wetter'))
                    CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Image.network(
                          'http://openweathermap.org/img/w/${iconval}.png',
                        ))
                ],
              ),
            ),
            subtitle: Text(sortedMap.values.elementAt(i)),
          ),
        ),
      ),
    );
  }
}

String removeTrailing(String name) {
  if (name.contains('_')) {
    return name.substring(0, name.indexOf('_'));
  } else
    return name;
}
