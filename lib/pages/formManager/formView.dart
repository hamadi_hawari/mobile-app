import 'package:deiser_app/mvp/mvpView.dart';
import 'package:deiser_app/pages/formManager/AllFormsView.dart';
import 'package:deiser_app/pages/formManager/FormPresenter.dart';
import 'package:deiser_app/pages/formManager/dynamicForm.dart';
import 'package:deiser_app/services/formServices.dart';
import 'package:flutter/material.dart';
import 'dynamic_textfields.dart';
import 'empty_state.dart';
import 'field.dart';
import 'form.dart';
import 'user.dart';

class Multiform extends MVPView {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new MultiFormField(),
    );
  }
}

class MultiFormField extends StatefulWidget {
  @override
  _MultiFormState createState() => _MultiFormState();
}

class _MultiFormState extends State<MultiFormField> {
  List<FieldForm> fields = [
    FieldForm(field: Field(name: '', typeName: 'text field'))
  ];
  final _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: .0,
        backgroundColor: Color(0xffec6500),
        //leading: Icon(Icons.save_outlined),
        title: Text('REGISTER FORM'),
        actions: <Widget>[
          FlatButton(
            child: Text('Save'),
            textColor: Colors.white,
            onPressed: onSave,
          )
        ],
      ),
      body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.grey,
                Colors.white10,
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: ReorderableListView(
            onReorder: _onReorder,
            children: [for (final item in fields) getChildItems(item, context)],
          )),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: onAddForm,
        backgroundColor: Color(0xffec6500),
        foregroundColor: Colors.white,
      ),
    );
  }

  ///on form user deleted
  void onDelete(Field _field) {
    setState(() {
      var find = fields.firstWhere(
        (it) => it.field == _field,
        orElse: () => null,
      );
      if (find != null) fields.removeAt(fields.indexOf(find));
    });
  }

  ///on add form
  void onAddForm() {
    setState(() {
      var _field = Field();
      fields.add(FieldForm(
        field: _field,
        onDelete: () => onDelete(_field),
      ));
    });
  }

  ///on save forms
  void onSave() {
    if (fields.length > 0) {
      var allValid = true;
      fields.forEach((form) => allValid = allValid && form.isValid());

      if (allValid) {
        // var data = fields.map((it) => it.field).toList();
        //print('form data : ${data.toString()}');
        DynamicForm form;
        print('fieldsssssssss ${fields.first}');

        //form.fields = data;

        for (int itr = 0; itr < fields.length; itr++) {
          fields[itr].field.name =
              fields[itr].field.name + '_' + itr.toString();
        }
        var orders = Map.fromIterable(fields,
            key: (e) => e.field.name, value: (e) => fields.indexOf(e));
        orders.removeWhere((key, value) => key == fields.first.field.name);
        //  print('fields ${fields}');

        var map1 = Map.fromIterable(fields,
            key: (e) => e.field.name.toString(),
            value: (e) => e.field.typeName);

        map1.removeWhere((key, value) => key == fields.first.field.name);

        var sections = Map.fromIterable(fields,
            key: (e) => e.field.name, value: (e) => e.field.section);
        sections.removeWhere((key, value) => key == fields.first.field.name);
        var requiredOption = Map.fromIterable(fields,
            key: (e) => e.field.name, value: (e) => e.field.requiredField);
        requiredOption
            .removeWhere((key, value) => key == fields.first.field.name);
        requiredOption.forEach((key, value) {
          if (requiredOption[key] == null) {
            requiredOption[key] = false;
          }
        });
        List<FieldForm> fields2 = [];
        for (int i = 0; i < fields.length; i++) {
          if ((fields[i].field.typeName == 'drop down') ||
              (fields[i].field.typeName == 'check box') ||
              (fields[i].field.typeName == 'section')) {
            // print('values of chack box : ${fields[i].field.drop}');
            fields2.add(fields[i]);
          }
        }
        print('fields 2 $map1');
        Map<String, List<String>> map2 = Map.fromIterable(fields2,
            key: (e) => e.field.name, value: (e) => e.field.drop);
        // print('here fieldssssss ${map1}');
        FormManager manager = new FormManager();
        manager.createFormDoc(map1, fields.first.field.name, map2, orders,
            requiredOption, sections);
        Navigator.of(context, rootNavigator: true).pop('dialog');
      }
    }
  }

  void _onReorder(int oldIndex, int newIndex) {
    setState(
      () {
        if (newIndex > oldIndex) {
          newIndex -= 1;
        }
        final FieldForm item = fields.removeAt(oldIndex);
        fields.insert(newIndex, item);
      },
    );
  }

  Widget getChildItems(_item, _context) {
    return Padding(
      key: ValueKey(_item),
      padding: const EdgeInsets.all(0.0),
      child: InkWell(
        child: Padding(padding: const EdgeInsets.all(8.0), child: _item),
      ),
    );
  }
}
