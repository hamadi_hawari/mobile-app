import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/mvp/mvpView.dart';
import 'package:deiser_app/pages/adminDashboard/adminDashboardView.dart';
import 'package:deiser_app/pages/formManager/checksubmit.dart';
import 'package:deiser_app/pages/formManager/field.dart';
import 'package:deiser_app/pages/projectPage/projectPresenter.dart';
import 'package:deiser_app/pages/projectPage/projectView.dart';
import 'package:deiser_app/services/formServices.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:deiser_app/pages/formManager/dynamic_textfields.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather/weather.dart';

Map<String, bool> valuesForms = {};
bool checker = false;
List<String> submittedForms = [];
List<String> nameDocs = [];
List<String> nameDocsuser = [];
Map<String, dynamic> projectForms = {};

class SubmitsView extends MVPView {
  // This widget is the root of your application.

  final DocumentSnapshot form;
  final DocumentSnapshot project;
  const SubmitsView({Key key, this.form, this.project}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Color(0xffec6500),
        title: new Text(removeTrailing(form.documentID)),
      ),
      body: new MyHomePage(
        title: form.documentID,
        form: form,
        project: project,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.form, this.project}) : super(key: key);
  final DocumentSnapshot form;
  final String title;
  final DocumentSnapshot project;

  @override
  _MyHomePageState createState() => new _MyHomePageState(form, project);
}

class _MyHomePageState extends State<MyHomePage> {
  final DocumentSnapshot project;

  int _currentstep = 0;
  final DocumentSnapshot form;
  List<Step> spr = List<Step>();

  final PageController controller = PageController();
  _MyHomePageState(this.form, this.project);

  @override
  void initState() {
    super.initState();
    projectForms = project.data['forms'];
  }

  @override
  Widget build(BuildContext context) {
    final ValueNotifier<String> name = ValueNotifier<String>("");
    double val = 185;

    return new Scaffold(
      body: new Theme(
          data: ThemeData(
              primaryColor: Colors.orange,
              colorScheme: ColorScheme.light(primary: Colors.orange)),
          child: StaggeredGridView.count(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            crossAxisCount: 2,
            crossAxisSpacing: 12.0,
            mainAxisSpacing: 12.0,
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            children: <Widget>[
              Container(
                child: ValueListenableBuilder(
                  valueListenable: name,
                  builder: (BuildContext context, value, Widget child) {
                    return StreamBuilder<QuerySnapshot>(
                        stream: Firestore.instance
                            .collection("FormValues")
                            .snapshots(),
                        builder: (BuildContext context,
                            AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (!snapshot.hasData)
                            return new Text("There is no expense");
                          return Flex(
                            direction: Axis.vertical,
                            children: snapshot.data.documents.map((document) {
                              if ((projectForms[document.data['form']] ==
                                      true) &&
                                  (document.data['project'] ==
                                      project.documentID) &&
                                  (document.data['form'] == form.documentID)) {
                                print(project.documentID);
                                return subcell(context, document,
                                    project.documentID, form);
                              } else
                                return Center(
                                  child: Container(
                                    width: 1,
                                    height: 1,
                                  ),
                                );
                            }).toList(),
                          );
                        });
                  },
                ),
              ),
            ],
            staggeredTiles: [
              StaggeredTile.extent(2, 10000.0),
              //StaggeredTile.extent(2, 500.0),
              // StaggeredTile.extent(2, 150.0),
            ],
          )),
    );
  }
}

String removeTrailing(String name) {
  return name.substring(0, name.indexOf('_'));
}

Widget subcell(BuildContext context, DocumentSnapshot form, String projectName,
    DocumentSnapshot forms) {
  return GestureDetector(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CheckSubmit(
              form: form,
              projectName: projectName,
              forms: forms,
            ),
          ));
    },
    child: Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.all(16.0),
              child: Container(
                  width: 200.0,
                  height: 25.0,
                  child: Text(removeTrailing(form.data['form']),
                      style: TextStyle(color: Colors.black, fontSize: 20.0)))),
          Padding(
              padding: EdgeInsets.all(16.0),
              child: Container(
                  width: 300.0,
                  height: 40.0,
                  child: Text(
                      'eingereicht von : ' +
                          (form.data['data']['Benutzer'] ??
                              form.data['data']['user']),
                      style: TextStyle(color: Colors.black, fontSize: 15.0)))),
          if (user_role == 'admin')
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Material(
                    color: Colors.white,
                    shadowColor: Colors.white,
                    child: Text(
                        (form.data['data']['time'] ??
                            form.data['data']['Uhrzeit']),
                        style: TextStyle(color: Colors.black)),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 50),
                  child: Material(
                    color: Colors.white,
                    shadowColor: Colors.white,
                    child: Text(
                        (form.data['data']['date'] ??
                            form.data['data']['Datum']),
                        style: TextStyle(color: Colors.black)),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Material(
                    color: Colors.white,
                    shadowColor: Colors.white,
                    child: FlatButton(
                      color: Color.fromRGBO(246, 220, 234, 0.16),
                      child: Text('Löschen',
                          style: TextStyle(
                              color: Color.fromRGBO(235, 94, 108, 1))),
                      onPressed: () {
                        showAlertDialogForm(context, form.documentID);
                      },
                    ),
                  ),
                ),
              ],
            )
        ],
      ),
    ),
  );

  /// Item image

  /// Review
  /// _showPopupMenu(){
}

showAlertDialogForm(BuildContext context, String id) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      FormManager().deletesubmit(id);
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    content: Text("Sind Sie sicher?"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
