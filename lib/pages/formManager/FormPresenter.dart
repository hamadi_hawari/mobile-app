import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
// MVP
import 'package:deiser_app/mvp/mvpPresenter.dart';
import 'FormModel.dart';
import 'AllFormsView.dart';

class FormPagePresenter extends MVPPresenter {
  final FormPageModel _model;

  FormPagePresenter(this._model) : super('License');

  static FormPageView build(dynamic item, {Widget appLogo}) =>
      FormPageView(FormPagePresenter(FormPageModel(item: item)));

  Map<String, dynamic> get item => _model.item;
}
