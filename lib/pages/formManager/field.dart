import 'package:flutter/cupertino.dart';

class Field {
  String name;
  Widget type;
  String typeName;
  String dropOptions;
  String description;
  bool requiredField;
  List<String> drop;
  String section;
  Field(
      {this.name = '',
      this.type,
      this.typeName,
      this.dropOptions,
      this.description,
      this.requiredField,
      this.drop,
      this.section = '0'});
}
