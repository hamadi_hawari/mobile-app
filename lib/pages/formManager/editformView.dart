import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/mvp/mvpView.dart';
import 'package:deiser_app/pages/formManager/AllFormsView.dart';
import 'package:deiser_app/pages/formManager/FormPresenter.dart';
import 'package:deiser_app/pages/formManager/dynamicForm.dart';
import 'package:deiser_app/pages/formManager/dynamic_textfields_for_edit.dart';
import 'package:deiser_app/services/formServices.dart';
import 'package:flutter/material.dart';
import 'dynamic_textfields.dart';
import 'empty_state.dart';
import 'field.dart';
import 'form.dart';
import 'user.dart';

class EditForm extends MVPView {
  // This widget is the root of your application.
  final forms;

  const EditForm({Key key, this.forms}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new MultiFormFieldedit(
        forms: forms,
      ),
    );
  }
}

class MultiFormFieldedit extends StatefulWidget {
  final forms;

  const MultiFormFieldedit({Key key, this.forms}) : super(key: key);
  @override
  _MultiFormState createState() => _MultiFormState(forms);
}

class _MultiFormState extends State<MultiFormFieldedit> {
  final DocumentSnapshot forms;
  List<FieldFormedit> fields = [];
  final _scaffoldKey = GlobalKey();

  _MultiFormState(this.forms);
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var map = HashMap.from(forms.data['data'][0]);
    var mapdrops = HashMap.from(forms.data['data'][0]);
    var req = forms.data['requiredField'][0];
    Map<String, dynamic> newmap = {};

    Map<String, dynamic> documentorder = forms.data['orders'][0];
    List<String> sortedKeys = documentorder.keys.toList(growable: false)
      ..sort((k1, k2) => documentorder[k1].compareTo(documentorder[k2]));

    Map<String, dynamic> sortedMap = new Map.fromIterable(sortedKeys,
        key: (k) => k, value: (k) => documentorder[k]);
    sortedMap.forEach((k, v) => {newmap[k] = map[k]});
    newmap.entries.forEach((e) {
      if (e.key == forms.documentID) {
        fields.add(
          FieldFormedit(
            field: Field(name: e.key.toString(), typeName: e.value.toString()),
          ),
        );
      } else {
        if ((forms.data['data'][0][e.key.toString()] == 'drop down') ||
            (forms.data['data'][0][e.key.toString()] == 'check box')) {
          List<String> drops;

          drops = forms.data[e.key.toString()]
              .map<String>((value) => value.toString())
              .toList();
          var _field = Field(
              name: e.key.toString(),
              typeName: e.value.toString(),
              drop: drops,
              requiredField: forms.data['requiredField'][0][e.key],
              section: forms.data['sections'][0][e.key]);
          fields.add(
            FieldFormedit(field: _field, onDelete: () => onDelete(_field)),
          );
        } else {
          var _field = Field(
              name: e.key,
              typeName: e.value,
              requiredField: forms.data['requiredField'][0][e.key],
              section: forms.data['sections'][0][e.key]);
          fields.add(
            FieldFormedit(field: _field, onDelete: () => onDelete(_field)),
          );
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffec6500),
        elevation: .0,
        title: Text('Formular bearbeiten'),
        actions: <Widget>[
          FlatButton(
            child: Text('Speichern'),
            textColor: Colors.white,
            onPressed: onSave,
          )
        ],
      ),
      body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.grey,
                Colors.white10,
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: ReorderableListView(
            onReorder: _onReorder,
            children: [for (final item in fields) getChildItems(item, context)],
          )),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: onAddForm,
        backgroundColor: Color(0xffec6500),
        foregroundColor: Colors.white,
      ),
    );
  }

  ///on form user deleted
  void onDelete(Field _field) {
    print('delete here ${_field.toString()}');

    setState(() {
      var find = fields.firstWhere(
        (it) => it.field == _field,
        orElse: () => null,
      );
      print('delete pressed $find');
      if (find != null) fields.removeAt(fields.indexOf(find));
    });
  }

  ///on add form
  void onAddForm() {
    setState(() {
      var _field = Field();
      fields.add(FieldFormedit(
        field: _field,
        onDelete: () => onDelete(_field),
      ));
      print('field length : ${fields.length}');
    });
  }

  ///on save forms
  void onSave() {
    if (fields.length > 0) {
      var allValid = true;
      fields.forEach((form) => allValid = allValid && form.isValid());
      print('fields not valid');
      if (allValid) {
        print('fields are valid');

        List<FieldFormedit> fields2 = [];
        for (int i = 0; i < fields.length; i++) {
          if ((fields[i].field.typeName == 'drop down') ||
              (fields[i].field.typeName == 'section') ||
              (fields[i].field.typeName == 'check box')) {
            // print('values of chack box : ${fields[i].field.drop}');
            fields2.add(fields[i]);
          }
          if ((fields[i].field.name.contains('_')) == false) {
            //String val = value;

            setState(() {
              fields[i].field.name =
                  fields[i].field.name + '_' + (i + 1).toString();
            });
          }
        }
        var map1 = Map.fromIterable(fields,
            key: (e) => e.field.name, value: (e) => e.field.typeName);
        var orders = Map.fromIterable(fields,
            key: (e) => e.field.name, value: (e) => fields.indexOf(e));
        var sections = Map.fromIterable(fields,
            key: (e) => e.field.name, value: (e) => e.field.section);
        var requiredOption = Map.fromIterable(fields,
            key: (e) => e.field.name, value: (e) => e.field.requiredField);
        requiredOption.forEach((key, value) {
          if (value != true) {
            requiredOption[key] = false;
          }
        });

        Map<String, List<String>> map2 = Map.fromIterable(fields2,
            key: (e) => e.field.name, value: (e) => e.field.drop);
        FormManager manager = new FormManager();

        manager.updateFormDoc(
            map1, forms.documentID, map2, orders, requiredOption, sections);
        print('fields are updated');

        Navigator.of(context, rootNavigator: true).pop('dialog');
      }
    }
  }

  void _onReorder(int oldIndex, int newIndex) {
    setState(
      () {
        if (newIndex > oldIndex) {
          newIndex -= 1;
        }
        final FieldFormedit item = fields.removeAt(oldIndex);
        fields.insert(newIndex, item);
      },
    );
  }

  Widget getChildItems(_item, _context) {
    return Padding(
      key: ValueKey(_item),
      padding: const EdgeInsets.all(0.0),
      child: InkWell(
        child: Padding(padding: const EdgeInsets.all(8.0), child: _item),
      ),
    );
  }
}
