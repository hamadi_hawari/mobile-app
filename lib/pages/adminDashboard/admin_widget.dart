import 'package:deiser_app/pages/adminDashboard/lastActivities.dart';
import 'package:flutter/material.dart';
import 'LastActivities.dart';

class ShopItemsPage extends StatefulWidget {
  @override
  _ShopItemsPageState createState() => _ShopItemsPageState();
}

class _ShopItemsPageState extends State<ShopItemsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      scrollDirection: Axis.vertical,
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      children: <Widget>[
        Container(
            margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 54.0),
            child: Material(
              elevation: 3.0,
              color: Colors.black,
              borderRadius: BorderRadius.circular(32.0),
            )),
        ShopItem(),
      ],
    ));
  }
}

class ShopItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 0.0),
      child: Stack(
        children: <Widget>[
          /// Item card
          Align(
            alignment: Alignment.topCenter,
            child: SizedBox.fromSize(
                size: Size.fromHeight(110.0),
                child: Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    /// Item description inside a material
                    Container(
                      margin: EdgeInsets.only(top: 0.0),
                      child: Material(
                        elevation: 2.0,
                        borderRadius: BorderRadius.circular(12.0),
                        shadowColor: Color(0x802196F3),
                        color: Colors.white,
                        child: InkWell(
                          onTap: () =>
                              Navigator.of(context).push(MaterialPageRoute(
                                  //  builder: (_) => LastActivitiesView()
                                  )),
                          child: Padding(
                            padding: EdgeInsets.all(24.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                /// Title and rating
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('Ivascu Adrian',
                                        style: TextStyle(
                                            color: Colors.blueAccent)),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text('text',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 11.0)),
                                      ],
                                    ),
                                  ],
                                ),

                                /// Infos
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),

                    /// Item image
                    Align(
                      alignment: Alignment.centerRight,
                      child: Padding(
                        padding: EdgeInsets.only(right: 20.0),
                        child: SizedBox.fromSize(
                          size: Size.fromRadius(35.0),
                          child: Material(
                            elevation: 2.0,
                            shadowColor: Color(0x802196F3),
                            shape: CircleBorder(),
                            child: Icon(
                              Icons.face,
                              color: Colors.orange,
                              size: 30.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),

          /// Review
        ],
      ),
    );
  }
}
