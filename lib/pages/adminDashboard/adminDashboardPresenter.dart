import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
// MVP
import 'package:deiser_app/mvp/mvpPresenter.dart';
import 'adminDashboardModel.dart';
import 'adminDashboardView.dart';

class AdminDashboardPresenter extends MVPPresenter {
  final AdminDashboardModel _model;

  AdminDashboardPresenter(this._model) : super('License');

  static AdminDashboard build(dynamic item, {Widget appLogo}) =>
      AdminDashboard();

  Map<String, dynamic> get item => _model.item;
}
