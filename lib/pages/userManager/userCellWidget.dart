import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/pages/adminDashboard/lastActivities.dart';
import 'package:deiser_app/pages/formManager/FormCellWidget.dart';
import 'package:deiser_app/pages/userManager/editUserView.dart';
import 'package:deiser_app/services/projectServices.dart';
import 'package:deiser_app/services/userServices.dart';
import 'package:flutter/material.dart';

enum WhyFarther { harder, smarter, selfStarter, tradingCharter }

class UserCell extends StatefulWidget {
  @override
  _UserCellState createState() => _UserCellState();
}

class _UserCellState extends State<UserCell> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      scrollDirection: Axis.vertical,
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      children: <Widget>[
        Container(
            margin: EdgeInsets.symmetric(vertical: 16.0, horizontal: 54.0),
            child: Material(
              //elevation: 8.0,
              color: Colors.white,
              borderRadius: BorderRadius.circular(32.0),
            )),
        // ProjectCellItem(),
      ],
    ));
  }
}

class UserCellItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('users').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) return new Text("There is no expense");
          print(snapshot.data.documents.last.documentID);
          return ListView(
            children: snapshot.data.documents.map((document) {
              return Center(
                child: Container(
                    width: MediaQuery.of(context).size.width / 1,
                    height: MediaQuery.of(context).size.height / 6,
                    child: Text('data') //cell(context, document),
                    ),
              );
            }).toList(),
          );
        });
  }
}

Widget cell(BuildContext context, DocumentSnapshot user,
    List<DocumentSnapshot> nameprjects) {
  int projectNumber = 0;

  for (DocumentSnapshot item in nameprjects) {
    if (item.data['users'][user.data['username']] == true) {
      projectNumber++;
    }
  }
  return GestureDetector(
    onTap: () {
      /*  Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => FormCell(),
          ));*/
    },
    child: Card(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                    width: 340,
                    child: Row(
                      children: [
                        SizedBox(
                          width: 270,
                          child: Text('   ' + user.data['username'],
                              style: TextStyle(
                                  color: Colors.black, fontSize: 20.0)),
                        ),
                        SizedBox(
                          width: 30,
                          child: Material(
                            color: Colors.white,
                            shadowColor: Colors.white,
                            child: IconButton(
                              icon: new Icon(Icons.edit_rounded),
                              color: Colors.orange,
                              onPressed: () =>
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (_) => EditUser(
                                            name: user.documentID,
                                          )
                                      // ProjectScreen()
                                      )),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 30,
                          child: Material(
                            color: Colors.white,
                            shadowColor: Colors.white,
                            child: IconButton(
                              icon: new Icon(Icons.delete_forever_rounded),
                              color: Colors.orange,
                              onPressed: () {
                                showAlertDialog(context, user.documentID,
                                    user.data["username"]);
                              },
                            ),
                          ),
                        )
                      ],
                    )),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('     ' + projectNumber.toString(),
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w400,
                            fontSize: 15.0)),
                    Text('  Bauvorhaben',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w200,
                            fontSize: 15.0)),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          ]),
    ),

    /// Item image
  );

  /// Review
  /// _showPopupMenu(){
}

showAlertDialog(BuildContext context, String id, String username) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      UserAccountManager().delete(id, username);
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    content: Text("Sind Sie sicher ?"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
