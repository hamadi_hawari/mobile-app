import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/pages/formManager/dynamic_textfields.dart';
import 'package:deiser_app/pages/formManager/formView.dart';
import 'package:deiser_app/pages/formManager/dropDownOptions.dart';
import 'package:deiser_app/pages/projectPage/addProjectWidget.dart';
import 'package:deiser_app/pages/userManager/addUserView.dart';
import 'package:deiser_app/pages/userManager/userProfileView.dart';
import 'package:flutter/material.dart';
import 'editUserView.dart';
import 'userPresenter.dart';
import 'package:deiser_app/mvp/mvpView.dart';

import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'userCellWidget.dart';

List<DocumentSnapshot> nameprjects = [];

class UserPageView extends MVPView {
  final UserPagePresenter _presenter;

  UserPageView(this._presenter) : super(presenter: _presenter);

  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(65.0), // here the desired height
          child: AppBar(
            elevation: 2.0,
            backgroundColor: Color(0xffec6500),
            title: Text('Benutzer',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 20.0)),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                ),
              )
            ],
          )),
      body: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({
    Key key,
  }) : super(key: key);

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

/// This is the stateless widget that the main application instantiates.
class _MyHomePageState extends State<MyHomePage> {
  // Attributes
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nameprjects = [];
    nameSub =
        Firestore.instance.collection('Projects').snapshots().listen((data) {
      //print('listener Fired at line 39 main.dart');
      data.documentChanges.forEach((x) {
        nameprjects.add(x.document);
        //   print(x.document.data['data']);
      });
    });
  }

  // Constructor
  final ValueNotifier<String> name = ValueNotifier<String>("");
  TextEditingController search = new TextEditingController();
  StreamSubscription nameSub;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StaggeredGridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 12.0,
      mainAxisSpacing: 12.0,
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      children: <Widget>[
        Container(
            width: 50.0,
            child: Row(
              children: [
                new Flexible(
                  child: TextField(
                    controller: search,
                    onChanged: (value) {
                      name.value = value;
                    },
                    decoration: InputDecoration(
                        icon: Icon(Icons.search),
                        // border: new OutlineInputBorder(
                        //    borderSide:
                        //       new BorderSide(color: Colors.white)),
                        hintText: 'Suchen '),
                  ),
                ),
                IconButton(
                  icon: new Icon(Icons.add_box_sharp),
                  iconSize: 45,
                  onPressed: () => Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Filluser()
                          // ProjectScreen()
                          )),
                  color: Colors.orange,
                )
              ],
            )),
        ValueListenableBuilder(
          valueListenable: name,
          builder: (BuildContext context, value, Widget child) {
            return StreamBuilder<QuerySnapshot>(
                stream: (value.toString() != "" && value.toString() != null)
                    ? Firestore.instance
                        .collection('users')
                        .where('name', isGreaterThanOrEqualTo: value.toString())
                        .where('name', isLessThan: value.toString() + 'z')
                        .snapshots()
                    : Firestore.instance.collection("users").snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (!snapshot.hasData) return new Text("There is no expense");
                  return ListView(
                    children: snapshot.data.documents.map((document) {
                      return Center(
                        child: Container(
                          width: MediaQuery.of(context).size.width / 1.1,
                          // height: MediaQuery.of(context).size.height / 8,
                          child: cell(context, document, nameprjects),
                        ),
                      );
                    }).toList(),
                  );
                });
          },
        ),
      ],
      staggeredTiles: [
        StaggeredTile.extent(2, 50.0),
        StaggeredTile.extent(2, 500.0),
      ],
    ));
  }

  Widget _buildTile(Widget child, {Function() onTap}) {
    return Material(
        elevation: 5.0,
        color: Colors.black,
        borderRadius: BorderRadius.circular(12.0),
        shadowColor: Color(0x802196F3),
        child: InkWell(
            // Do onTap() if it isn't null, otherwise do print()
            onTap: onTap != null
                ? () => onTap()
                : () {
                    print('Not set yet');
                  },
            child: child));
  }
}
