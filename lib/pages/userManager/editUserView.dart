import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/mailManager.dart';
import 'package:deiser_app/mvp/mvpView.dart';
import 'package:deiser_app/pages/adminDashboard/adminDashboardPresenter.dart';
import 'package:deiser_app/pages/adminDashboard/adminDashboardView.dart';
import 'package:deiser_app/pages/formManager/field.dart';
import 'package:deiser_app/pages/formManager/user.dart';
import 'package:deiser_app/pages/projectPage/project.dart';
import 'package:deiser_app/pages/projectPage/projectPresenter.dart';
import 'package:deiser_app/pages/projectPage/projectView.dart';
import 'package:deiser_app/services/projectServices.dart';
import 'package:deiser_app/services/userServices.dart';
import 'package:flutter/material.dart';
import 'package:deiser_app/pages/formManager/dynamic_textfields.dart';
import 'package:flutter/material.dart';

TextEditingController usertNameController = new TextEditingController();
String roleController = 'admin';
TextEditingController emailController = new TextEditingController();
TextEditingController passwordController = new TextEditingController();

class EditUser extends MVPView {
  // This widget is the root of your application.
  final String name;

  const EditUser({Key key, this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(65.0), // here the desired height
          child: AppBar(
            elevation: 2.0,
            backgroundColor: Color(0xffec6500),
            title: Text('Edit User',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 20.0)),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                ),
              )
            ],
          )),
      body: new MyHomePage(
        name: name,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.name}) : super(key: key);
  final String name;

  @override
  _MyHomePageState createState() => new _MyHomePageState(name);
}

class _MyHomePageState extends State<MyHomePage> {
  final String name;

  List<Step> spr = <Step>[
    // const Step( title:  ,'SubTitle1', 'This is Content', state: StepState.indexed, true)

    Step(
        title: const Text('user role'),
        content: DropdownButtonFormField<String>(
          value: 'admin',
          onChanged: (val) {
            roleController = val;
          },
          items: ['admin', 'manager', 'user'].map<DropdownMenuItem<String>>(
            (String val) {
              return DropdownMenuItem(
                child: Text(val),
                value: val,
              );
            },
          ).toList(),
          decoration: InputDecoration(
            labelText: 'role',
            icon: Icon(Icons.calendar_today),
          ),
        ),
        state: StepState.indexed,
        isActive: true),

    Step(
        title: const Text('user email'),
        content: TextFormField(
          controller: emailController,
          onSaved: (val) {},
          onChanged: (val) {
            emailController.text = val;
          },
          validator: (val) => val.length > 3 ? null : 'Field name is invalid',
          decoration: InputDecoration(
            labelText: 'answer',
            hintText: 'Enter your Answer',
            isDense: true,
          ),
        ),
        state: StepState.indexed,
        isActive: true),
    Step(
      title: const Text('password'),
      content: TextFormField(
        controller: passwordController,
        onChanged: (val) {
          passwordController.text = val;
        },
        onSaved: (val) {},
        validator: (val) => val.length > 3 ? null : 'Field name is invalid',
        decoration: InputDecoration(
          labelText: 'answer',
          hintText: 'Enter your Answer',
          isDense: true,
        ),
      ),
      state: StepState.indexed,
      isActive: true,
    ),
  ];

  _MyHomePageState(this.name);

  Future<dynamic> getData() async {
    final DocumentReference document =
        Firestore.instance.collection("users").document(name);

    await document.get().then<dynamic>((DocumentSnapshot snapshot) async {
      // print(snapshot.data['password']) ;
      usertNameController.text = snapshot.data['username'];
      passwordController.text = snapshot.data['password'];
      //roleController = snapshot.data['role'];
      emailController.text = snapshot.data['email'];
    });
  }

  int _currentstep = 0;
  static const _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  void _movetonext() {
    setState(() {
      if (spr.length - 1 == _currentstep) {
        String pass = getRandomString(7);
        print('role $roleController');
        UserAccountManager().updateUserDoc(
            User(
                fullName: usertNameController.text,
                email: emailController.text,
                role: roleController,
                password: passwordController.text),
            emailController.text);
        AdminDashboardPresenter pr;
        Navigator.of(context, rootNavigator: true).pop('dialog');
      } else {
        _currentstep++;
      }
    });
  }

  void valueUpdate(String val) {
    setState(() {
      roleController = val;
    });
  }

  void _movetostart() {
    setState(() {
      _currentstep = 0;
    });
  }

  void _showcontent(int s) {
    showDialog<Null>(
      context: context,

      barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('You clicked on'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: <Widget>[
                spr[s].title,
                spr[s].subtitle,
              ],
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    spr.add(
      Step(
        title: const Text('user Name'),
        content: TextFormField(
          controller: usertNameController,
          onChanged: (val) {
            usertNameController.text = val;
            print(usertNameController.text);
          },
          validator: (val) => val.length > 3 ? null : 'Field name is invalid',
          decoration: InputDecoration(
            labelText: 'answer',
            hintText: 'Enter your Answer',
            isDense: true,
          ),
        ),
        state: StepState.indexed,
        isActive: true,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    getData();
    return new Scaffold(
      body: new Theme(
        data: ThemeData(
            primaryColor: Colors.orange,
            colorScheme: ColorScheme.light(primary: Colors.orange)),
        child: new Stepper(
          steps: spr,
          type: StepperType.vertical,
          currentStep: _currentstep,
          onStepContinue: _movetonext,
          onStepCancel: _movetostart,
          onStepTapped: _showcontent,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _currentstep = spr.length - 1;
          });
        },
        child: Icon(Icons.arrow_downward_outlined),
        backgroundColor: Colors.green,
      ),
    );
  }
}
