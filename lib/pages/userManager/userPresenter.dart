import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
// MVP
import 'package:deiser_app/mvp/mvpPresenter.dart';
import 'userModel.dart';
import 'AllUsersView.dart';

class UserPagePresenter extends MVPPresenter {
  final UserPageModel _model;

  UserPagePresenter(this._model) : super('License');

  static UserPageView build(dynamic item, {Widget appLogo}) =>
      UserPageView(UserPagePresenter(UserPageModel(item: item)));

  Map<String, dynamic> get item => _model.item;
}
