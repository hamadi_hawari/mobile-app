// Redux
import 'package:deiser_app/application.dart';
import 'package:redux/redux.dart';
import 'package:deiser_app/redux/reduxModel.dart';

class UserPageModel {
  final Map<String, dynamic> item;
  final Store<ReduxModel> store = Application.store;

  UserPageModel({this.item});

  bool get role => store.state.roles.isEmpty;
}
