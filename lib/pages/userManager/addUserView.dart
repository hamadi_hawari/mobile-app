import 'dart:math';

import 'package:deiser_app/mailManager.dart';
import 'package:deiser_app/mvp/mvpView.dart';
import 'package:deiser_app/pages/adminDashboard/adminDashboardPresenter.dart';
import 'package:deiser_app/pages/adminDashboard/adminDashboardView.dart';
import 'package:deiser_app/pages/formManager/field.dart';
import 'package:deiser_app/pages/formManager/user.dart';
import 'package:deiser_app/pages/projectPage/project.dart';
import 'package:deiser_app/pages/projectPage/projectPresenter.dart';
import 'package:deiser_app/pages/projectPage/projectView.dart';
import 'package:deiser_app/services/projectServices.dart';
import 'package:deiser_app/services/userServices.dart';
import 'package:flutter/material.dart';
import 'package:deiser_app/pages/formManager/dynamic_textfields.dart';
import 'package:flutter/material.dart';

TextEditingController usertNameController = new TextEditingController();
TextEditingController roleController = new TextEditingController();
TextEditingController emailController = new TextEditingController();

class Filluser extends MVPView {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(65.0), // here the desired height
          child: AppBar(
            elevation: 2.0,
            backgroundColor: Color(0xffec6500),
            title: Text('Benutzer hinzufügen',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 20.0)),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                ),
              )
            ],
          )),
      body: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({
    Key key,
  }) : super(key: key);

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentstep = 0;
  static const _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  void _movetonext() {
    setState(() {
      print(_currentstep);
      if (spr.length - 1 == _currentstep) {
        String pass = getRandomString(7);
        UserAccountManager().createUserDoc(
            User(
                fullName: usertNameController.text,
                email: emailController.text.toLowerCase(),
                role: roleController.text,
                password: pass),
            emailController.text.toLowerCase());
        MailManager(emailController.text, pass);
        AdminDashboardPresenter pr;
        Navigator.of(context, rootNavigator: true).pop('dialog');
      } else {
        _currentstep++;
      }
    });
  }

  void _movetostart() {
    setState(() {
      _currentstep = 0;
    });
  }

  void _showcontent(int s) {
    showDialog<Null>(
      context: context,

      barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('You clicked on'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: <Widget>[
                spr[s].title,
                spr[s].subtitle,
              ],
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Theme(
        data: ThemeData(
            primaryColor: Colors.orange,
            colorScheme: ColorScheme.light(primary: Colors.orange)),
        child: new Stepper(
          steps: spr,
          type: StepperType.vertical,
          currentStep: _currentstep,
          onStepContinue: _movetonext,
          onStepCancel: _movetostart,
          onStepTapped: _showcontent,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _currentstep = spr.length - 1;
          });
        },
        child: Icon(Icons.arrow_downward_outlined),
        backgroundColor: Colors.green,
      ),
    );
  }
}

List<Step> spr = <Step>[
  // const Step( title:  ,'SubTitle1', 'This is Content', state: StepState.indexed, true)

  Step(
    title: const Text('Vorname & Name'),
    content: TextFormField(
      controller: usertNameController,
      onSaved: (val) {},
      validator: (val) => val.length > 3 ? null : 'Field name is invalid',
      decoration: InputDecoration(
        labelText: 'Antwort',
        hintText: 'Name eingeben',
        isDense: true,
      ),
    ),
    state: StepState.indexed,
    isActive: true,
  ),

  Step(
      title: const Text('Benutzerrolle'),
      content: DropdownButtonFormField<String>(
        value: 'user',
        onChanged: (value) {
          roleController.text = value;
        },
        items: ['admin', 'manager', 'user'].map<DropdownMenuItem<String>>(
          (String val) {
            return DropdownMenuItem(
              child: Text(val),
              value: val,
            );
          },
        ).toList(),
        decoration: InputDecoration(
          labelText: 'Rolle',
          icon: Icon(Icons.calendar_today),
        ),
      ),
      state: StepState.indexed,
      isActive: true),

  Step(
      title: const Text('Benutzer-Email'),
      content: TextFormField(
        controller: emailController,
        onSaved: (val) {},
        validator: (val) => val.length > 3 ? null : 'Field name is invalid',
        decoration: InputDecoration(
          labelText: 'Antwort',
          hintText: 'Email eingeben',
          isDense: true,
        ),
      ),
      state: StepState.indexed,
      isActive: true),
];
