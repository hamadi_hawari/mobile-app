import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:deiser_app/pages/fillForm/fillFormView.dart';
import 'package:deiser_app/pages/formManager/AllFormsView.dart';
import 'package:deiser_app/pages/formManager/FormPresenter.dart';
import 'package:deiser_app/pages/projectPage/projectCellWidget.dart';
import 'package:deiser_app/pages/userManager/AllUsersView.dart';
import 'package:deiser_app/pages/userManager/userPresenter.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../main.dart';
import 'userDashboardPresenter.dart';
import 'package:deiser_app/mvp/mvpView.dart';

import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:deiser_app/pages/projectPage/projectView.dart';
import 'package:deiser_app/pages/projectPage/projectPresenter.dart';

/// This is the stateless widget that the main application instantiates.
///
class UserDashboard extends StatefulWidget {
  const UserDashboard({Key key}) : super(key: key);

  @override
  _UserDashboardState createState() => _UserDashboardState();
}

class _UserDashboardState extends State<UserDashboard> {
  ProjectPagePresenter _presenterProject;
  FormPagePresenter _presenterForm;
  UserPagePresenter _presenterUser;

  // Constructor
  _UserDashboardState();

  var users = '0';
  var forms = '0';
  var projects = '0';

  Future<dynamic> getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = prefs.getString("username");

    final CollectionReference document3 =
        Firestore.instance.collection("Projects");

    DocumentSnapshot variable =
        await Firestore.instance.collection('users').document(userId).get();
    username = variable.data['username'];
    await document3.getDocuments().then((value) => setState(() {
          int numm = 0;
          List<DocumentSnapshot> projectss = value.documents;
          for (var item in projectss) {
            if (item.data['users'][username] == true) numm++;
          }
          projects = numm.toString();
        }));
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      getData();
    });
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(65.0), // here the desired height
            child: AppBar(
              centerTitle: true,
              elevation: 0.0,
              backgroundColor: Color(0xffec6500),
              title: Image.asset(
                'assets/images/logo.png',
                fit: BoxFit.scaleDown,
                scale: 6,
              ),
              leading: Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: IconButton(
                    icon: Icon(Icons.menu),
                    color: Colors.white,
                    // onPressed: () => Navigator.of(context).push(
                    //    MaterialPageRoute(builder: (_) => MyHomePageMain()))
                  )),
              actions: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      IconButton(
                          icon: Icon(Icons.logout),
                          color: Colors.white,
                          onPressed: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (_) => MyHomePageMain())))
                    ],
                  ),
                )
              ],
            )),
        backgroundColor: Colors.grey[100],
        body: Container(
            width: double.infinity,
            /*decoration: BoxDecoration(
            gradient: LinearGradient(begin: Alignment.topCenter, colors: [
          Colors.orange[700],
          Colors.orange[900]
        ])),*/

            child: StaggeredGridView.count(
              crossAxisCount: 2,
              crossAxisSpacing: 12.0,
              mainAxisSpacing: 35.0,
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 100.0),
              children: <Widget>[
                SizedBox(
                  height: 55,
                ),
                _buildTile(
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('BAUVORHABEN',
                                  style: TextStyle(
                                      color: Colors.grey[400],
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.w500)),
                              Text(projects.toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 22.0))
                            ],
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: EdgeInsets.only(right: 30.0),
                              child: SizedBox.fromSize(
                                size: Size.fromRadius(35.0),
                                child: Material(
                                  color: Colors.white,
                                  //elevation: 1.0,
                                  //shadowColor: Color(0x802196F3),
                                  //shape: CircleBorder(),
                                  child: Icon(
                                    Icons.home_work_rounded,
                                    color: Color(0xffec6500),
                                    size: 45.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ]),
                  ),
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            ProjectPageView(_presenterProject),
                      )),
                ),

                /*,
            Text(
              'Recent Activities',
              style: TextStyle(
                color: Colors.black,
                fontSize: 30.0,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Container(
                height: 1.0,
                width: 3.0,
                color: Colors.orange,
              ),
            ),
            ShopItem(),
            ShopItem(),*/
              ],
              staggeredTiles: [
                StaggeredTile.extent(2, 55.0),
                StaggeredTile.extent(2, 110.0),

                //StaggeredTile.extent(2, 30.0),
                //StaggeredTile.extent(2, 5.0),
                //StaggeredTile.extent(2, 120.0),
                //StaggeredTile.extent(2, 120.0),
              ],
            )));
  }

  Widget _buildTile(Widget child, {Function() onTap}) {
    return Container(
        //elevation: 2.0,
        //color: Color(0xF3F4F4),
        decoration: BoxDecoration(
            //border: Border.all(color: Colors.blueGrey[100]),
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: [
              BoxShadow(
                color: Colors.blueGrey[200],
                offset: Offset(0.0, 1.0), //(x,y)
                blurRadius: 1.0,
              ),
            ]),
        //borderRadius: BorderRadius.circular(12.0),
        //shadowColor: Color(0x802196F3),
        child: InkWell(
            // Do onTap() if it isn't null, otherwise do print()
            onTap: onTap != null
                ? () => onTap()
                : () {
                    print('Not set yet');
                  },
            child: child));
  }
}
