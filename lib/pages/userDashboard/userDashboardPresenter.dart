import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
// MVP
import 'package:deiser_app/mvp/mvpPresenter.dart';
import 'userDashboardModel.dart';
import 'userDashboardView.dart';

class UserDashboardPresenter extends MVPPresenter {
  final UserDashboardModel _model;

  UserDashboardPresenter(this._model) : super('License');

  static UserDashboard build(dynamic item, {Widget appLogo}) => UserDashboard();

  Map<String, dynamic> get item => _model.item;
}
