import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/mvp/mvpView.dart';
import 'package:deiser_app/pages/formManager/field.dart';
import 'package:deiser_app/pages/projectPage/project.dart';
import 'package:deiser_app/pages/projectPage/projectPresenter.dart';
import 'package:deiser_app/pages/projectPage/projectView.dart';
import 'package:deiser_app/services/projectServices.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:deiser_app/pages/formManager/dynamic_textfields.dart';
import 'package:flutter/material.dart';

TextEditingController projectNameController = new TextEditingController();
TextEditingController projectOwnerController = new TextEditingController();
TextEditingController projectleaderController = new TextEditingController();
TextEditingController projectPostController = new TextEditingController();
TextEditingController projectAddressController = new TextEditingController();
TextEditingController projectZipController = new TextEditingController();
TextEditingController projectStreetController = new TextEditingController();
TextEditingController projectStartController = new TextEditingController();
TextEditingController projectEndController = new TextEditingController();
TextEditingController projectClientController = new TextEditingController();
TextEditingController projectStatusController = new TextEditingController();
Map<String, bool> valuesForms = {};
Map<String, bool> valuesUsers = {};

String baustellenartController = 'Kleinbaustelle';
String seitenController = 'Einseitig';
TextEditingController breiteontroller = new TextEditingController();
String oberflacheController = 'Asphalt (man.)';
TextEditingController wiederherstllungsbreiteController =
    new TextEditingController();
TextEditingController grabenlangeController = new TextEditingController();
TextEditingController grabenbreiteController = new TextEditingController();
TextEditingController unebenheitController = new TextEditingController();
String ausfuhrendertController = 'Deiser';

StreamSubscription nameSub;
String _valueinti = '';

class EditProject extends MVPView {
  final String name;

  const EditProject({Key key, this.name}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(65.0), // here the desired height
          child: AppBar(
            elevation: 2.0,
            backgroundColor: Color(0xffec6500),
            title: Text('Edit project',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 20.0)),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                ),
              )
            ],
          )),
      body: new MyHomePage(
        name: name,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.name}) : super(key: key);

  final String name;

  @override
  _MyHomePageState createState() => new _MyHomePageState(name);
}

class _MyHomePageState extends State<MyHomePage> {
  final String name;
  List<String> usernames = [];

  _MyHomePageState(this.name);

  List<Step> get spr => [
        Step(
          title: const Text('Name BV'),
          subtitle: Text('Name BV eingeben'),
          content: TextFormField(
            controller: projectNameController,
            onSaved: (val) {},
            validator: (val) => val.length > 3 ? null : 'Field name is invalid',
            decoration: InputDecoration(
              labelText: 'Antwort',
              hintText: 'Antwort eingben',
              isDense: true,
            ),
          ),
          state: StepState.indexed,
          isActive: true,
        ),
        Step(
            title: const Text('Kolonnenführer'),
            subtitle: Text('Kolonnenführer auswählen'),
            content: StreamBuilder<QuerySnapshot>(
                stream: Firestore.instance.collection('users').snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(
                      child: CupertinoActivityIndicator(),
                    );
                  _valueinti = snapshot.data.documents.first.data['username'];
                  return Container(
                    padding: EdgeInsets.only(bottom: 16.0),
                    child: Row(
                      children: <Widget>[
                        new Expanded(
                          flex: 4,
                          child: DropdownButton(
                            value: projectOwnerController.text ?? _valueinti,
                            isDense: true,
                            onChanged: (valueSelectedByUser) {
                              setState(() {
                                _valueinti = valueSelectedByUser;
                                projectOwnerController.text =
                                    valueSelectedByUser;
                                print(_valueinti);
                              });
                            },
                            selectedItemBuilder: (BuildContext context) {
                              return snapshot.data.documents
                                  .map((DocumentSnapshot document) {
                                return Text(document.data['username']);
                              }).toList();
                            },
                            items: snapshot.data.documents
                                .map((DocumentSnapshot document) {
                              return DropdownMenuItem<String>(
                                value: document.data['username'],
                                child: Text(document.data['username']),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Innendienst'),
            subtitle: Text('Innendienst auswählen'),
            content: StreamBuilder<QuerySnapshot>(
                stream: Firestore.instance.collection('users').snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(
                      child: CupertinoActivityIndicator(),
                    );

                  return Container(
                    padding: EdgeInsets.only(bottom: 16.0),
                    child: Row(
                      children: <Widget>[
                        new Expanded(
                          flex: 4,
                          child: DropdownButton(
                            value: projectleaderController.text ??
                                snapshot.data.documents.first.data['username'],
                            isDense: true,
                            onChanged: (valueSelectedByUser) {
                              projectleaderController.text =
                                  valueSelectedByUser;
                            },
                            items: snapshot.data.documents
                                .map((DocumentSnapshot document) {
                              return DropdownMenuItem<String>(
                                value: document.data['username'],
                                child: Text(document.data['username']),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Straße'),
            subtitle: Text('Straße eingeben'),
            content: TextFormField(
              controller: projectPostController,
              onSaved: (val) {},
              validator: (val) =>
                  val.length > 3 ? null : 'Field name is invalid',
              decoration: InputDecoration(
                labelText: 'Antwort',
                hintText: 'Antwort eingeben',
                isDense: true,
              ),
            ),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Hausnummer'),
            subtitle: Text('Hausnummer eingeben'),
            content: TextFormField(
              controller: projectAddressController,
              onSaved: (val) {},
              validator: (val) =>
                  val.length > 3 ? null : 'Field name is invalid',
              decoration: InputDecoration(
                labelText: 'Antwort',
                hintText: 'Antwort eingeben',
                isDense: true,
              ),
            ),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Postleitzahl'),
            subtitle: Text('Geben Sie die Postleitzahl ein'),
            content: TextFormField(
              controller: projectZipController,
              onSaved: (val) {},
              validator: (val) =>
                  val.length > 3 ? null : 'Field name is invalid',
              decoration: InputDecoration(
                labelText: 'Antwort',
                hintText: 'Antwort eingeben',
                isDense: true,
              ),
            ),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Stadt'),
            subtitle: Text('Stadt eingeben'),
            content: TextFormField(
              controller: projectStreetController,
              onSaved: (val) {},
              validator: (val) =>
                  val.length > 3 ? null : 'Field name is invalid',
              decoration: InputDecoration(
                labelText: 'Antwort',
                hintText: 'Antwort eingeben',
                isDense: true,
              ),
            ),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Auftraggeber'),
            subtitle: Text('Auftraggeber eingeben'),
            content: TextFormField(
              controller: projectStartController,
              onSaved: (val) {},
              validator: (val) =>
                  val.length > 3 ? null : 'Field name is invalid',
              decoration: InputDecoration(
                labelText: 'Antwort',
                hintText: 'Antwort eingeben',
                isDense: true,
              ),
            ),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Ausführung von'),
            subtitle: Text('Datum eingeben'),
            content: TextFormField(
              controller: projectEndController,
              onSaved: (val) {},
              validator: (val) =>
                  val.length > 3 ? null : 'Field name is invalid',
              decoration: InputDecoration(
                labelText: 'Antwort',
                hintText: 'Antwort eingeben',
                isDense: true,
              ),
            ),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Ausführung bis'),
            subtitle: Text('Datun eingeben'),
            content: TextFormField(
              controller: projectClientController,
              onSaved: (val) {},
              validator: (val) =>
                  val.length > 3 ? null : 'Field name is invalid',
              decoration: InputDecoration(
                labelText: 'Antwort',
                hintText: 'Antwort eingeben',
                isDense: true,
              ),
            ),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Baustellenart'),
            subtitle: Text('Baustellenart'),
            content: StatefulBuilder(
              builder: (context, _setState) => new DropdownButton<String>(
                value: baustellenartController,
                isExpanded: true,
                items: <String>[
                  'Kleinbaustelle',
                  'Reparaturbaustelle',
                  'Linienbaustelle >10m'
                ]
                    .map<DropdownMenuItem<String>>(
                        (value) => new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            ))
                    .toList(),
                onChanged: (_val) {
                  setState(() {
                    baustellenartController = _val;
                    print(baustellenartController);
                  });
                },
              ),
            ),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Erf. Naschschnitt Seiten'),
            subtitle: Text('Erf. Naschschnitt Seiten'),
            content: StatefulBuilder(
              builder: (context, _setState) => new DropdownButton<String>(
                value: seitenController,
                isExpanded: true,
                items: <String>[
                  'Einseitig',
                  'Beidseitig',
                ]
                    .map<DropdownMenuItem<String>>(
                        (value) => new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            ))
                    .toList(),
                onChanged: (_val) {
                  setState(() {
                    seitenController = _val;
                  });
                },
              ),
            ),
            state: StepState.indexed,
            isActive: true),
        Step(
          title: const Text('Breite Nachschnitt'),
          subtitle: Text('Breite Nachschnitt'),
          content: TextFormField(
            controller: breiteontroller,
            onSaved: (val) {},
            validator: (val) => val.length > 3 ? null : 'Field name is invalid',
            decoration: InputDecoration(
              labelText: 'Antwort',
              hintText: 'Antwort eingben',
              isDense: true,
            ),
          ),
          state: StepState.indexed,
          isActive: true,
        ),
        Step(
            title: const Text('Art der Oberfläche'),
            subtitle: Text('Art der Oberfläche'),
            content: StatefulBuilder(
              builder: (context, _setState) => new DropdownButton<String>(
                value: oberflacheController,
                isExpanded: true,
                items: <String>[
                  'Asphalt (man.)',
                  'Asphalt (masch.)',
                  'Natursteinpflaster',
                  'Betonpflaster',
                  'Klinkerpflaster',
                  'Plattenflache',
                  'Sonstiges'
                ]
                    .map<DropdownMenuItem<String>>(
                        (value) => new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            ))
                    .toList(),
                onChanged: (_val) {
                  setState(() {
                    oberflacheController = _val;
                  });
                },
              ),
            ),
            state: StepState.indexed,
            isActive: true),
        Step(
          title: const Text('Wiederherstllungsbreite'),
          subtitle: Text('Wiederherstllungsbreite [m] '),
          content: TextFormField(
            controller: wiederherstllungsbreiteController,
            onChanged: (val) {},
            validator: (val) => val.length > 3 ? null : 'Field name is invalid',
            decoration: InputDecoration(
              labelText: 'Antwort',
              hintText: 'Antwort eingben',
              isDense: true,
            ),
          ),
          state: StepState.indexed,
          isActive: true,
        ),
        Step(
          title: const Text('Grabenlänge'),
          subtitle: Text('Grabenlänge [m]'),
          content: TextFormField(
            controller: grabenlangeController,
            onSaved: (val) {},
            validator: (val) => val.length > 3 ? null : 'Field name is invalid',
            decoration: InputDecoration(
              labelText: 'Antwort',
              hintText: 'Antwort eingben',
              isDense: true,
            ),
          ),
          state: StepState.indexed,
          isActive: true,
        ),
        Step(
          title: const Text('Grabenbreite'),
          subtitle: Text('Grabenbreite [m] '),
          content: TextFormField(
            controller: grabenbreiteController,
            onChanged: (val) {
              if (int.parse(grabenbreiteController.text) < 2) {
                if (oberflacheController == 'Asphalt (masch.)') {
                  unebenheitController.text = "3";
                } else
                  unebenheitController.text = "5";
              } else if (oberflacheController == 'Asphalt (masch.)') {
                unebenheitController.text = (1.5 /
                        1000 *
                        1000 *
                        int.parse(wiederherstllungsbreiteController.text))
                    .toString();
              } else {
                unebenheitController.text = (2.5 /
                        1000 *
                        1000 *
                        int.parse(wiederherstllungsbreiteController.text))
                    .toString();
              }
            },
            validator: (val) => val.length > 3 ? null : 'Field name is invalid',
            decoration: InputDecoration(
              labelText: 'Antwort',
              hintText: 'Antwort eingben',
              isDense: true,
            ),
          ),
          state: StepState.indexed,
          isActive: true,
        ),
        Step(
          title: const Text('Maximale Unebenheit'),
          subtitle: Text('Maximale Unebenheit [mm]'),
          content: TextFormField(
            controller: unebenheitController,
            onSaved: (val) {},
            validator: (val) => val.length > 3 ? null : 'Field name is invalid',
            decoration: InputDecoration(
                labelText: 'Antwort',
                hintText: 'Antwort eingben',
                isDense: true,
                enabled: false),
          ),
          state: StepState.indexed,
          isActive: true,
        ),
        Step(
            title: const Text('Ausführender Betrieb'),
            subtitle: Text('Ausführender Betrieb'),
            content: StatefulBuilder(
              builder: (context, _setState) => new DropdownButton<String>(
                value: ausfuhrendertController,
                isExpanded: true,
                items: <String>[
                  'Deiser',
                  'Panzer&Braun',
                ]
                    .map<DropdownMenuItem<String>>(
                        (value) => new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            ))
                    .toList(),
                onChanged: (_val) {
                  setState(() {
                    ausfuhrendertController = _val;
                  });
                },
              ),
            ),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Formulare'),
            subtitle: Text('Formuare auswählen'),
            content: new StreamBuilder<QuerySnapshot>(
                stream: Firestore.instance.collection("Forms").snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (!snapshot.hasData) return new Text("There is no expense");
                  return ListView(
                    shrinkWrap: true,
                    children: snapshot.data.documents.map((document) {
                      valuesForms[document.documentID] = false;

                      return Container(
                          width: 50,
                          height: 100,
                          child: StatefulBuilder(builder:
                              (BuildContext context, StateSetter setState) {
                            return Center(
                              child: CheckboxListTile(
                                title:
                                    Text(removeTrailing(document.documentID)),
                                value: valuesForms[document.documentID],
                                onChanged: (bool val) {
                                  setState(() {
                                    valuesForms[document.documentID] = val;
                                  });
                                },
                              ),
                            );
                          }));
                    }).toList(),
                  );
                }),
            state: StepState.indexed,
            isActive: true),
        Step(
            title: const Text('Mitarbeiter'),
            subtitle: Text('Mitarbeiter auswählen'),
            content: new StreamBuilder<QuerySnapshot>(
                stream: Firestore.instance.collection("users").snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (!snapshot.hasData) return new Text("There is no expense");
                  return ListView(
                    shrinkWrap: true,
                    children: snapshot.data.documents.map((document) {
                      valuesUsers[document.data['username']] = false;

                      return Container(
                          width: 50,
                          height: 100,
                          child: StatefulBuilder(builder:
                              (BuildContext context, StateSetter setState) {
                            return Center(
                              child: CheckboxListTile(
                                title: Text(document.data['username']),
                                value: valuesUsers[document.data['username']],
                                onChanged: (bool val) {
                                  setState(() {
                                    valuesUsers[document.data['username']] =
                                        val;
                                  });
                                },
                              ),
                            );
                          }));
                    }).toList(),
                  );
                }),
            state: StepState.indexed,
            isActive: true),
      ];
  String removeTrailing(String name) {
    return name.substring(0, name.indexOf('_'));
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  Future<dynamic> getData() async {
    final DocumentReference document =
        Firestore.instance.collection("Projects").document(name);

    await document.get().then<dynamic>((DocumentSnapshot snapshot) async {
      // print(snapshot.data['password']) ;
      projectNameController.text = snapshot.data['name'];

      projectOwnerController.text = snapshot.data['projectOwner'];
      projectleaderController.text = snapshot.data['teamLeader'];
      projectPostController.text = snapshot.data['post'];
      projectAddressController.text = snapshot.data['address'];
      projectZipController.text = snapshot.data['zip'];
      projectStreetController.text = snapshot.data['street'];
      projectStreetController.text = snapshot.data['startDate'];
      projectEndController.text = snapshot.data['endDate'];
      projectClientController.text = snapshot.data['client'];
      projectStatusController.text = snapshot.data['status'];

      oberflacheController = snapshot.data['artOberflaeche'];
      ausfuhrendertController = snapshot.data['ausfuehrenderBetrieb'];
      baustellenartController = snapshot.data['baustellenart'];
      breiteontroller.text = snapshot.data['breiteNachschnitt'];
      seitenController = snapshot.data['erfNaschschnittSeiten'];
      grabenbreiteController.text = snapshot.data['grabenbreite'];
      grabenlangeController.text = snapshot.data['grabenlaenge'];
      unebenheitController.text = snapshot.data['maximaleUnebenheit'];
      wiederherstllungsbreiteController.text =
          snapshot.data['wiederherstllungsbreite'];
      setState(() {
        valuesForms = Map<String, bool>.from(snapshot.data['forms']);
        valuesUsers = Map<String, bool>.from(snapshot.data['users']);
      });

      print('-----------------');
      print(valuesForms);
      print(valuesUsers);
    });
  }

  int _currentstep = 0;
  void _movetonext() {
    setState(() {
      if (spr.length - 1 == _currentstep) {
        ProjectManager().updateProjectDoc(
            Project(
              name: projectNameController.text,
              projectOwnerId: projectOwnerController.text,
              teamLeaderId: projectleaderController.text,
              post: projectPostController.text,
              address: projectAddressController.text,
              zip: projectZipController.text,
              street: projectStreetController.text,
              startDate: projectStreetController.text,
              endDate: projectEndController.text,
              client: projectClientController.text,
              status: projectStatusController.text,
              artOberflaeche: oberflacheController,
              ausfuehrenderBetrieb: ausfuhrendertController,
              baustellenart: baustellenartController,
              breiteNachschnitt: breiteontroller.text,
              erfNaschschnittSeiten: seitenController,
              grabenbreite: grabenbreiteController.text,
              grabenlaenge: grabenlangeController.text,
              maximaleUnebenheit: unebenheitController.text,
              wiederherstllungsbreite: wiederherstllungsbreiteController.text,
            ),
            valuesUsers,
            valuesForms,
            name);
        ProjectPagePresenter pr;
        Navigator.of(context, rootNavigator: true).pop('dialog');
      } else {
        _currentstep++;
      }
    });
  }

  void _movetostart() {
    setState(() {
      _currentstep = 0;
    });
  }

  void _showcontent(int s) {
    setState(() {
      _currentstep = s;
    });
  }

  @override
  Widget build(BuildContext context) {
    // getData();
    return new Scaffold(
      body: new Theme(
        data: ThemeData(
            primaryColor: Colors.orange,
            colorScheme: ColorScheme.light(primary: Colors.orange)),
        child: new Stepper(
          controlsBuilder: (BuildContext context,
              {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
            return Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RaisedButton(
                  color: Colors.orange[500],
                  onPressed: onStepContinue,
                  child: const Text('Next!'),
                ),
              ],
            );
          },
          steps: spr,
          type: StepperType.vertical,
          currentStep: _currentstep,
          onStepContinue: _movetonext,
          onStepCancel: _movetostart,
          onStepTapped: _showcontent,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _currentstep = spr.length - 1;
          });
        },
        child: Icon(Icons.arrow_downward_outlined),
        backgroundColor: Colors.green,
      ),
    );
  }
}
