class Project {
  String name;
  String projectOwnerId;
  String teamLeaderId;
  String street;
  String post;
  String address;

  String streetNum;
  String city;
  String zip;
  String gpsPos;
  String startDate;
  String endDate;
  String client;
  String status;
  String createTime;
  String baustellenart;
  String erfNaschschnittSeiten;
  String breiteNachschnitt;
  String artOberflaeche;
  String wiederherstllungsbreite;
  String grabenlaenge;
  String grabenbreite;
  String maximaleUnebenheit;
  String ausfuehrenderBetrieb;
  Project(
      {this.name,
      this.projectOwnerId,
      this.teamLeaderId,
      this.street,
      this.streetNum,
      this.city,
      this.post,
      this.address,
      this.zip,
      this.gpsPos,
      this.startDate,
      this.endDate,
      this.client,
      this.status,
      this.createTime,
      this.baustellenart,
      this.erfNaschschnittSeiten,
      this.breiteNachschnitt,
      this.artOberflaeche,
      this.wiederherstllungsbreite,
      this.grabenlaenge,
      this.grabenbreite,
      this.maximaleUnebenheit,
      this.ausfuehrenderBetrieb});
}

class CardDetails {
  String cardHolderName;
  String cardNumber;
  String expiryMonth;
  String expiryYear;
  int securityCode;
  CardDetails(
      {this.cardHolderName,
      this.cardNumber,
      this.expiryMonth,
      this.expiryYear,
      this.securityCode});
}

class Address {
  String postCode;
  String addressLine;
  Address({this.postCode, this.addressLine});

  factory Address.fromJson(Map<String, dynamic> json) {
    return Address(
      postCode: json['postCode'],
      addressLine: json['address'],
    );
  }
}

class Payment {
  Address address;
  CardDetails cardDetails;
  Payment({this.address, this.cardDetails});
}
