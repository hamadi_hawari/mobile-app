import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import 'project.dart';

class ProjectScreen extends StatelessWidget {
  const ProjectScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('add project',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 20.0)),
      ),
      body: ProjectForm(),
    );
  }
}

/// Form widgets are stateful widgets
class ProjectForm extends StatefulWidget {
  ProjectForm({Key key}) : super(key: key);

  @override
  _ProjectFormState createState() => _ProjectFormState();
}

class _ProjectFormState extends State<ProjectForm> {
  DateTime selectedDate = DateTime.now();
  Map<String, bool> validateField = {
    "cardField": false,
    "postCodeField": false
  };
  String expiryMonth;
  int expiryYear;
  bool rememberInfo = false;
  Address _paymentAddress = new Address();
  CardDetails _cardDetails = new CardDetails();
  bool loading = false;

  final _formKey = GlobalKey<FormState>();
  final _addressLineController = TextEditingController();
  final List yearsList = List.generate(12, (int index) => index + 2020);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(10.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 4,
                      child: TextFormField(
                        onSaved: (val) => _cardDetails.cardNumber = val,
                        autovalidate: validateField['cardField'],
                        onChanged: (value) {
                          setState(() {
                            validateField['cardField'] = true;
                          });
                        },
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: 'Project name',
                            icon: Icon(Icons.credit_card)),
                        validator: (value) {
                          if (value.length != 16)
                            return "Please enter a valid name";
                        },
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      child: DropdownButtonFormField<String>(
                        onSaved: (val) => _cardDetails.expiryMonth = val,
                        value: expiryMonth,
                        items: [
                          'hamadi hawari',
                          'ibrahim ben ahmed',
                        ].map<DropdownMenuItem<String>>(
                          (String val) {
                            return DropdownMenuItem(
                              child: Text(val),
                              value: val,
                            );
                          },
                        ).toList(),
                        onChanged: (val) {
                          setState(() {
                            expiryMonth = val;
                          });
                        },
                        decoration: InputDecoration(
                          labelText: 'projectOwner',
                          icon: Icon(Icons.calendar_today),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: DropdownButtonFormField<String>(
                          onSaved: (val) => _cardDetails.expiryMonth = val,
                          value: expiryMonth,
                          items: [
                            'hamadi hawari',
                            'ibrahim ben ahmed',
                          ].map<DropdownMenuItem<String>>(
                            (String val) {
                              return DropdownMenuItem(
                                child: Text(val),
                                value: val,
                              );
                            },
                          ).toList(),
                          onChanged: (val) {
                            setState(() {
                              expiryMonth = val;
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'team leader',
                            icon: Icon(Icons.calendar_today),
                          ),
                        ),
                      ),
                    ]),
                TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                    decoration: InputDecoration(
                      labelText: 'Post Code',
                      icon: Icon(Icons.location_on),
                    ),
                  ),
                  suggestionsCallback: (pattern) async {
                    return await _fetchAddress(pattern);
                  },
                  itemBuilder: (context, Address address) {
                    return ListTile(
                      leading: Icon(Icons.location_city),
                      title: Text(address.addressLine),
                      subtitle: Text(address.postCode),
                    );
                  },
                  onSuggestionSelected: (Address address) {
                    _addressLineController.text = address.addressLine;
                  },
                  onSaved: (val) => this._paymentAddress.postCode = val,
                ),
                TextFormField(
                  onSaved: (val) => _paymentAddress.addressLine = val,
                  controller: _addressLineController,
                  decoration: InputDecoration(
                    labelText: 'Address Line',
                    icon: Icon(Icons.location_city),
                    suffixIcon: IconButton(
                      onPressed: () => _addressLineController.clear(),
                      icon: Icon(Icons.clear),
                    ),
                  ),
                ),
                _createField('street', Icons.ac_unit_rounded),
                _createField('street Number', Icons.ac_unit_rounded),
                _createField('city', Icons.ac_unit_rounded),
                _createField('zip', Icons.ac_unit_rounded),
                Text('start date'),
                _createDatePicker(),
                Text('end date'),
                _createDatePicker(),
                _createField('client', Icons.ac_unit_rounded),
                _createField('status', Icons.ac_unit_rounded),
                _createField('baustellenart', Icons.ac_unit_rounded),
                _createField('erfNaschschnittSeiten', Icons.ac_unit_rounded),
                _createField('breiteNachschnitt', Icons.ac_unit_rounded),
                _createField('artOberflaeche', Icons.ac_unit_rounded),
                _createField('wiederherstllungsbreite', Icons.ac_unit_rounded),
                _createField('grabenlaenge', Icons.ac_unit_rounded),
                _createField('grabenbreite', Icons.ac_unit_rounded),
                _createField('maximaleUnebenheit', Icons.ac_unit_rounded),
                _createField('ausfuehrenderBetrieb', Icons.ac_unit_rounded),
                CheckboxListTile(
                  value: rememberInfo,
                  onChanged: (val) {
                    setState(() {
                      rememberInfo = val;
                    });
                  },
                  title: Text('Remember Information'),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        child: loading
                            ? SpinKitWave(
                                color: Colors.white,
                                size: 15.0,
                              )
                            : Text('Add Project'),
                        color: Colors.pinkAccent,
                        textColor: Colors.white,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            setState(() {
                              loading = true;
                            });
                            _formKey.currentState.save();
                            Timer(Duration(seconds: 4), () {
                              Payment payment = new Payment(
                                  address: _paymentAddress,
                                  cardDetails: _cardDetails);
                              setState(() {
                                loading = false;
                              });
                              final snackBar =
                                  SnackBar(content: Text('Payment Proccessed'));
                              Scaffold.of(context).showSnackBar(snackBar);
                              print('Saved');
                            });
                          }
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  Future<List<Address>> _fetchAddress(String postCode) async {
    final response = await http.get(
        "https://my-json-server.typicode.com/refactord/deep-dive-db/addresses");
    if (response.statusCode == 200) {
      return _searchAddresses(response, postCode);
    } else {
      throw Exception('Failed to load addresses');
    }
  }

  ///This should be done on the server side but due to the use of a basic custom JSON server
  /// I'll manually do the search here
  List<Address> _searchAddresses(Response response, String postCode) {
    Map<String, dynamic> body = json.decode(response.body);
    var addresses = body[postCode];
    if (addresses != null) {
      addresses = (addresses as List).map((a) => Address.fromJson(a)).toList();
      return addresses;
    }
    return null;
  }

  // ignore: missing_return
  Widget _createField(String name, IconData iname) {
    return TextFormField(
      scrollPadding: EdgeInsets.all(20),
      onSaved: (val) => _paymentAddress.addressLine = val,
      controller: _addressLineController,
      decoration: InputDecoration(
        ///border: new OutlineInputBorder(
        //   borderSide: new BorderSide(color: Colors.black)),
        labelText: name,
        icon: Icon(iname),
        suffixIcon: IconButton(
          onPressed: () => _addressLineController.clear(),
          icon: Icon(Icons.clear),
        ),
      ),
    );
  }

  Widget _createFieldDropDown(String name, IconData iname) {
    return DropdownButtonFormField<String>(
      onSaved: (val) => _cardDetails.expiryMonth = val,
      value: expiryMonth,
      items: [
        'hamadi hawari',
        'ibrahim ben ahmed',
      ].map<DropdownMenuItem<String>>(
        (String val) {
          return DropdownMenuItem(
            child: Text(val),
            value: val,
          );
        },
      ).toList(),
      onChanged: (val) {
        setState(() {
          expiryMonth = val;
        });
      },
      decoration: InputDecoration(
        labelText: name,
        icon: Icon(iname),
      ),
    );
  }

  Widget _createFieldDate(String name, IconData iname) {
    return TextFormField(
      onSaved: (val) => _paymentAddress.addressLine = val,
      controller: _addressLineController,
      decoration: InputDecoration(
        labelText: name,
        icon: Icon(iname),
        suffixIcon: IconButton(
          onPressed: () => _createDatePicker(),
          icon: Icon(Icons.clear),
        ),
      ),
    );
  }

  Widget _createDatePicker() {
    return Container(
      height: 50,
      child: CupertinoDatePicker(
        mode: CupertinoDatePickerMode.date,
        initialDateTime: DateTime(1969, 1, 1),
        onDateTimeChanged: (DateTime newDateTime) {
          // Do something
        },
      ),
    );
  }
}
