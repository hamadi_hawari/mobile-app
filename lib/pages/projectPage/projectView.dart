import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/pages/formManager/dynamic_textfields.dart';
import 'package:deiser_app/pages/formManager/formView.dart';
import 'package:deiser_app/pages/formManager/dropDownOptions.dart';
import 'package:deiser_app/pages/projectPage/addProjectWidget.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'addProjectViewV2.dart';
import 'projectPresenter.dart';
import 'package:deiser_app/mvp/mvpView.dart';

import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'projectCellWidget.dart';

/// This is the stateless widget that the main application instantiates.
String user_role = 'admin';
String userId = '';
String username = '';

class ProjectPageView extends MVPView {
  // This widget is the root of your application.
  final ProjectPagePresenter _presenter;

  // Constructor
  ProjectPageView(this._presenter) : super(presenter: _presenter);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(65.0), // here the desired height
          child: AppBar(
            elevation: 2.0,
            backgroundColor: Color(0xffec6500),
            title: Text('Bauvorhaben',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 20.0)),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                ),
              )
            ],
          )),
      body: new MyHomePage(),
    );
  }
}
// Constructor

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

Future<void> getrole() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  user_role = prefs.getString("role");
  userId = prefs.getString("username");
  DocumentSnapshot variable =
      await Firestore.instance.collection('users').document(userId).get();
  username = variable.data['username'];
  print(username);
}

List<String> nameDocs = [];
StreamSubscription nameSub;

class _MyHomePageState extends State<MyHomePage> {
  final ValueNotifier<String> name = ValueNotifier<String>("");
  TextEditingController search = new TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nameDocs = [];
    nameSub =
        Firestore.instance.collection('FormValues').snapshots().listen((data) {
      data.documentChanges.forEach((x) {
        setState(() {
          nameDocs.add(x.document.documentID);
        });
      });
    });
    setState(() {
      getrole();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StaggeredGridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 12.0,
      mainAxisSpacing: 12.0,
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
      children: <Widget>[
        Container(
            width: 50.0,
            child: Row(
              children: [
                new Flexible(
                  child: TextField(
                    controller: search,
                    onChanged: (value) {
                      name.value = value;
                    },
                    decoration: InputDecoration(
                        icon: Icon(Icons.search),
                        // border: new OutlineInputBorder(
                        //    borderSide:
                        //       new BorderSide(color: Colors.white)),
                        hintText: 'Suchen '),
                  ),
                ),
                if (user_role == 'admin')
                  IconButton(
                    icon: new Icon(Icons.add_box_sharp),
                    iconSize: 45,
                    onPressed: () => Navigator.of(context)
                        .push(MaterialPageRoute(builder: (_) => Fillform()
                            // ProjectScreen()
                            )),
                    color: Color(0xffec6500),
                  )
              ],
            )),
        DefaultTabController(
          length: 3,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                child: TabBar(
                    indicatorColor: Colors.orange,
                    labelColor: Colors.black,
                    labelStyle: TextStyle(fontSize: 12),
                    tabs: [
                      Tab(
                        text: "Alle",
                      ),
                      Tab(text: "Offen"),
                      Tab(text: "Abgeschlossen"),

                      // Tab(text: "Geschlossen"),
                    ]),
              ),
              Container(
                //Add this to give height
                height: MediaQuery.of(context).size.height,
                child: TabBarView(children: [
                  Container(
                    child: ValueListenableBuilder(
                      valueListenable: name,
                      builder: (BuildContext context, value, Widget child) {
                        return StreamBuilder<QuerySnapshot>(
                            stream: (value.toString() != "" &&
                                    value.toString() != null)
                                ? Firestore.instance
                                    .collection('Projects')
                                    .where('name',
                                        isGreaterThanOrEqualTo:
                                            value.toString())
                                    .where('name',
                                        isLessThan: value.toString() + 'z')
                                    .snapshots()
                                : Firestore.instance
                                    .collection("Projects")
                                    .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<QuerySnapshot> snapshot) {
                              if (!snapshot.hasData) return new Text("");
                              return ListView(
                                itemExtent: 150,
                                children:
                                    snapshot.data.documents.map((document) {
                                  return Center(
                                    child: Container(
                                      width:
                                          MediaQuery.of(context).size.width / 1,
                                      child: document.data['users'][username] ==
                                                  false &&
                                              user_role != 'admin'
                                          ? null
                                          : cell(context, document, user_role,
                                              nameDocs),
                                    ),
                                  );
                                }).toList(),
                              );
                            });
                      },
                    ),
                  ),
                  Container(
                    child: ValueListenableBuilder(
                      valueListenable: name,
                      builder: (BuildContext context, value, Widget child) {
                        return StreamBuilder<QuerySnapshot>(
                            stream: (value.toString() != "" &&
                                    value.toString() != null)
                                ? Firestore.instance
                                    .collection('Projects')
                                    .where('name',
                                        isGreaterThanOrEqualTo:
                                            value.toString())
                                    .where('name',
                                        isLessThan: value.toString() + 'z')
                                    .where('status', isEqualTo: 'open')
                                    .snapshots()
                                : Firestore.instance
                                    .collection("Projects")
                                    .where('status', isEqualTo: 'open')
                                    .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<QuerySnapshot> snapshot) {
                              if (!snapshot.hasData) return new Text("");
                              return ListView(
                                itemExtent: 185,
                                children:
                                    snapshot.data.documents.map((document) {
                                  return Center(
                                    child: Container(
                                      width:
                                          MediaQuery.of(context).size.width / 1,
                                      child: document.data['users'][username] ==
                                                  false &&
                                              user_role != 'admin'
                                          ? null
                                          : cell(context, document, user_role,
                                              nameDocs),
                                    ),
                                  );
                                }).toList(),
                              );
                            });
                      },
                    ),
                  ),
                  Container(
                    child: ValueListenableBuilder(
                      valueListenable: name,
                      builder: (BuildContext context, value, Widget child) {
                        return StreamBuilder<QuerySnapshot>(
                            stream: (value.toString() != "" &&
                                    value.toString() != null)
                                ? Firestore.instance
                                    .collection('Projects')
                                    .where('name',
                                        isGreaterThanOrEqualTo:
                                            value.toString())
                                    .where('name',
                                        isLessThan: value.toString() + 'z')
                                    .where('status', isEqualTo: 'pending')
                                    .snapshots()
                                : Firestore.instance
                                    .collection("Projects")
                                    .where('status', isEqualTo: 'pending')
                                    .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<QuerySnapshot> snapshot) {
                              if (!snapshot.hasData) return new Text("");
                              return ListView(
                                itemExtent: 185,
                                children:
                                    snapshot.data.documents.map((document) {
                                  return Center(
                                    child: Container(
                                      width:
                                          MediaQuery.of(context).size.width / 1,
                                      child: document.data['users'][username] ==
                                                  false &&
                                              user_role != 'admin'
                                          ? null
                                          : cell(context, document, user_role,
                                              nameDocs),
                                    ),
                                  );
                                }).toList(),
                              );
                            });
                      },
                    ),
                  ),
                  /* Container(
                    child: ValueListenableBuilder(
                      valueListenable: name,
                      builder: (BuildContext context, value, Widget child) {
                        return StreamBuilder<QuerySnapshot>(
                            stream: (value.toString() != "" &&
                                    value.toString() != null)
                                ? Firestore.instance
                                    .collection('Projects')
                                    .where('name',
                                        isGreaterThanOrEqualTo:
                                            value.toString())
                                    .where('name',
                                        isLessThan: value.toString() + 'z')
                                    .where('status', isEqualTo: 'closed')
                                    .snapshots()
                                : Firestore.instance
                                    .collection("Projects")
                                    .where('status', isEqualTo: 'closed')
                                    .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<QuerySnapshot> snapshot) {
                              if (!snapshot.hasData) return new Text("");
                              return ListView(
                                itemExtent: 185,
                                children:
                                    snapshot.data.documents.map((document) {
                                  return Center(
                                    child: Container(
                                      width:
                                          MediaQuery.of(context).size.width / 1,
                                      child: document.data['users'][username] ==
                                                  false &&
                                              user_role != 'admin'
                                          ? null
                                          : cell(context, document, user_role),
                                    ),
                                  );
                                }).toList(),
                              );
                            });
                      },
                    ),
                  ),*/
                ]),
              ),
            ],
          ),
        ),
        //ProjectCellItem(),
        // ProjectCellItem(
        //   name: search.text,
        //  ),
      ],
      staggeredTiles: [
        StaggeredTile.extent(2, 50.0),
        StaggeredTile.extent(2, 15000.0),
        //StaggeredTile.extent(2, 500.0),
        // StaggeredTile.extent(2, 150.0),
      ],
    ));
  }

  Widget _buildTile(Widget child, {Function() onTap}) {
    return Material(
        elevation: 5.0,
        color: Colors.white,
        borderRadius: BorderRadius.circular(12.0),
        shadowColor: Color(0x802196F3),
        child: InkWell(
            // Do onTap() if it isn't null, otherwise do print()
            onTap: onTap != null
                ? () => onTap()
                : () {
                    print('Not set yet');
                  },
            child: child));
  }
}
