import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/pages/adminDashboard/lastActivities.dart';
import 'package:deiser_app/pages/formManager/FormCellWidget.dart';
import 'package:deiser_app/pages/projectPage/editProjectView.dart';
import 'package:deiser_app/services/projectServices.dart';
import 'package:deiser_app/services/userServices.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

enum WhyFarther { harder, smarter, selfStarter, tradingCharter }
String user_role;

class ProjectCell extends StatefulWidget {
  @override
  _ProjectCellState createState() => _ProjectCellState();
}

Future<void> getrole() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();

  user_role = prefs.getString("role");
}

class _ProjectCellState extends State<ProjectCell> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getrole();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      scrollDirection: Axis.vertical,
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      children: <Widget>[
        Container(
            margin: EdgeInsets.symmetric(vertical: 16.0, horizontal: 54.0),
            child: Material(
              //elevation: 8.0,
              color: Colors.white,
              borderRadius: BorderRadius.circular(32.0),
            )),
        // ProjectCellItem(),
      ],
    ));
  }
}

class ProjectCellItem extends StatelessWidget {
  final String name;

  const ProjectCellItem({Key key, this.name}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    //print('name hereeeeeeeeeee $name');
  }
}

Widget cell(BuildContext context, DocumentSnapshot project, String userRole,
    List<String> nameDocs) {
  //getrole();

  int cal = 0;
  int trueforms = 0;
  List<String> verifform = [];
  projectForms = project.data['forms'];
  bool checkClosed = true;
  for (var x in project.data['forms'].keys) {
    if ((projectForms[x] == true) &&
        (nameDocs.any((element) => element.contains(project.documentID + x)) ==
            true)) {
      cal++;
      print(project.documentID + x);
    }
    if ((projectForms[x] == true) &&
        (nameDocs.any((element) => element.contains(project.documentID + x)) ==
            false)) {
      checkClosed = false;
    }
  }

  if (checkClosed == false) {
    ProjectManager().updateProjectstatues('open', project.documentID);
  }
  if (checkClosed == true) {
    ProjectManager().updateProjectstatues('pending', project.documentID);
  }
  int forms = 0;
  for (var v in project.data['forms'].values) {
    if (v == true) {
      forms++;
    }
  }
  int users = 0;
  for (var v in project.data['users'].values) {
    if (v == true) {
      users++;
    }
  }
  ;
  return GestureDetector(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => FormCell(
              project: project,
              userRole: user_role,
            ),
          ));
    },

    child: Container(
        height: 150,
        child: Card(
          margin: EdgeInsets.all(5),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    /*SizedBox(
                  height: 10,
                ),*/
                    SizedBox(
                        width: 345,
                        child: Row(
                          children: [
                            SizedBox(
                                width: 270,
                                child: Text('  ' + project.data['name'],
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 20.0))),
                            if (userRole == 'admin')
                              SizedBox(
                                width: 30,
                                child: Material(
                                  color: Colors.white,
                                  shadowColor: Colors.white,
                                  child: IconButton(
                                    icon: new Icon(Icons.edit_rounded),
                                    color: Colors.orange,
                                    onPressed: () => Navigator.of(context)
                                        .push(MaterialPageRoute(
                                            builder: (_) => EditProject(
                                                  name: project.documentID,
                                                )
                                            // ProjectScreen()
                                            )),
                                  ),
                                ),
                              ),
                            if (userRole == 'admin')
                              SizedBox(
                                width: 30,
                                child: Material(
                                  color: Colors.white,
                                  shadowColor: Colors.white,
                                  child: IconButton(
                                    icon:
                                        new Icon(Icons.delete_forever_rounded),
                                    color: Colors.orange,
                                    onPressed: () {
                                      showAlertDialog(
                                          context, project.documentID);
                                    },
                                  ),
                                ),
                              ),
                          ],
                        )),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('   ' + forms.toString(),
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w700,
                                fontSize: 14.0)),
                        Text(' Formulare    ',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w200,
                                fontSize: 14.0)),
                        Text(' ' + users.toString(),
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w700,
                                fontSize: 14.0)),
                        Text(' Mitarbeiter   ',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w200,
                                fontSize: 14.0)),
                        ButtonTheme.bar(
                          child: ButtonBar(
                            children: <Widget>[
                              if (checkClosed == true)
                                FlatButton(
                                  color: Color.fromRGBO(50, 205, 50, 0.13),
                                  child: Text('Abgeschlossen',
                                      style: TextStyle(
                                          color:
                                              Color.fromRGBO(50, 205, 50, 1))),
                                  onPressed: () {
                                    ProjectManager().getDocs();
                                  },
                                ),
                              if (project.data['status'] == 'closed not used')
                                FlatButton(
                                  color: Color.fromRGBO(255, 256, 0, 0.13),
                                  child: Text('Geschlossen',
                                      style: TextStyle(
                                          color:
                                              Color.fromRGBO(235, 94, 108, 1))),
                                  onPressed: () {
                                    ProjectManager().getDocs();
                                  },
                                ),
                              if (checkClosed == false)
                                FlatButton(
                                  color: Color.fromRGBO(255, 165, 0, 0.13),
                                  child: Text('Offen',
                                      style:
                                          TextStyle(color: Colors.orange[400])),
                                  onPressed: () {
                                    ProjectManager().getDocs();
                                  },
                                ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ]),
        )),

    /// Item image
  );

  /// Review
}

valueLineContent(String val, String val2) {
  final baseColor = PdfColors.cyan;

  return pw.Expanded(
      child: pw.Column(children: [
    pw.Container(
      alignment: pw.Alignment.centerLeft,
      padding: const pw.EdgeInsets.only(bottom: 2),
      child: pw.Text(
        val,
        style: pw.TextStyle(
          color: baseColor,
          fontSize: 20,
        ),
      ),
    ),
    pw.Container(
      alignment: pw.Alignment.centerLeft,
      padding: const pw.EdgeInsets.only(bottom: 2),
      child: pw.Text(
        val2,
        style: pw.TextStyle(
          color: PdfColors.black,
          fontSize: 16,
        ),
      ),
    ),
  ]));
}

Future<void> _createPDF(String projectName) async {
  //Create a PDF document.
  var documents = pw.Document();

  final Future<QuerySnapshot> documentfireStore =
      Firestore.instance.collection("FormValues").getDocuments();
  await documentfireStore.then<dynamic>((QuerySnapshot snapshot) async {
    for (int i = 0; i < snapshot.documents.length; i++) {
      if (snapshot.documents[i].documentID.contains(projectName)) {
        //Add page and draw text to the page.

        documents.addPage(pw.Page(
          build: (pw.Context context) => pw.Center(
            child: pw.Row(
                crossAxisAlignment: pw.CrossAxisAlignment.start,
                children: [
                  pw.Expanded(
                      child: pw.Column(children: [
                    // for (var i = 0; i < 2; i++)
                    for (int j = 0;
                        j <
                            Map<String, String>.from(
                                    snapshot.documents[i].data["data"])
                                .length;
                        j++)
                      valueLineContent(
                          Map<String, String>.from(
                                  snapshot.documents[i].data["data"])
                              .keys
                              .elementAt(j)
                              .toString(),
                          snapshot.documents[i].data["data"].values
                              .elementAt(j)
                              .toString())
                  ])),
                ]),
          ),
        ));
      }
    }
  });

  //Save the document

  Directory directory = await getExternalStorageDirectory();
  String path = directory.path;
//Get directory path
  final file = File('$path/$projectName.pdf');
  await file.writeAsBytes(await documents.save());
//Open the PDF document in mobile
  OpenFile.open('$path/$projectName.pdf');
}

showAlertDialog(BuildContext context, String id) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      ProjectManager().delete(id);
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    content: Text("Are you sure ?"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
