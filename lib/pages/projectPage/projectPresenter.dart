import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
// MVP
import 'package:deiser_app/mvp/mvpPresenter.dart';
import 'projectModel.dart';
import 'projectView.dart';

class ProjectPagePresenter extends MVPPresenter {
  final ProjectPageModel _model;

  ProjectPagePresenter(this._model) : super('License');

  static ProjectPageView build(dynamic item, {Widget appLogo}) =>
      ProjectPageView(ProjectPagePresenter(ProjectPageModel(item: item)));

  Map<String, dynamic> get item => _model.item;
}
