import 'package:deiser_app/pages/formManager/field.dart';
import 'package:flutter/material.dart';
import 'package:im_stepper/stepper.dart';

class IconStepperDemo extends StatefulWidget {
  @override
  _IconStepperDemo createState() => _IconStepperDemo();
}

class _IconStepperDemo extends State<IconStepperDemo> {
  // MUST BE MAINTAINED, SEPARATELY.
  int currentIndex = 0;
  List<Field> fields = [
    Field(
        name: 'name',
        type: FlatButton(
          color: Colors.blueGrey,
          child: Text('next'),
        )),
    Field(
        name: 'field',
        type: DropdownButtonFormField<String>(
          value: 'text field',
          items: [
            'text field',
            'drop down menu',
            'check box',
          ].map<DropdownMenuItem<String>>(
            (String val) {
              return DropdownMenuItem(
                child: Text(val),
                value: val,
              );
            },
          ).toList(),
          decoration: InputDecoration(
            labelText: 'type',
          ),
        ))
  ];
  // THESE MUST BE USED TO CONTROL THE STEPPER FROM EXTERNALLY.
  bool goNext = false;
  bool goPrevious = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Fill FORM'),
        ),
        body: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Colors.blueGrey,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    spreadRadius: 1.0,
                    blurRadius: 2.0,
                  )
                ],
                // borderRadius: BorderRadius.circular(5.0),
              ),
              child: IconStepper.externallyControlled(
                goNext: goNext,
                goPrevious: goPrevious,
                direction: Axis.horizontal,
                stepColor: Colors.white,
                activeStepColor: Colors.amber,
                lineColor: Colors.amberAccent,
                lineLength: 75,
                steppingEnabled: true,
                icons: [
                  Icon(Icons.looks_one),
                  Icon(Icons.looks_two),
                  Icon(Icons.looks_3),
                  Icon(Icons.looks_4),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(width: 0.1),
              ),
              padding: EdgeInsets.all(8.0),
              alignment: Alignment.centerLeft,
              child: Text(header()),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(5.0),
                child: FittedBox(
                  child: Center(
                    child: Text('${currentIndex + 1}'),
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RaisedButton(
                  color: Colors.red[400],
                  onPressed: () {
                    // MUST TO CONTROL STEPPER FROM EXTERNAL BUTTONS.
                    setState(() {
                      goNext = false;
                      goPrevious = true;

                      if (currentIndex > 0) {
                        currentIndex--;
                      }
                    });
                  },
                  child: Text('Previous'),
                ),
                RaisedButton(
                  color: Colors.blue[300],
                  onPressed: () {
                    // MUST TO CONTROL STEPPER FROM EXTERNAL BUTTONS.
                    setState(() {
                      goNext = true;
                      goPrevious = false;

                      if (currentIndex < 3) {
                        currentIndex++;
                      }
                    });
                  },
                  child: Text('Next'),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  String header() {
    switch (currentIndex) {
      case 0:
        return 'Form Field 1';

      case 1:
        return 'Form Field 2';

      case 2:
        return 'Form Field 3';

      case 3:
        return 'Form Field 4';

      default:
        return 'Unknown';
    }
  }
}
