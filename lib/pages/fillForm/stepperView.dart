import 'package:flutter/material.dart';

import 'package:im_stepper/stepper.dart';

class StepperPage extends StatefulWidget {
  @override
  _IconStepperDemo createState() => _IconStepperDemo();
}

class _IconStepperDemo extends State<StepperPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('fill form'),
        ),
        body: Row(
          children: <Widget>[
            Container(
              // margin: EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                color: Colors.blueGrey,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    spreadRadius: 1.0,
                    blurRadius: 2.0,
                  )
                ],
                // borderRadius: BorderRadius.circular(5.0),
              ),
              child: IconStepper(
                direction: Axis.vertical,
                // enableNextPreviousButtons: false,
                stepColor: Colors.white,
                activeStepColor: Colors.amber,
                lineColor: Colors.amberAccent,
                // lineDotRadius: 2,
                lineLength: 75,
                onStepReached: (value) {
                  setState(() {
                    print('value: $value');
                    selectedIndex = value;
                  });
                },
                steppingEnabled: true,
                icons: [
                  Icon(Icons.looks_one),
                  Icon(Icons.looks_two),
                  Icon(Icons.looks_3),
                  Icon(Icons.looks_4),
                  Icon(Icons.looks_5),
                  Icon(Icons.looks_6),
                  Icon(Icons.looks_6),
                  Icon(Icons.looks_two),
                  Icon(Icons.looks_two),
                  Icon(Icons.looks_two),
                  Icon(Icons.looks_two),
                  Icon(Icons.looks_two),
                  Icon(Icons.call_end),
                  Icon(Icons.block),
                  Icon(Icons.mail),
                  Icon(Icons.face),
                  Icon(Icons.fast_forward),
                  Icon(Icons.gamepad),
                ],
              ),
            ),
            Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(width: 0.1),
                  ),
                  padding: EdgeInsets.all(8.0),
                  alignment: Alignment.centerLeft,
                  child: Text(header()),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.all(5.0),
                    child: FittedBox(
                      child: Center(
                        child: Text('${selectedIndex + 1}'),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [],
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  String header() {
    switch (selectedIndex) {
      case 0:
        return 'Form Field 1';

      case 1:
        return 'Form Field 2';

      case 2:
        return 'Form Field 3';

      case 3:
        return 'Form Field 4';

      case 4:
        return 'Specially abled';

      case 5:
        return 'Personal Details';

      case 6:
        return 'Social Details';

      case 7:
        return 'Review';

      default:
        return 'Unknown';
    }
  }
}
