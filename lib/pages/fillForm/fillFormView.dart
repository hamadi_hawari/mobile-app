import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/mvp/mvpView.dart';
import 'package:deiser_app/pages/adminDashboard/adminDashboardView.dart';
import 'package:deiser_app/pages/formManager/field.dart';
import 'package:deiser_app/pages/formManager/sumbitsView.dart';
import 'package:deiser_app/pages/projectPage/projectPresenter.dart';
import 'package:deiser_app/pages/projectPage/projectView.dart';
import 'package:deiser_app/services/formServices.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:deiser_app/pages/formManager/dynamic_textfields.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather/weather.dart';

Map<String, bool> valuesForms = {};
bool checker = false;
String vall;
String sectionIndex = '0';

class Fillform extends MVPView {
  // This widget is the root of your application.

  final DocumentSnapshot form;
  final String projectName;
  const Fillform({Key key, this.form, this.projectName}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Color(0xffec6500),
        title: new Text(removeTrailing(form.documentID)),
      ),
      body: new MyHomePage(
        title: form.documentID,
        form: form,
        projectName: projectName,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.form, this.projectName})
      : super(key: key);
  final DocumentSnapshot form;
  final String title;
  final String projectName;

  @override
  _MyHomePageState createState() => new _MyHomePageState(form, projectName);
}

class _MyHomePageState extends State<MyHomePage> {
  final String projectName;
  int itemcount;
  int _currentstep = 0;
  final DocumentSnapshot form;
  List<Step> spr = List<Step>();
  Map<String, dynamic> newmap = {};
  String choice = '';
  final PageController controller = PageController();
  _MyHomePageState(this.form, this.projectName);
  Map<String, String> dataEntry = Map<String, String>();
  String loc = "";
  String weather = "";

  @override
  void initState() {
    super.initState();
    getadress();
  }

  Future<void> getadress() async {
    Location location = new Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return '';
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return '';
      }
    }

    _locationData = await location.getLocation();
    final coordinates =
        new Coordinates(_locationData.latitude, _locationData.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    WeatherFactory wf = WeatherFactory('287bc553b993eec3e0dd318e88ae4225');

    Weather w = await wf.currentWeatherByLocation(
        _locationData.latitude, _locationData.longitude);
    setState(() {
      dataEntry["icon"] = w.weatherIcon;
      loc = addresses.first.addressLine;
      weather = 'main : ' +
          w.weatherMain +
          '\n' +
          'desc :  ' +
          w.weatherDescription +
          '\n' +
          'temp: ' +
          w.temperature.toString() +
          '\n' +
          'humidity: ' +
          w.humidity.toString() +
          '\n' +
          'cloud:  ' +
          w.cloudiness.toString() +
          '\n' +
          'wind:  ' +
          w.windSpeed.toString();
    });
  }

  void savedata() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      // var map1 = Map.fromIterable(spr,
      //  key: (e) => e., value: (e) => e.field.typeName);
      DateTime now = DateTime.now();
      dataEntry["Benutzer"] = prefs.getString('username');
      dataEntry['status'] = 'submited';
      dataEntry['Datum'] = now.day.toString() +
          '/' +
          now.month.toString() +
          '/' +
          now.year.toString();

      dataEntry['Uhrzeit'] = now.hour.toString() + '.' + now.minute.toString();
      dataEntry['Standort'] = loc;
      dataEntry['Wetter'] = weather;
      FormManager().fillFormDoc(
          dataEntry,
          projectName +
              form.documentID +
              '+' +
              dataEntry["Benutzer"] +
              dataEntry['Uhrzeit'],
          projectName,
          form.documentID);
      ProjectPagePresenter pr;
      valuesForms = {};
      Navigator.of(context, rootNavigator: true).pop('dialog');
    });
  }

  String selected;
  @override
  Widget build(BuildContext context) {
    double c_width = MediaQuery.of(context).size.width * 0.8;
    double c_height = MediaQuery.of(context).size.height * 0.8;

    ScrollPhysics phy = new NeverScrollableScrollPhysics();
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          body: new Theme(
              data: ThemeData(
                  primaryColor: Colors.orange,
                  colorScheme: ColorScheme.light(primary: Colors.orange)),
              child: PageView.builder(
                  // Changes begin here
                  physics: phy,
                  controller: controller,
                  scrollDirection: Axis.horizontal,
                  //itemCount: itemcount,
                  itemBuilder: (context, position) {
                    Map<String, dynamic> document = form.data['data'][0];
                    Map<String, dynamic> documentorder = form.data['orders'][0];
                    Map<String, dynamic> documentrequired =
                        form.data['requiredField'][0];

                    Map<String, dynamic> sections = form.data['sections'][0];
                    int baseSection = sections.values
                        .toList()
                        .where((item) => item.contains("0"))
                        .length;
                    int secondSection = sections.values
                        .toList()
                        .where((item) => item.contains("2"))
                        .length;

                    int firstSection = sections.values
                        .toList()
                        .where((item) => item.contains("1"))
                        .length;

                    var counter = sections.values.toList();
                    counter.retainWhere((element) => element == '0');
                    itemcount = counter.length + 4;
                    newmap = {};
                    List<String> sortedKeys = documentorder.keys
                        .toList(growable: false)
                          ..sort((k1, k2) =>
                              documentorder[k1].compareTo(documentorder[k2]));

                    Map<String, dynamic> sortedMap = new Map.fromIterable(
                        sortedKeys,
                        key: (k) => k,
                        value: (k) => documentorder[k]);
                    sortedMap.forEach((k, v) => {newmap[k] = document[k]});
                    print(newmap);
                    //int progressBar = (form.data['data'][0].length) - 2;
                    TextEditingController datatext;
                    return Scaffold(
                        body: Column(children: [
                      SizedBox(
                        height: 20,
                      ),
                      /*   LinearProgressIndicator(
                                        valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
                                        value: (position + 1) / itemcount,
                                      ),*/
                      Align(
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 56, left: 20, right: 20, bottom: 50),
                          alignment: Alignment.topCenter,
                          child: (newmap.values.elementAt(position) != 'text')
                              ? Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                        width: c_width,
                                        padding: EdgeInsets.only(
                                            top: 20,
                                            left: 0,
                                            right: 0,
                                            bottom: 80),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Flexible(
                                              child: Text(
                                                removeTrailing(newmap.keys
                                                    .elementAt(position)),
                                                maxLines: 20,
                                                style: TextStyle(
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ),
                                            if (documentrequired[newmap.keys
                                                    .elementAt(position)] ==
                                                true)
                                              Text(
                                                ' *',
                                                style: TextStyle(
                                                    color: Colors.red),
                                              )
                                          ],
                                        )),
                                  ],
                                )
                              : null,
                        ),
                      ),
                      if (newmap.values.elementAt(position) == 'text')
                        Container(
                            padding: EdgeInsets.only(
                                top: 55, left: 20, right: 20, bottom: 55),
                            child: Text(
                              removeTrailing(newmap.keys.elementAt(position)),
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold),
                            )),
                      if (newmap.values.elementAt(position) == 'check box')
                        Expanded(
                            child: ListTile(
                          title: multiplechoices(
                              form, newmap.keys.elementAt(position)),
                        )
                            //new Text(' ${document.values.elementAt(position)}'),
                            ),
                      if (newmap.values.elementAt(position) == 'drop down')
                        Expanded(
                            child: ListTile(
                          title:
                              dropdown(form, newmap.keys.elementAt(position)),
                        )
                            //new Text(' ${document.values.elementAt(position)}'),
                            ),
                      if (newmap.values.elementAt(position) == 'date picker')
                        Expanded(
                          child: Container(
                            height: 200,
                            child: CupertinoDatePicker(
                              mode: CupertinoDatePickerMode.date,
                              initialDateTime: DateTime(2020, 1, 1),
                              onDateTimeChanged: (DateTime newDateTime) {
                                // Do something
                                String formattedDate =
                                    "${newDateTime.day}/${newDateTime.month}/${newDateTime.year}";

                                dataEntry[newmap.keys.elementAt(position)] =
                                    formattedDate.toString();
                              },
                            ),
                          ),
                          //new Text(' ${document.values.elementAt(position)}'),
                        ),
                      if (newmap.values.elementAt(position) == 'text field')
                        Expanded(
                            child: Container(
                          padding: EdgeInsets.only(
                              top: 56, left: 20, right: 20, bottom: 50),
                          child: TextFormField(
                            controller: datatext = new TextEditingController(),
                            onChanged: (val) {
                              dataEntry[newmap.keys.elementAt(position)] = val;
                            },
                            validator: (val) =>
                                val.length > 3 ? null : 'Field name is invalid',
                            decoration: InputDecoration(
                              labelText:
                                  dataEntry[newmap.keys.elementAt(position)],
                              hintText: 'Enter your Answer',
                              isDense: true,
                            ),
                          ),
                        )

                            //new Text(' ${document.values.elementAt(position)}'),
                            ),
                      if (newmap.values.elementAt(position) == 'section')
                        Expanded(
                            child: ListTile(
                          title: sectiondropdown(
                              form, newmap.keys.elementAt(position), itemcount),
                        )
                            //new Text(' ${document.values.elementAt(position)}'),
                            ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(
                            top: 5, left: 20, right: 20, bottom: 15),
                        child: Stack(
                          alignment: FractionalOffset.bottomCenter,
                          children: <Widget>[
                            if (position != 0)
                              Align(
                                alignment: Alignment.bottomLeft,
                                child: FloatingActionButton(
                                  heroTag: "btn1",
                                  onPressed: () {
                                    //vall = null;
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());

                                    if ((sectionIndex == "2") &&
                                        (position ==
                                            baseSection + firstSection)) {
                                      controller.jumpToPage(baseSection - 1);
                                    } else {
                                      controller.previousPage(
                                          duration: Duration(milliseconds: 50),
                                          curve: Curves.linear);
                                    }
                                  },
                                  child: Icon(Icons.arrow_back_ios),
                                  backgroundColor: Colors.orange[500],
                                ),
                              ),
                            if ((form.data['data'][0].length) != position + 1)
                              Align(
                                alignment: Alignment.bottomRight,
                                child: FloatingActionButton(
                                  heroTag: "btn2",
                                  onPressed: () {
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());

                                    if ((documentrequired[newmap.keys
                                                .elementAt(position)] ==
                                            true) &&
                                        ((dataEntry[newmap.keys
                                                    .elementAt(position)] ==
                                                null) ||
                                            (dataEntry[newmap.keys
                                                    .elementAt(position)] ==
                                                ''))) {
                                    } else {
                                      vall = null;
                                      if (newmap.values.elementAt(position) ==
                                          'section') {
                                        sectionIndex = (form.data[newmap.keys
                                                        .elementAt(position)]
                                                    .indexOf(choice) +
                                                1)
                                            .toString();
                                        if (sectionIndex == "2") {
                                          controller.jumpToPage(
                                              (baseSection) + firstSection);
                                          print((baseSection) + firstSection);
                                        } else {
                                          controller.nextPage(
                                              duration:
                                                  Duration(milliseconds: 50),
                                              curve: Curves.linear);
                                        }
                                        //itemcount = 2;
                                      } else {
                                        controller.nextPage(
                                            duration:
                                                Duration(milliseconds: 50),
                                            curve: Curves.linear);
                                      }
                                    }
                                  },
                                  child: Icon(Icons.arrow_forward_ios),
                                  backgroundColor: Colors.orange[500],
                                ),
                              ),
                            //if ((form.data['data'][0].length) == position + 1)
                            if (baseSection + firstSection == position + 1)
                              Align(
                                alignment: Alignment.bottomRight,
                                child: FloatingActionButton(
                                  heroTag: "btn3",
                                  onPressed: () {
                                    setState(() {
                                      savedata();
                                    });
                                  },
                                  child: Icon(Icons.save),
                                  backgroundColor: Colors.orange[500],
                                ),
                              ),
                            if (baseSection + firstSection + secondSection ==
                                position + 1)
                              Align(
                                alignment: Alignment.bottomRight,
                                child: FloatingActionButton(
                                  heroTag: "btn4",
                                  onPressed: () {
                                    setState(() {
                                      savedata();
                                    });
                                  },
                                  child: Icon(Icons.save),
                                  backgroundColor: Colors.orange[500],
                                ),
                              ),
                          ],
                        ),
                      ))
                    ]));
                  })),
        ));
  }

  void itemChange(bool val, int index, valuesForms) {
    setState(() {
      valuesForms = val;
    });
  }

  Widget formbuilder(Map<String, dynamic> documentrequired, int position,
      Map<String, dynamic> sections) {
    return Scaffold(
        body: Column(children: [
      SizedBox(
        height: 20,
      ),
      LinearProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
        value: (position + 1) / itemcount,
      ),
      Align(
        child: Container(
          padding: EdgeInsets.only(top: 56, left: 20, right: 20, bottom: 50),
          alignment: Alignment.topCenter,
          child: (newmap.values.elementAt(position) != 'text')
              ? Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(newmap.keys.elementAt(position)),
                    if (documentrequired[newmap.keys.elementAt(position)] ==
                        true)
                      Text(
                        ' *',
                        style: TextStyle(color: Colors.red),
                      )
                  ],
                )
              : null,
        ),
      ),
      if (newmap.values.elementAt(position) == 'text')
        Container(
            padding: EdgeInsets.only(top: 55, left: 20, right: 20, bottom: 55),
            child: Text(
              newmap.keys.elementAt(position),
              style: TextStyle(fontSize: 22),
            )),
      if (newmap.values.elementAt(position) == 'check box')
        Expanded(
            child: ListTile(
          title: multiplechoices(form, newmap.keys.elementAt(position)),
        )
            //new Text(' ${document.values.elementAt(position)}'),
            ),
      if ((newmap.values.elementAt(position) == 'drop down') &&
          (sections[newmap.keys.elementAt(position)] == '0'))
        Expanded(
            child: ListTile(
          title: dropdown(form, newmap.keys.elementAt(position)),
        )
            //new Text(' ${document.values.elementAt(position)}'),
            ),
      if ((newmap.values.elementAt(position) == 'date picker') &&
          (sections[newmap.keys.elementAt(position)] == sectionIndex))
        Expanded(
          child: Container(
            height: 200,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              initialDateTime: DateTime(2020, 1, 1),
              onDateTimeChanged: (DateTime newDateTime) {
                // Do something
                dataEntry[newmap.keys.elementAt(position)] =
                    newDateTime.toString();
              },
            ),
          ),
          //new Text(' ${document.values.elementAt(position)}'),
        ),
      if ((newmap.values.elementAt(position) == 'text field') &&
          (sections[newmap.keys.elementAt(position)] == sectionIndex))
        Expanded(
            child: Container(
          padding: EdgeInsets.only(top: 56, left: 20, right: 20, bottom: 50),
          child: TextFormField(
            controller: new TextEditingController(),
            onChanged: (val) {
              dataEntry[newmap.keys.elementAt(position)] = val;
            },
            validator: (val) => val.length > 3 ? null : 'Field name is invalid',
            decoration: InputDecoration(
              labelText: 'answer',
              hintText: 'Enter your Answer',
              isDense: true,
            ),
          ),
        )

            //new Text(' ${document.values.elementAt(position)}'),
            ),
      if (newmap.values.elementAt(position) == 'section')
        Expanded(
            child: ListTile(
          title:
              sectiondropdown(form, newmap.keys.elementAt(position), itemcount),
        )
            //new Text(' ${document.values.elementAt(position)}'),
            ),
      Container(
        padding: EdgeInsets.only(top: 5, left: 20, right: 20, bottom: 5),
        child: Stack(
          children: <Widget>[
            if (position != 0)
              Align(
                alignment: Alignment.bottomLeft,
                child: FloatingActionButton(
                  heroTag: "btn1",
                  onPressed: () {
                    setState(() {
                      vall = null;
                      controller.previousPage(
                          duration: Duration(milliseconds: 50),
                          curve: Curves.linear);
                    });
                  },
                  child: Icon(Icons.arrow_back_ios),
                  backgroundColor: Colors.orange[500],
                ),
              ),
            if ((form.data['data'][0].length) != position + 1)
              Align(
                alignment: Alignment.bottomRight,
                child: FloatingActionButton(
                  heroTag: "btn2",
                  onPressed: () {
                    setState(() {
                      // print(documentrequired[newmap.keys.elementAt(position)]);
                      if ((documentrequired[newmap.keys.elementAt(position)] ==
                              true) &&
                          (dataEntry[newmap.keys.elementAt(position)] ==
                              null)) {
                      } else {
                        vall = null;
                        if (newmap.values.elementAt(position) == 'section') {
                          sectionIndex = (form
                                      .data[newmap.keys.elementAt(position)]
                                      .indexOf(choice) +
                                  1)
                              .toString();

                          print(newmap);
                          print('section index $sectionIndex');
                          //itemcount = 2;
                        }
                        controller.nextPage(
                            duration: Duration(milliseconds: 50),
                            curve: Curves.linear);
                      }
                    });
                  },
                  child: Icon(Icons.arrow_forward_ios),
                  backgroundColor: Colors.orange[500],
                ),
              ),
            //if ((form.data['data'][0].length) == position + 1)
            if (int.parse(sectionIndex) + 2 == position + 1)
              Align(
                alignment: Alignment.bottomRight,
                child: FloatingActionButton(
                  heroTag: "btn3",
                  onPressed: () {
                    setState(() {
                      savedata();
                    });
                  },
                  child: Icon(Icons.save),
                  backgroundColor: Colors.orange[500],
                ),
              ),
          ],
        ),
      )
    ]));
  }

  Widget dropdown(DocumentSnapshot form, String field) {
    print(vall);

    return StatefulBuilder(
      builder: (context, _setState) => new DropdownButton<String>(
        value: vall,
        isExpanded: true,
        items: form.data[field]
            .map<DropdownMenuItem<String>>(
                (value) => new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    ))
            .toList(),
        onChanged: (_val) {
          dataEntry[field] = _val;
          setState(() {
            vall = _val;
          });
        },
      ),
    );
  }

  Widget sectiondropdown(DocumentSnapshot form, String field, int itemcount) {
    //print(vall);

    return StatefulBuilder(
      builder: (context, _setState) => new DropdownButton<String>(
        value: vall,
        isExpanded: true,
        items: form.data[field]
            .map<DropdownMenuItem<String>>(
                (value) => new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    ))
            .toList(),
        onChanged: (_val) {
          dataEntry[field] = _val;
          setState(() {
            vall = _val;

            sectionIndex = (form.data[field].indexOf(_val) + 1).toString();
            // newmap.remove('text section two');
            choice = vall;
            print(itemcount);
          });
        },
      ),
    );
  }

  String removeTrailing(String name) {
    if (name.contains('_')) {
      return name.substring(0, name.indexOf('_'));
    } else
      return name;
  }

  Widget multiplechoices(DocumentSnapshot form, String field) {
    return new ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: form.data[field].length,
        itemBuilder: (BuildContext context, int index) {
          //print('reloading check values $index');
          //valuesForms[form.data[field][index]] = true;
          return new Card(
            child: new Container(
              padding: new EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  StatefulBuilder(
                      builder: (context, _setState) => new CheckboxListTile(
                          activeColor: Colors.pink[300],
                          dense: true,
                          //font change
                          title: new Text(
                            form.data[field][index],
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5),
                          ),
                          value: valuesForms[form.data[field][index]] ??= false,
                          onChanged: (bool val) {
                            setState(() {
                              valuesForms[form.data[field][index]] = val;
                              if ((valuesForms[form.data[field][index]] ==
                                      true) &&
                                  (dataEntry[field] != null))
                                dataEntry[field] = dataEntry[field] +
                                    '\n' +
                                    form.data[field][index];
                              else if ((valuesForms[form.data[field][index]] ==
                                      true) &&
                                  (dataEntry[field] == null))
                                dataEntry[field] = form.data[field][index];
                              else if ((valuesForms[form.data[field][index]] ==
                                      false) &&
                                  (dataEntry[field] != null))
                                dataEntry[field] = dataEntry[field]
                                    .replaceAll(form.data[field][index], ' ');

                              print(dataEntry[field]);
                            });
                          }))
                ],
              ),
            ),
          );
        });
  }
}

class FromState with ChangeNotifier {
  double _progress = 0;
  Field _selected;

  final PageController controller = PageController();

  get progress => _progress;
  get selected => _selected;

  set progress(double newValue) {
    _progress = newValue;
    notifyListeners();
  }

  set selected(Field newValue) {
    _selected = newValue;
    notifyListeners();
  }

  void nextPage() async {
    await controller.nextPage(
      duration: Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }
}
