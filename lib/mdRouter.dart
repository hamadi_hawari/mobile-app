import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:deiser_app/application.dart';
//import 'package:deiser_app/localization.dart';
import 'package:deiser_app/myFirebase.dart';

import 'package:deiser_app/routes.dart';
import 'package:pedantic/pedantic.dart';
//import 'package:flare_flutter/flare_actor.dart';

class MDRouter extends StatefulWidget {
  final bool isAuthenticated;
  MDRouter({@required this.isAuthenticated}) {
    final router = Router(
      routerDelegate: null,
    );
    Routes.configureRoutes(router);
    Application.router = router;
  }
  //void RunSplashFirst() {
  //  Center(
  //    child: FlareActor(
  //      'assets/animations/maybe2.flr',
  //      alignment: Alignment.center,
  //      fit: BoxFit.contain,
  //      animation: 'animation2',
  //    ),
  //  );
  //}

  @override
  MDRouterState createState() => MDRouterState();
}

class MDRouterState extends State<MDRouter> {
  MDRouterState() {
    Application.localeChanged = UpdateApp;
    for (Locale locale in Application.supportedLocales) {
      //unawaited(AppLocalizations.load(locale));
    }
  }

  void UpdateApp() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: <LocalizationsDelegate<dynamic>>[
        /*  AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate*/
      ],
      /* locale: Application.store.state.locale,
      supportedLocales: Application.supportedLocales,
      navigatorObservers: <NavigatorObserver>[MyFirebase.observer],
      home: widget.isAuthenticated ? MainPagePresenter.build() : LoginPresenter.build(),
      onGenerateRoute: Application.router.generator,
      onGenerateTitle: (context) => AppLocalizations.of(context).title,*/
    );
  }
}
