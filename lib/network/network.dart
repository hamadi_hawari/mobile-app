import 'dart:async';

import 'package:deiser_app/redux/reduxModel.dart';
import 'package:redux/redux.dart';

class Network {
  static final Network _singleton = Network._internal();
  Store<ReduxModel> store;

  factory Network() {
    return _singleton;
  }

  Network._internal();

  void handleTimeout() {}

  Timer _lastUpdateDataStart;
  Future<void> updateDataStart(bool force) async {
    // NOTE (zarka_j) debounce otherwise
    if (force == true ||
        _lastUpdateDataStart == null ||
        _lastUpdateDataStart.isActive == false) {
      _lastUpdateDataStart = Timer(const Duration(minutes: 1), handleTimeout);

/*
      store.dispatch(UpdateAllDataDone(projects...));
      store.dispatch(UpdatedeiserData(true));*/
    }
  }

  Timer _lastUpdatedeiserDataStart;
  Future<void> updatedeiserDataStart(bool force) async {
    // NOTE (zarka_j) debounce otherwise
    if (force == true ||
        _lastUpdatedeiserDataStart == null ||
        _lastUpdatedeiserDataStart.isActive == false) {
      _lastUpdatedeiserDataStart =
          Timer(const Duration(minutes: 1), handleTimeout);
/*
      Map<String, dynamic> deiserData = await deiserIOAPI.getdeiserData();
      Map<String, dynamic> deiserFilters = await deiserIOAPI.getdeiserFilters();
      store.dispatch(UpdatedeiserDataDone(deiserData));
      store.dispatch(UpdateFilters(deiserFilters));*/
    }
  }
}
