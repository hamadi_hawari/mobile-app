import 'dart:ui';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:deiser_app/application.dart';
import 'package:deiser_app/redux/roles.dart';

class ReduxModel {
  // Attributes

  final List<String> roles;
  final Map<String, dynamic> userProfile;

  // Constructors
  ReduxModel({
    this.roles = const <String>[Roles.Admin],
    this.userProfile = const <String, dynamic>{},
  });

  // Methods
  ReduxModel copyWith({
    List<String> roles,
    bool termsAccepted,
    Map<String, dynamic> userProfile,
  }) =>
      ReduxModel(
        roles: roles ?? this.roles,
        userProfile: userProfile ?? this.userProfile,
      );

  static Future<void> dropStorage() async {
    FlutterSecureStorage secureStorage = FlutterSecureStorage();
    await secureStorage.deleteAll();
    await Application.persisted.save(ReduxModel());
  }

  static ReduxModel fromJson(dynamic jsonData) {
    if (jsonData == null) {
      return ReduxModel();
    }
    try {
      Locale locale = Locale(jsonData['locale']);
      Intl.defaultLocale = Intl.canonicalizedLocale(locale.toString());
      return ReduxModel(
        roles: jsonData.containsKey('roles')
            ? List<String>.from(jsonData['roles'])
            : <String>[Roles.Admin],
      );
    } catch (SerializationException) {
      print('>>> Model Deserialization error ' +
          SerializationException.toString());
      dropStorage();
      return ReduxModel();
    }
  }

  dynamic toJson() => <String, dynamic>{
        'roles': roles,
      };
}
