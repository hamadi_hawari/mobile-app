import 'package:flutter/material.dart';

class UpdateLocale {
  final Locale _locale;

  UpdateLocale(this._locale);

  Locale get locale => _locale;
}

class UpdateTermsDone {
  final String _termsOfUsage;

  UpdateTermsDone(this._termsOfUsage);

  String get termsOfUsage => _termsOfUsage;
}

class UpdatePrivacyPolicyDone {
  final String _pp;

  UpdatePrivacyPolicyDone(this._pp);

  String get privacyPolicy => _pp;
}

class UpdateTerms {
  final bool _force;

  UpdateTerms(this._force);

  bool get force => _force;
}

class UpdatePrivacyPolicy {
  final bool _force;

  UpdatePrivacyPolicy(this._force);

  bool get force => _force;
}
