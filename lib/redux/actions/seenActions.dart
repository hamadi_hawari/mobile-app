class UpdateSeenValue {
  // Attributes
  final bool _force;

  // Constructors
  UpdateSeenValue(this._force);

  // Getters
  bool get force => _force;
}

class UpdateSeenValueDone {
  // Attributes
  final bool _seen;

  // Constructors
  UpdateSeenValueDone(this._seen);

  // Getters
  bool get seen => _seen;
}
