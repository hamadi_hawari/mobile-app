class UpdateDataSharingOptions {
  // Attributes
  final Map<String, dynamic> _dataSharingOptions;

  // Constructors
  UpdateDataSharingOptions(this._dataSharingOptions);

  // Getters
  Map<String, dynamic> get dataSharingOptions => _dataSharingOptions;
}
