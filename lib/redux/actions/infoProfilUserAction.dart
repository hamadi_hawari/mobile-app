class UpdateInfoUser {
  // Attributes
  final dynamic _value;
  final dynamic _field;
  final Map<String, dynamic> _oldUserProfile;
  Map<String, dynamic> _newUserProfile;
  // Constructors
  UpdateInfoUser(this._field, this._value, this._oldUserProfile) {
    if (_oldUserProfile == null || _oldUserProfile.isEmpty) {
      _newUserProfile = <String, dynamic>{};
    } else {
      _newUserProfile = _oldUserProfile;
    }
    _newUserProfile[_field] = _value;
  }
  // Getters
  Map<String, dynamic> get userProfile => _newUserProfile;
}
