class UpdateTheme {
// Atributes
  final bool _themeDark;

// Constructos
  UpdateTheme(this._themeDark);

// Getters
  bool get themeChanged => _themeDark;
}
