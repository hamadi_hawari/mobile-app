class UpdateShownValueDone {
  // Attributes
  final bool _termsMustBeDisplayed;

  // Constructors
  UpdateShownValueDone(this._termsMustBeDisplayed);

  // Getters
  bool get termsMustBeDisplayed => _termsMustBeDisplayed;
}

class UpdateShownValue {
  // Attributes
  final bool _force;

  // Constructors
  UpdateShownValue(this._force);

  // Getters
  bool get force => _force;
}

class UpdateAcceptedValueDone {
  // Attributes
  final bool _termsAccepted;

  // Constructors
  UpdateAcceptedValueDone(this._termsAccepted);

  // Getters
  bool get termsAccepted => _termsAccepted;
}

class UpdateAcceptedValue {
  // Attributes
  final bool _force;

  // Constructors
  UpdateAcceptedValue(this._force);

  // Getters
  bool get force => _force;
}
