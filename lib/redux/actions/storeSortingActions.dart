class UpdateStoreSorting {
// Atributes
  final String _sortingType;

// Constructos
  UpdateStoreSorting(this._sortingType);

// Getters
  String get storeSorting => _sortingType;
}
