class UpdateRatingApp {
// Atributes
  final bool _rateAppMustBeDisplayed;

// Constructos

  UpdateRatingApp(this._rateAppMustBeDisplayed);

// Getters
  bool get rateAppMustBeDisplayed => _rateAppMustBeDisplayed;
}

class UpdateTimeRating {
// Attributes
  final String _dateTimeForRating;

// Constructos
  UpdateTimeRating(this._dateTimeForRating);

// Getters
  String get dateTimeForRating => _dateTimeForRating;
}
