class AddNotificationsData {
  // Attributes
  final dynamic _data;
  final List<dynamic> _oldDatas;
  List<dynamic> _newDatas;

  // Constructors
  AddNotificationsData(this._data, this._oldDatas) {
    if (_oldDatas == null || _oldDatas.isEmpty) {
      _newDatas = <Map<String, dynamic>>[];
    } else {
      _newDatas = _oldDatas;
    }
    _newDatas.add(_data);
  }

  // Getters
  List<dynamic> get newDatas => _newDatas;
}

class CleanNotificationsDatas {}

class UpdateNotificationAlerts {
// Atributes
  final bool _alerts;

// Constructos
  UpdateNotificationAlerts(this._alerts);

// Getters
  bool get alerts => _alerts;
}

class UpdateNotificationOffers {
// Atributes
  final bool _offers;

// Constructos
  UpdateNotificationOffers(this._offers);

// Getters
  bool get offers => _offers;
}

class UpdateNotificationStats {
// Atributes
  final bool _stats;

// Constructos
  UpdateNotificationStats(this._stats);

// Getters
  bool get stats => _stats;
}
