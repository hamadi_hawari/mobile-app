import 'package:deiser_app/redux/roles.dart';

class UpdateDiagnostics {
  final bool _force;
  UpdateDiagnostics(this._force);
  bool get force => _force;
}

class UpdateRole {
  final List<String> _roles;
  UpdateRole(this._roles);
  List<String> get roles => _roles ?? <String>[Roles.Base];
}

class UpdateApps {
  final List<dynamic> _apps;
  UpdateApps(this._apps);
  List<dynamic> get apps => _apps;
}

class UpdateLastLocation {
  final Map<String, dynamic> _lastLocation;
  UpdateLastLocation(this._lastLocation);
  Map<String, dynamic> get lastLocation => _lastLocation;
}

class UpdateAllDataDone {
  final Map<String, dynamic> _lastLocation;

  UpdateAllDataDone(this._lastLocation);

  Map<String, dynamic> get lastLocation => _lastLocation;
}

class UpdateAllData {
  final bool _force;
  UpdateAllData(this._force);
  bool get force => _force;
}

class UpdateDataSharingOptionsStart {
  final bool _force;
  UpdateDataSharingOptionsStart(this._force);
  bool get force => _force;
}

class ResetApplication {
  ResetApplication();
}
