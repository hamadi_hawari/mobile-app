import 'dart:io';

import 'package:mailer2/mailer.dart';

class MailManager {
  MailManager(String userMail, String password) {
    // for an arbitrary SMTP server, go with `new SmtpOptions()`.

    var options = GmailSmtpOptions()
      ..username = 'deiser.app@gmail.com'
      ..password = '2GSJ45DS';

    // Create our email transport.
    var emailTransport = SmtpTransport(options);

    // Create our mail/envelope.
    var envelope = new Envelope()
      ..recipients.add(userMail)
      ..subject = 'You account is created (Deiser team)'
      ..html = '<p>Dear employee</p>' +
          '<p>your new deiser io account has been created , welcome to the team !</p>' +
          '<p>from now on , please log in to your account using your email address: <b> $userMail </b> and your password <b> $password </b> (we recommend you change it) </p>' +
          '<p>to have access to more roles and functionalities please contact our admin at<b> admin@deiser.de</b>  </p>' +
          'if you have any questions or concerns please contact our support team : +45613843546  <br>' +
          ' <br><br><br> <img src="https://api.globaldatabase.com/logo/www.deiser.de/"  width="200"height="120">';

    // Email it.
    emailTransport.send(envelope);
  }
}
