import 'dart:async';
import 'dart:core';

//import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:deiser_app/application.dart';
import 'package:deiser_app/config/config.dart';
import 'package:deiser_app/mdRouter.dart';
import 'package:deiser_app/network/network.dart';
import 'package:deiser_app/redux/reducer.dart';
import 'package:deiser_app/redux/reduxModel.dart';
import 'package:pedantic/pedantic.dart';
import 'package:redux/redux.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';

//import 'network/bluetooth.dart';
//import 'network/loginManager.dart';

void mainEntryPoint() async {
  print('Environment: ${Config.instance.environment}');

  //unawaited(MockServer().launch());

  final Persistor<ReduxModel> persisted = Persistor<ReduxModel>(
    //debug: true,
    storage: FlutterStorage(key: 'my-app'),
    serializer: JsonSerializer<ReduxModel>(ReduxModel.fromJson),
  );
  Application.persisted = persisted;

  ReduxModel initialState;
  try {
    initialState = await persisted.load();
  } catch (SerializationException) {
    print(
        '>>> Model Deserialization error ' + SerializationException.toString());
    await ReduxModel.dropStorage();
    initialState = ReduxModel();
  }

  //final Store<ReduxModel> store = Store<ReduxModel>(
  // reducer,
  //  initialState: initialState ?? ReduxModel(),
  //  middleware: <Middleware<ReduxModel>>[persisted.createMiddleware()],
  //);
  //Application.store = store;

  final Network dn = Network();
  // dn.store = store;

  FlutterError.onError = (FlutterErrorDetails details) {
    if (Config.instance.environment == 'Development') {
      FlutterError.dumpErrorToConsole(details);
    } else {
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  // Crashlytics.instance.enableInDevMode = true;
  FlutterError.onError = (FlutterErrorDetails details) {
    if (Config.instance.environment == 'Development') {
      FlutterError.dumpErrorToConsole(details);
    } else {
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
    // Crashlytics.instance.recordFlutterError(details);
  };

  // FIXME - check if vehicle is already scanned and do not perform again
  // Start bluetooth scanner

  //Bluetooth();

  //bool isAuthenticated = await LoginManager().tryAutoLogin();
  await SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
  ]).then((_) {
    runZoned<Future<Null>>(() async {
      // runApp(
      //    MDRouter(isAuthenticated: isAuthenticated),
      //  );
    });
  });
}
