// Flutter
import 'package:flutter/material.dart';

class ProjectsCardView extends StatelessWidget {
  // Attributes
  final Widget icon;
  final Widget info;
  final Widget label;

  // Constructor
  ProjectsCardView({this.icon, this.info, this.label});

  // Methods
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints cons) => Card(
        elevation: 1.0,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment(-0.9, -0.8),
              child: icon,
            ),
            Align(
              alignment: Alignment(0.9, -0.5),
              child: info,
            ),
            Align(alignment: Alignment(0.0, 1.0), child: label),
          ],
        ),
      ),
    );
  }
}
