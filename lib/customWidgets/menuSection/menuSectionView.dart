// Flutter
import 'package:flutter/material.dart';
// styles

class MenuSectionView extends StatelessWidget {
  // Attributes
  final Widget child;
  final String title;

  // Constructor
  MenuSectionView({@required this.title, @required this.child});

  // Methods
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        FractionallySizedBox(
          widthFactor: 0.95,
          child: Text(
            title ?? '',
          ),
        ),
        child ?? Container(),
      ],
    );
  }
}
