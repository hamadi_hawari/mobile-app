abstract class Config {
  static Config _instance;

  Config();

  factory Config.assign(Config specializedConfig) {
    _instance = _instance ?? specializedConfig;
    return _instance;
  }

  static Config get instance => _instance;

  String get playID => 'deiser_app';
  String get storeID => '2147483647';

  String get environment;
  Map<String, String> get deiserConnect;
  Map<String, String> get deiser;
}
