import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/Animation/FadeAnimation.dart';
import 'package:deiser_app/pages/userDashboard/userDashboardView.dart';
import 'package:flutter/material.dart';
import 'package:deiser_app/pages/adminDashboard/adminDashboardView.dart';
import 'package:deiser_app/pages/adminDashboard/adminDashboardPresenter.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePageMain(),
    ));

class MyHomePageMain extends StatefulWidget {
  MyHomePageMain({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePageMain> {
  AdminDashboardPresenter _presenter;
  dynamic data;
  String username;
  String password;
  String role;
  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  Future<dynamic> getData(String email) async {
    final DocumentReference document =
        Firestore.instance.collection("users").document(email);

    await document.get().then<dynamic>((DocumentSnapshot snapshot) async {
      // print(snapshot.data['password']) ;
      setState(() {
        username = snapshot.data['email'];
        password = snapshot.data['password'];
        role = snapshot.data['role'];
      });
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffec6500), //(236, 101, 0, 1.0),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              scale: 0.6,
              image: ExactAssetImage('assets/images/logo_deiser.png'),
              fit: BoxFit.scaleDown,
              alignment: Alignment.topCenter),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 180,
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  /*FadeAnimation(
                      1,
                      Text(
                        "Login",
                        style: TextStyle(color: Colors.white, fontSize: 40),
                      )),*/
                  SizedBox(
                    height: 85,
                  ),
                ],
              ),
            ),
            // SizedBox(height: 20),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(60),
                        topRight: Radius.circular(60))),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.all(30),
                    child: Column(
                      children: <Widget>[
                        //     SizedBox(
                        //      height: 60,
                        //    ),
                        FadeAnimation(
                            1.4,
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color.fromRGBO(225, 95, 27, .3),
                                        blurRadius: 20,
                                        offset: Offset(0, 10))
                                  ]),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Colors.grey[200]))),
                                    child: TextField(
                                      controller: usernameController,
                                      decoration: InputDecoration(
                                          hintText: "Email ",
                                          hintStyle:
                                              TextStyle(color: Colors.grey),
                                          border: InputBorder.none),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Colors.grey[200]))),
                                    child: TextField(
                                      controller: passwordController,
                                      onChanged: (value) {
                                        getData(usernameController.text
                                            .toLowerCase());
                                      },
                                      decoration: InputDecoration(
                                          hintText: "Password",
                                          hintStyle:
                                              TextStyle(color: Colors.grey),
                                          border: InputBorder.none),
                                      obscureText: true,
                                    ),
                                  ),
                                ],
                              ),
                            )),
                        SizedBox(
                          height: 15,
                        ),

                        FadeAnimation(
                            1.6,
                            Padding(
                                padding: EdgeInsets.only(top: 45),
                                child: Container(
                                  height: 50,
                                  margin: EdgeInsets.symmetric(horizontal: 50),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: Color(0xffec6500)),
                                  child: Center(
                                    child: FlatButton(
                                      textColor: Colors.white,
                                      onPressed: () async {
                                        print(username.toLowerCase());
                                        print(usernameController.text
                                            .toLowerCase());
                                        if ((username.toLowerCase() ==
                                                usernameController.text
                                                    .toLowerCase()) &&
                                            (password ==
                                                passwordController.text)) {
                                          SharedPreferences prefs =
                                              await SharedPreferences
                                                  .getInstance();

                                          await prefs.setString(
                                              'username', username);
                                          await prefs.setString('role', role);

                                          if (role == 'admin') {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      AdminDashboard(),
                                                ));
                                          } else {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      UserDashboard(),
                                                ));
                                          }
                                        } else {
                                          showAlertDialog(context);
                                        }
                                      },
                                      child: Text("login"),
                                    ),
                                  ),
                                ))),
                        SizedBox(
                          height: 10,
                        ),
                        FadeAnimation(
                            1.5,
                            Text(
                              "Forgot Password?",
                              style: TextStyle(color: Colors.grey),
                            )),
                        SizedBox(
                          height: 40,
                        ),

                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

showAlertDialog(BuildContext context) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {},
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Wrong credentials"),
    content: Text("insert correct username and password"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
