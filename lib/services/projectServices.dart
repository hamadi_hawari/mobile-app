import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/pages/projectPage/project.dart';

class ProjectManager {
  ProjectManager();

  Future getDocs() async {
    QuerySnapshot querySnapshot =
        await Firestore.instance.collection("Projects").getDocuments();
    print("inside func ${querySnapshot.documents.length}");

    for (int i = 0; i < querySnapshot.documents.length; i++) {
      var a = querySnapshot.documents[i];

      print(a.documentID);
      print(a.data);
    }
  }

  Future<void> createProjectDoc(
      Project project, var users, var forms, String id) async {
    final Firestore _firestore = Firestore.instance;

    DocumentReference ref = _firestore.collection('Projects').document(id);

    return ref.setData({
      'name': project.name,
      'projectOwner': project.projectOwnerId,
      'teamLeader': project.teamLeaderId,
      'post': project.post,
      'address': project.address,
      'zip': project.zip,
      'street': project.street,
      'startDate': project.startDate,
      'endDate': project.endDate,
      'client': project.client,
      'status': project.status,
      'artOberflaeche': project.artOberflaeche,
      'ausfuehrenderBetrieb': project.ausfuehrenderBetrieb,
      'baustellenart': project.baustellenart,
      'breiteNachschnitt': project.breiteNachschnitt,
      'erfNaschschnittSeiten': project.erfNaschschnittSeiten,
      'grabenbreite': project.grabenbreite,
      'grabenlaenge': project.grabenlaenge,
      'maximaleUnebenheit': project.maximaleUnebenheit,
      'wiederherstllungsbreite': project.wiederherstllungsbreite,
      'forms': forms,
      'users': users
    });
  }

  Future<void> updateProjectDoc(Project project, Map<String, bool> users,
      Map<String, bool> forms, String id) async {
    final Firestore _firestore = Firestore.instance;

    DocumentReference ref = _firestore.collection('Projects').document(id);
    // Firestore.instance.collection('Projects').document(id).updateData(
    //     {'forms': FieldValue.delete(), 'users': FieldValue.delete()});
    ref.updateData({
      'name': project.name,
      'projectOwner': project.projectOwnerId,
      'teamLeader': project.teamLeaderId,
      'post': project.post,
      'address': project.address,
      'zip': project.zip,
      'street': project.street,
      'startDate': project.startDate,
      'endDate': project.endDate,
      'client': project.client,
      'status': project.status,
      'artOberflaeche': project.artOberflaeche,
      'ausfuehrenderBetrieb': project.ausfuehrenderBetrieb,
      'baustellenart': project.baustellenart,
      'breiteNachschnitt': project.breiteNachschnitt,
      'erfNaschschnittSeiten': project.erfNaschschnittSeiten,
      'grabenbreite': project.grabenbreite,
      'grabenlaenge': project.grabenlaenge,
      'maximaleUnebenheit': project.maximaleUnebenheit,
      'wiederherstllungsbreite': project.wiederherstllungsbreite,
      'forms': forms,
      'users': users
    });
  }

  Future<void> updateProjectstatues(String status, String id) async {
    final Firestore _firestore = Firestore.instance;

    DocumentReference ref = _firestore.collection('Projects').document(id);
    // Firestore.instance.collection('Projects').document(id).updateData(
    //     {'forms': FieldValue.delete(), 'users': FieldValue.delete()});
    ref.updateData({
      'status': status,
    });
  }

  Future<void> delete(String id) async {
    final Firestore _firestore = Firestore.instance;

    DocumentReference ref = _firestore.collection('Projects').document(id);
    return ref.delete();
  }
}
