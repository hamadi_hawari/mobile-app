import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/pages/formManager/dynamicForm.dart';

class FormManager {
  FormManager();

  Future<void> createFormDoc(
      var form,
      String id,
      Map<String, List<String>> drops,
      var orders,
      var requiredField,
      var sections) async {
    final Firestore _firestore = Firestore.instance;
    var updated_forms = [];
    DocumentReference ref = _firestore.collection('Forms').document(id);
    ref.updateData({
      'data': FieldValue.delete(),
      'orders': FieldValue.delete(),
      'requiredField': FieldValue.delete(),
      'sections': FieldValue.delete()
    });

    ref.setData({
      'name': id,
    });
    //print('here fields ${drops.values}');
    drops.forEach((key, value) {
      // print('here fields inside ${value}');
      ref.updateData({
        key: value,
      });
    });

    return ref.updateData({
      'data': FieldValue.arrayUnion([form]),
      'orders': FieldValue.arrayUnion([orders]),
      'requiredField': FieldValue.arrayUnion([requiredField]),
      'sections': FieldValue.arrayUnion([sections]),
    });
  }

  Future<void> fillFormDoc(
      var form, String id, String project, String formname) async {
    final Firestore _firestore = Firestore.instance;
    //print(form);
    DocumentReference refP =
        _firestore.collection('Projects').document(project);
    DocumentReference ref = _firestore.collection('FormValues').document(id);
    ref.setData({});
    refP.updateData({
      'status': 'open',
    });
    return ref.updateData({
      'project': project,
      'form': formname,
      'data': form,
    });
  }

  Future<void> updateFormDoc(
      var form,
      String id,
      Map<String, List<String>> drops,
      var orders,
      var requiredFrom,
      var sections) async {
    final Firestore _firestore = Firestore.instance;

    DocumentReference ref = _firestore.collection('Forms').document(id);
    ref.delete();
    ref.setData({
      'name': id,
    });
    drops.forEach((key, value) {
      ref.updateData({
        key: value,
      });
    });
    return ref.updateData({
      'data': FieldValue.arrayUnion([form]),
      'orders': FieldValue.arrayUnion([orders]),
      'requiredField': FieldValue.arrayUnion([requiredFrom]),
      'sections': FieldValue.arrayUnion([sections]),
    });
  }

  Future<void> delete(String id) async {
    final Firestore _firestore = Firestore.instance;
    print("hereeee dem");
    DocumentReference ref = _firestore.collection('Forms').document(id);

    var snapshots = _firestore.collection('Projects').snapshots();
    ref.delete();
    await snapshots.forEach((snapshot) async {
      List<DocumentSnapshot> documents = snapshot.documents;
      for (var document in documents) {
        await document.reference
            .updateData({'forms.$id': FieldValue.delete()}).whenComplete(() {
          print('Field Deletedfdddddd');
        });
      }
    });
    return ref.delete();
  }

  Future<void> deletesubmit(String id) async {
    final Firestore _firestore = Firestore.instance;

    DocumentReference ref = _firestore.collection('FormValues').document(id);

    return ref.delete();
  }

  Future<void> deleteaffected(String formname, DocumentSnapshot project) async {
    final Firestore _firestore = Firestore.instance;

    DocumentReference ref =
        _firestore.collection('Projects').document(project.documentID);
    var valuesForms = project.data['forms'];
    valuesForms[formname] = false;
    return ref.updateData({
      'forms': valuesForms,
    });
    ;
  }
}
