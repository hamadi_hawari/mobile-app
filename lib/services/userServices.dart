import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deiser_app/pages/formManager/user.dart';

class UserAccountManager {
  UserAccountManager();

  Future<bool> login(String username, String password) async {
    final DocumentReference document =
        Firestore.instance.collection("users").document('admin');

    await document.get().then<dynamic>((DocumentSnapshot snapshot) async {
      if ((username = snapshot.data['username']) &&
          (password = snapshot.data['password'])) {
        return true;
      }
    });
    return false;
  }

  Future<void> createUserDoc(User user, String id) async {
    final Firestore _firestore = Firestore.instance;

    DocumentReference ref =
        _firestore.collection('users').document(id.toLowerCase());

    return ref.setData({
      'name': id,
      'username': user.fullName,
      'password': user.password,
      'role': user.role,
      'email': user.email.toLowerCase(),
    });
  }

  Future<void> updateUserDoc(User user, String id) async {
    final Firestore _firestore = Firestore.instance;
    print(user.fullName);
    DocumentReference ref = _firestore.collection('users').document(id);
    return ref.updateData({
      'name': id,
      'username': user.fullName,
      'password': user.password,
      'role': user.role,
      'email': user.email,
    });
  }

  Future<void> delete(String role, String user) async {
    final Firestore _firestore = Firestore.instance;

    DocumentReference ref = _firestore.collection('users').document(role);
    var snapshots = _firestore.collection('Projects').snapshots();
    ref.delete();
    await snapshots.forEach((snapshot) async {
      List<DocumentSnapshot> documents = snapshot.documents;
      for (var document in documents) {
        await document.reference
            .updateData({'users.$user': FieldValue.delete()}).whenComplete(() {
          print('ffffField Deleted');
        });
      }
    });
    return ref.delete();
  }

  void passwordRecover() {}
}
