#!/bin/bash
set -e
[ -e test_driver ] || exit 0

if [ $1 = 'ios' ] ; then
  cd ios
  pod update
  ulimit -S -n 2048
  cd ..
fi
trim()
{
  local trimmed=""

  read -r trimmed
  # Strip leading space.
  trimmed="${trimmed## }"
  # Strip trailing space.
  trimmed="${trimmed%% }"

  echo "${trimmed}"
}

TARGET_PLATFORM="${1}"
EMULATOR_ID="$(flutter devices | grep "${TARGET_PLATFORM}" | awk -F' • ' '{print $2}' | trim)"
FILES="$(ls test/integration | grep -v _test.dart)"

for FILE in ${FILES}; do
  if [ "${TARGET_PLATFORM}" == "ios" ]; then
    echo "-> Killing all iOS simulators"
    killall "Simulator" 2> /dev/null || :
    xcrun simctl erase all || :
    echo "-> Waiting for a simulator to be re-instanciated"
    # The simulator is automatically relaunched from ~/Library/LaunchAgent by build user
    while ! flutter devices | grep "${TARGET_PLATFORM}"; do sleep 1; done
    EMULATOR_ID="$(flutter devices | grep "${TARGET_PLATFORM}" | awk -F' • ' '{print $2}' | trim)"
    echo "-> Waiting for a simulator to be booted"
    while ! xcrun simctl openurl "${EMULATOR_ID}" "https://munic.io" 2>/dev/null; do sleep 1; done
  fi
  flutter drive -d "${EMULATOR_ID}" --target="test_driver/${FILE}"
done
