import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  const Duration longTimeout = Duration(seconds: 30);

  group('Munic.io:', () {
    final SerializableFinder loginButton = find.byType('RaisedButton');
    FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });
    test('Splash: Wait for transition', () async {
      await driver.waitUntilNoTransientCallbacks(timeout: longTimeout);
    });
    test('AuthenticationHome: Press login button', () async {
      await driver.tap(loginButton, timeout: longTimeout);
    });
  });
}
