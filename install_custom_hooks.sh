#!/bin/bash -e

CURRENT_DIRECTORY="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)")"
CUSTOM_HOOK_DIRECTORY="${CURRENT_DIRECTORY}/custom_hooks/"
HOOKS_DIRECTORY="${CURRENT_DIRECTORY}/../.git/hooks"

cd "${CUSTOM_HOOK_DIRECTORY}"
cp "pre-commit" "${HOOKS_DIRECTORY}"
