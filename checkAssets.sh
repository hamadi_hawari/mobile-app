#!/bin/bash
verbose='false'
[ "$1" == '-v' ] && export verbose='true'

checkAsset() {
  local file=${1//\/\//\/}
  if grep -rniq $file lib ; then
    [ "$verbose" = 'true' ] && echo -e "\033[32mcan keep $file\033[0m" 1>&2
    return 0
  else
    if [ "$verbose" = 'true' ] ; then
      echo -e "\033[33mcould remove $file\033[0m" 1>&2
    fi
  fi
  return 1
}
checkGroupAsset() {
  local group=${1}
  local fileListRem=()
  local fileListKeep=()
  for file in $(find $group -type f -maxdepth 1) ; do
    [[ $file =~ ^ass ]] || continue
    if ! checkAsset $file ; then
      fileListRem=($fileListRem $file)
    else
      file=${file//\/\//\/}
      fileListKeep=($fileListKeep $file)
    fi
  done
  if [ "${fileListRem[0]}" != '' ] ; then
    if [ "${fileListKeep[0]}" = '' ] ; then
      echo -e "\033[31m>>> Folder $group is not used, remove it\033[0m" 1>&2
    else
      echo -e "\033[31m>>> Folder $group is not fully included, please add only\033[0m" 1>&2
      for x in "${fileListKeep[@]}" ; do
        echo -- "- $x"
      done
      echo -e "\033[31m<<< done\033[0m" 1>&2
    fi
  fi
}
for assetGroup in $(grep -- '^[ \t]*- assets/' pubspec.yaml | sed 's@^.* assets/@assets/@') ; do
  [[ $assetGroup =~ /licences/ ]] && continue
  case $assetGroup in
    */)
      checkGroupAsset $assetGroup
      ;;
    *)
      checkAsset "$assetGroup" || echo $file 1>&2
      ;;
  esac
done
