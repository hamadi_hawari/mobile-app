#!/bin/bash

shopt -s expand_aliases

DEPENDENCIES_FILE="./assets/dependencies/framework_dependencies.csv"

function command_exists()
{
  command -v "${1}" &>/dev/null
}

if command_exists gsed; then
  alias sed='gsed'
fi

# checks if we display all the dependencies used
flutter pub pub deps -s compact | tail -n +6  | sed '/dev dependencies/q' | grep -v "dev dependencies:" | awk '{print $1 , $2 , $3}' | grep -v ' flutter ' | awk '{print $2}' | while read line ; do
  if [ ! -z "$line" ]; then
    dependencie="$(cat $DEPENDENCIES_FILE | grep "$line" )"
    if [ -z "$dependencie" ]; then
      echo -e "\033[31m$line licence is missing\033[0m" 1>&2
      #exit 1
    fi
  fi
done

# checks if we display dependencies which are not used
all_used_dependencies="$(flutter pub pub deps -s compact | tail -n +6  | sed '/dev dependencies/q' | grep -v "dev dependencies:" | awk '{print $1 , $2 , $3}' | grep -v ' flutter ' | awk '{print $2}')"

cat $DEPENDENCIES_FILE | awk -F '";"' '{print $1}' | while read line ; do
  name=${line}
  dependencie="$(echo "$all_used_dependencies" | grep $name)"
  if [ -z "$dependencie" ] ; then
    echo -e "\033[33m$name is not used but displayed\033[0m" 1>&2
    #exit 1
  fi
done
exit 0
