#!/bin/bash -e

blue="$(tput setaf 4)"
cyan="$(tput setaf 6)"
reset="$(tput sgr0)"

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$DIR/bash-yaml/yaml.sh"

create_variables ".gitlab-ci.yml"

OS="$(uname)"

echo "$blue# before script";
echo "$cyan$before_script$reset"
$before_script
echo

echo "$blue# $flutterDoctor_stage"
echo "## flutterDoctor"
echo "$cyan$flutterDoctor_script$reset"
$flutterDoctor_script
echo

echo "$blue# $generateAboutDependenciesAssets_stage"
echo "## $generateAboutDependenciesAssets_stage"
$generateAboutDependenciesAssets_script__
script/check_dependencies.sh
echo

echo "$blue# $dartAnalyzer_stage"
echo "## dartAnalyzer"
echo "$cyan$dartAnalyzer_script$reset"
$dartAnalyzer_script
echo

echo "## formatValidation"
echo "$cyan$formatValidation_script$reset"
$formatValidation_script
echo

echo "$blue# $unitTests_stage"
echo "## unit-test"
echo "$cyan$unitTests_script$reset"
$unitTests_script
echo

echo "$blue# $androidIntegrationTests_stage"
echo "## android integration-test"
echo "$cyan$androidIntegrationTests_script$reset"
ANDROID_ID="$(flutter devices | grep android | awk -F' • ' '{print $2}')"
[[ ! -z "$ANDROID_ID" ]] && $androidIntegrationTests_script || echo "There is no android emulator running"
echo

echo "$blue# $iOSIntegrationTests_stage"
echo "## iOS integration-test"
echo "$cyan$iOSIntegrationTests_script$reset"
iOS_ID="$(flutter devices | grep ios | awk -F' • ' '{print $2}')"
[[ ! -z "$iOS_ID" ]] && $iOSIntegrationTests_script || echo "There is no iOS emulator running"
echo

echo "$blue# $buildApk_stage"
echo "## buildApk"
echo "$cyan$buildApk_script$reset"
[[ "$OS" == "Linux" ]] && ($buildApk_script && (ls $buildApk_artifacts_paths
[[ 0 -eq $? ]] && echo || exit "App not build")) || echo "You are not operating on a linux machine"
echo

echo "$blue# $buildIpa_stage"
echo "## buildIpa"
echo "$cyan$buildIpa_script$reset"
[[ "$OS" == "Linux" ]] && echo "We can not build for iOS, you are running on a Linux machine" || ($buildIpa_script && (ls $buildIpa_artifacts_paths
[[ 0 -eq $? ]] && echo || exit "App not build"))
echo
