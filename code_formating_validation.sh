#!/bin/bash -e
for x in $(ls -d /usr/local/Cellar/node/*) ; do
  export PATH=$PATH:$x/bin
done
type eclint &>/dev/null || npm install -g eclint

target_files=""
# ./*.md excluding subfolders
target_files="${target_files} $(find . -maxdepth 1 -type f -name "*.md")"
# lib/
target_files="${target_files} $(find lib/ -type f -and -not -wholename '*/l10n/*' -and -not -name localization.dart -and -not -name tripShareView.dart -and -not -wholename '*/Api/*')"
# examples/
if [ -d examples ]; then
  target_files="${target_files} $(find examples/ -type f)"
fi
# test/
target_files="${target_files} $(find test/ -type f)"
# script/ excluding subfolders (to ignore script/bash-yaml/)
#target_files="${target_files} $(find script/ -maxdepth 1 -type f)"
# assets/*.json
target_files="${target_files} $(find assets/ -type f -name "*.json" -and -not -name convertcsv.json)"
eclint check ${target_files}
