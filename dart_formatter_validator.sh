#!/bin/bash

# NOTE: This script is intended to be used with the gitlab pipeline only, thus
# it is not included in the local_pipeline.sh script.
# Moreover on developers side a custom git hook exists to ensure only valid code
# has been commited, see README.md for more informations about this.

set -e

CURRENT_DIRECTORY="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)")"
PROJECT_ROOT_DIRECTORY="$(realpath "${CURRENT_DIRECTORY}/..")"
EDITOR_CONFIG="${PROJECT_ROOT_DIRECTORY}/.editorconfig"

trim()
{
  local trimmed=""

  read -r trimmed
  # Strip leading space.
  trimmed="${trimmed## }"
  # Strip trailing space.
  trimmed="${trimmed%% }"

  echo "${trimmed}"
}

DART_EDITOR_CONFIG="$(perl -00 -ne 'print if /\[\*.dart\]/' "${EDITOR_CONFIG}")"
NB_CHAR_PER_LINE="$(echo "${DART_EDITOR_CONFIG}" | grep max_line_length | cut -d'=' -f2 | trim)"

$(dirname $(which flutter))/cache/dart-sdk/bin/dartfmt \
  --dry-run \
  --set-exit-if-changed \
  --line-length "${NB_CHAR_PER_LINE}" \
  --fix-optional-new \
  "${PROJECT_ROOT_DIRECTORY}"
