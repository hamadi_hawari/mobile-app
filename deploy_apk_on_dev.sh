#!/bin/bash
set -e

d=$(date +%Y.%m.%d-%H.%M.%S)
curl -u $ARTIFACTORY_LOGIN:$ARTIFACTORY_PASSWORD -X PUT "$ARTIFACTORY_URL/artifactory/$ARTIFACTORY_REPO/$ARTIFACTORY_DEV_DIR/munic_io-dev-$d-apk-$CI_COMMIT_SHA.apk" -T build/app/outputs/apk/release/app-release.apk
