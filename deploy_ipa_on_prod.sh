#!/bin/bash
set -e

d=$(date +%Y.%m.%d-%H.%M.%S)
if [ "$CI_COMMIT_TAG" != "" ]; then
  curl -u $ARTIFACTORY_LOGIN:$ARTIFACTORY_PASSWORD -X PUT "$ARTIFACTORY_URL/artifactory/$ARTIFACTORY_REPO/$ARTIFACTORY_PROD_DIR/munic_io-$d-ipa-$CI_COMMIT_TAG.ipa" -T build/ios/app-release.ipa
fi
