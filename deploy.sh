#!/bin/bash
usage() {
  echo "$0 [tag] [ios|android]" 1>&2
  exit 1
}
[ $# -ne 2 ] && usage
export lane=''
case $1 in
  *beta*)
    export lane='beta'
    ;;
  *release*)
    export lane='release'
    ;;
  *)
    exit ;;
esac
case $2 in
  ios)
    cd ios
    FASTLANE_APPLE_APPLICATION_SPECIFIC_PASSWORD='nkvk-xrjw-svxh-bmee' bundle exec fastlane $lane
    ;;
  *)
    cd android
    bundle exec fastlane $lane
    ;;
esac
