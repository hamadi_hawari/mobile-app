#!/bin/bash
set -e

NB_KEEP=$NB_DEV_APK_RETAINED
#we get two lines by artifact in repo
let "NB_KEEP=NB_KEEP*2"
RESULTS=$(curl -s --header "X-Result-Detail: info" -X GET -u $ARTIFACTORY_LOGIN:$ARTIFACTORY_PASSWORD "$ARTIFACTORY_URL/artifactory/api/search/artifact?name=dev&repos=$ARTIFACTORY_REPO" | egrep "uri|created\"" | awk '{print $3}' | sed s'/.$//' | sed s'/.$//' | sed -E 's/^.{1}//')
echo "Results:\n$RESULTS"
SAVEIFS=$IFS
# Change IFS to new line.
IFS=$'\n'
RESULTS=($RESULTS)
# Restore IFS
IFS=$SAVEIFS

while [ "${#RESULTS[@]}" -ge "$NB_KEEP" ] ; do
  i="0"
  min=$(gdate +%s%3N)
  while [ "$i" -lt "${#RESULTS[*]}" ] ; do
    mod=$(($i%2))
    if [ "$mod" -eq "0" ] ; then
      created=$(gdate -d ${RESULTS[$i]} +%s%3N)
      if [ "$created" -lt "$min" ] ; then
        min="$created"
        let "min_index=i+1"
        echo "min: $min"
        echo "min_index: $min_index"
      fi
    fi
    let "i=i+1"
  done
  echo "deleting path ${RESULTS[$min_index]}"
  curl -X DELETE -u $ARTIFACTORY_LOGIN:$ARTIFACTORY_PASSWORD ${RESULTS[$min_index]}
  RESULTS=$(curl -s --header "X-Result-Detail: info" -X GET -u $ARTIFACTORY_LOGIN:$ARTIFACTORY_PASSWORD "$ARTIFACTORY_URL/artifactory/api/search/artifact?name=dev&repos=$ARTIFACTORY_REPO" | egrep "uri|created\"" | awk '{print $3}' | sed s'/.$//' | sed s'/.$//' | sed -E 's/^.{1}//')
  SAVEIFS=$IFS
  # Change IFS to new line.
  IFS=$'\n'
  RESULTS=($RESULTS)
  # Restore IFS
  IFS=$SAVEIFS
done
exit 0
