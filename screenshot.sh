#!/bin/bash
usage() {
  echo "$0 [ios|android]" 1>&2
  exit 1
}
[ $# -ne 1 ] && usage
export lane=''
case $1 in
  ios)
    cd ios
    echo "Think of: fastlane update_fastlane"
    echo "Think of: fastlane snapshot update"
    SNAPSHOT_FORCE_DELETE=1 fastlane snapshot reset_simulators
    (cd .. ; ./script/build.sh debug ios)
    fastlane screenshot
    ;;
  *)
    cd android
    (cd .. ; ./script/build.sh debug apk)
    fastlane screenshot
    ;;
esac
