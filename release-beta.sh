#!/bin/bash
git fetch --prune --all --tags
type gsed &>/dev/null && alias sed=gsed
currentVersion=$(git describe | grep --color=no beta | sed 's@-beta.*$@-beta@')
version=$(awk '{
  match($0, /\.([0-9]+)([^0-9]*)$/, a);
  gsub(/\.([0-9]+)[^0-9]*-beta$/,"." a[1]+1 a[2]);
  print;
}' <<< $currentVersion)
echo "From $currentVersion to $version"
git clean -xdf .
git tag -a $version -m $version
git push origin develop --tags
(cd android ; bundle install)
(cd ios ; bundle install)
#(cd ios ; bundle exec fastlane run update_fastlane)
#(cd ios ; bundle exec fastlane snapshot update)
./script/build.sh prod ios
./script/deploy.sh $version ios
./script/build.sh prod apk
./script/deploy.sh $version apk
git co ios/Runner/Assets.xcassets
