#!/bin/bash

shopt -s expand_aliases

JSON_FILE="./assets/dependencies/dependencies.json"
APP="./assets/dependencies/munic_app.csv"
FRAMEWORK="./assets/dependencies/framework.csv"
FRAMEWORK_DEPENDENCIES="./assets/dependencies/framework_dependencies.csv"
OTHER_DEPENDENCIES="./assets/dependencies/other_dependencies.csv"

function command_exists()
{
  command -v "${1}" &>/dev/null
}

if command_exists gsed; then
  alias sed='gsed'
fi

rm -f $JSON_FILE
echo "[" >> $JSON_FILE

# Munic App
while read -u3 LINE; do
  NAME="$(awk -F';' '{print $1}' <<< "$LINE")"
  DESCRIPTION="$(awk -F';' '{print $2}' <<< "$LINE")"
  LICENSE="$(awk -F';' '{print $3}' <<< "$LINE")"
  FILEPATH="$(awk -F';' '{print $4}' <<< "$LINE")"
  VERSION="\""
  VERSION+="$(git describe 2>/dev/null || git rev-parse --short HEAD)"
  VERSION+="\""

  cat >> $JSON_FILE <<EOF
  {
    "name": $NAME,
    "description": $DESCRIPTION,
    "version": $VERSION,
    "license": $LICENSE,
    "path": $FILEPATH
  },
EOF
done 3<$APP

# Flutter and Dart SDK
while read -u3 LINE; do
  [ "${LINE[0]}" = '#' ] && continue

  NAME="$(awk -F';' '{print $1}' <<< "$LINE")"
  DESCRIPTION="$(awk -F';' '{print $2}' <<< "$LINE")"
  VAR="$(echo $NAME | cut -d "\"" -f 2)"
  FILEPATH="$(awk -F';' '{print $4}' <<< "$LINE")"
  VERSION="\""
  if [[ "$NAME" =~ Dart ]] ; then
    VERSION+="$(flutter --version | grep Dart | awk '{print $4;}')"
  else
    VERSION+="$(flutter --version | grep Flutter | awk '{print $2;}')"
  fi
  VERSION+="\""
  new_license='assets/dependencies/licenses/'${VAR// /_}'.txt'
  curl $FILEPATH -o $new_license

  cat >> $JSON_FILE <<EOF
  {
    "name": $NAME,
    "description": $DESCRIPTION,
    "version": $VERSION,
    "license": "BSD",
    "path": "$new_license"
  },
EOF
done 3<$FRAMEWORK

FLUTTER_PATH=$(which flutter | sed 's@/bin/flutter$@@')
FLUTTER_CACHE="$FLUTTER_PATH/.pub-cache/hosted/pub.dartlang.org/"
getDescription() {
  local descfile=$1/README.md
  if [ -e $descfile ] ; then
    sed ' /^[ \t]*[!<#\[]/d ; /^[ \t]*$/d ; s@([^)]*)@@ ; s@\[\([^]]*\)\]@\1@ ; s@<[^>]*>@@ ; s@!@@' $descfile | head -n 1
  fi
}
getlicenseType() {
  local license=$1/LICENSE
  if [ ! -e $license ] ; then
    return
  elif grep -q \ Apache $license ; then
    echo Apache
  elif grep -q \ MIT $license ; then
    echo MIT
  elif grep -q \ BSD $license ; then
    echo BSD
  else
    echo BSD
  fi
}
for name in $(<$FRAMEWORK_DEPENDENCIES) ; do
  version="$(flutter pub pub deps -s compact | grep -v SDK | sed '/dev dependencies/q' | grep -v "dev dependencies:" | grep -- "- $name " | awk -F ' ' '{print $3}')"
  licenseType=$(getlicenseType $FLUTTER_CACHE/${name}-$version)
  if [ "$licenseType" != '' ] ; then
    new_license=assets/dependencies/licenses/$name.txt
    cp -f $FLUTTER_CACHE/${name}-$version/LICENSE $new_license
    description=$(getDescription $FLUTTER_CACHE/${name}-$version)
    [ ${#description} -lt 5 ] && description=""
    cat >> $JSON_FILE <<EOF
  {
    "name": "$name",
    "description": "$description",
    "version": "$version",
    "license": "$licenseType",
    "path": "$new_license"
  },
EOF
  fi
done

# other dependencies
while read -u3 LINE; do
 NAME="$(awk -F';' '{print $1}' <<< "$LINE")"
 DESCRIPTION="$(awk -F';' '{print $2}' <<< "$LINE")"
 LICENSE="$(awk -F';' '{print $3}' <<< "$LINE")"
 FILEPATH="$(awk -F';' '{print $4}' <<< "$LINE")"
 VERSION="$(awk -F';' '{print $5}' <<< "$LINE")"

  cat >> $JSON_FILE <<EOF
  {
    "name": $NAME,
    "description": $DESCRIPTION,
    "version": $VERSION,
    "license": $LICENSE,
    "path": $FILEPATH
  },
EOF
done 3<$OTHER_DEPENDENCIES

echo "]" >> $JSON_FILE

NB_OF_LINES="$(cat $JSON_FILE | wc -l)"
CHANGE="$(expr $NB_OF_LINES - 1 )"
sed -i "$(expr $CHANGE)s/},/}/" $JSON_FILE
