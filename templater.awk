#!/usr/bin/env awk
function err (text) {
        print text > "/dev/stderr"
}
BEGIN {
  if (ARGC != 3) {
    err("Error: not enough arguments.")
    err("")
    err("Usage: ./templater <content_file> <template_file>")
    err("Aborting.")
    exit 1
  }

  if (length(ENVIRON["PATTERN"]) == 0) {
    err("Error: no pattern specified.")
    err("")
    err("Specify a pattern via the `PATTERN` environment variable.")
    err("For example: ")
    err("  PATTERN=__CONTENT__ templater contents.txt template.txt")
    err("Aborting.")
    exit 1
  }
}
NR==FNR {
  content_lines[n++]=$0;
  next;
}
$0 ~ ENVIRON["PATTERN"] {
  for (i = 0; i < n; i++) {
    print content_lines[i]
  }
  next
}
1
