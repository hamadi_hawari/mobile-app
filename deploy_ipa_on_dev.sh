#!/bin/bash
set -e

d=$(date +%Y.%m.%d-%H.%M.%S)
curl -u $ARTIFACTORY_LOGIN:$ARTIFACTORY_PASSWORD -X PUT "$ARTIFACTORY_URL/artifactory/$ARTIFACTORY_REPO/$ARTIFACTORY_DEV_DIR/munic_io-dev-$d-ipa-$CI_COMMIT_SHA.ipa" -T build/ios/app-release.ipa
