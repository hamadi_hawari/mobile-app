#!/bin/bash
set -e

usage() {
  echo "$0 [debug|prod] [ios|android]" 1>&2
  exit 1
}
[ $# -ne 2 ] && usage
flutter packages get
[ ! -e assets/dependencies/dependencies.json ] && ./script/generate_json_assets_about.sh
export PATH="$PATH":"$HOME/.pub-cache/bin"
! type protoc-gen-dart &>/dev/null && pub global activate protoc_plugin
CHMOD=chmod
type gchmod &>/dev/null && export CHMOD=gchmod
$CHMOD -R 755 build &>/dev/null
if [ ! -e lib/tabPages/municIOPage/Message.pb.dart ] ; then
  SED=sed
  type gsed &>/dev/null && export SED=gsed
  git submodule update --init --recursive
  (cd dataScientistLogger/data/extra/proto/ ; protoc --dart_out=../../../../lib/tabPages/municIOPage/ Message.proto)
  $SED -i 's@// ignore_for_file: .*@\0,unnecessary_const@' lib/tabPages/municIOPage/message.pbjson.dart
  $SED -i 's@// ignore_for_file: .*@\0,annotate_overrides,sort_unnamed_constructors_first,avoid_as@' lib/tabPages/municIOPage/message.pb.dart
fi
[ ! -e lib/localization.dart ] && ./script/localization.sh
main=lib/mainProdEnvironment.dart
case $1 in
  debug)
    main=lib/mainDevEnvironment.dart
    ;;
  *)
    ;;
esac

version=$(git describe | grep --color=no beta | sed 's@v@@ ; s@-beta.*$@@')
case $2 in
  ios)
    if [ "$(md5sum pubspec.lock | cut -f 1 -d \ )" != "$(< ios/pod.md5)" ] ; then
      (cd ios ; pod install)
      md5sum pubspec.lock | cut -f 1 -d \  > ios/pod.md5
    fi
    flutter build ios --no-codesign -t $main --build-name ${version} --build-number "$(date +'%y%m%d%H%M')"
    mkdir -p build/ios/Payload
    cp -r build/ios/iphoneos/Runner.app build/ios/Payload/
    (cd build/ios/ ; zip -r app-release.ipa Payload)
    mkdir -p output/ios/
    if [ "$1" = 'debug' ] ; then
      mv build/ios/app-release.ipa output/ios/app-debug.ipa
    else
      mv build/ios/app-release.ipa output/ios/
    fi
    ;;
  *)
    flutter build appbundle -t $main --build-name ${version} --build-number "$(date +'%y%m%d%H%M')" --target-platform android-arm,android-arm64
    mkdir -p output/bundle/
    if [ "$1" = 'debug' ] ; then
      mv build/app/outputs/bundle/release/app.aab output/bundle/app-debug.aab
    else
      mv build/app/outputs/bundle/release/app.aab output/bundle/app-release.aab
    fi
    ;;
esac
