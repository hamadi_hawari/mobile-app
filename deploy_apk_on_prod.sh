#!/bin/bash
set -e

d=$(date +%Y.%m.%d-%H.%M.%S)
if [ "$CI_COMMIT_TAG" != "" ]; then
  curl -u $ARTIFACTORY_LOGIN:$ARTIFACTORY_PASSWORD -X PUT "$ARTIFACTORY_URL/artifactory/$ARTIFACTORY_REPO/$ARTIFACTORY_PROD_DIR/munic_io-$d-apk-$CI_COMMIT_TAG.apk" -T build/app/outputs/apk/release/app-release.apk
fi
