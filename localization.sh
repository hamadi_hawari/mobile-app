#!/bin/bash
rm -rf lib/l10n getters.txt
mkdir -p lib/l10n
# Generate arb - should handle xliff instead of csv
count=0
language=( en )
function command_exists()
{
  command -v "${1}" &>/dev/null
}

if command_exists gsed; then
  alias sed='gsed'
fi

f() {
  shift
  shift
  echo ${@}
}
trap -- 'rm -f getters.txt localization.dart' EXIT
echo "  // ignore_for_file: prefer_single_quotes" > getters.txt
while read line ; do
  oldifs=$IFS
  IFS=';'
  array=($line)
  IFS=$oldifs
  if [ $count -eq 0 ] ; then
    export language=($(f ${array[@]}))
    for lang in messages "${language[@]}" ; do
      echo '{"title": "munic.io", "@@last_modified": "'$(date +'%Y-%m-%dT%H:%M:%S.%s')'", "@@locale": "'$lang'"' >> lib/l10n/intl_$lang.arb
    done
    let count=1
    continue
  fi
  i=1
  for lang in messages "${language[@]}" ; do
    echo ", \"${array[1]}\":\"${array[$i]}\", \"@${array[1]}\""': { "type": "text", "placeholders": {} }' >> lib/l10n/intl_$lang.arb
    let i=$i+1
  done
  echo "  String get "${array[1]}" { return Intl.message(\""${array[2]}"\", name:'"${array[1]}"') ; }" >> getters.txt
  let count=$count+1
done < localization.csv
for lang in messages "${language[@]}" ; do
  echo '}' >> lib/l10n/intl_$lang.arb
done
getLanguages() {
  local first=true
  for x in "${@}" ; do
    [ "$first" = true ] && first=false || echo -n ', '
    echo -n "\"$x\""
    [[ "$x" =~ _ ]] && echo -n ", \"${x%_*}\""
  done
}
sed 's@__LANGUAGES__@'"$(getLanguages "${language[@]}")"'@' script/localization.dart.tmpl > lib/localization.dart
# Create getters
PATTERN='//__GETTERS__' awk -f script/templater.awk getters.txt lib/localization.dart > localization.dart
mv -f localization.dart lib/
# Create dart files
flutter pub pub run intl_translation:generate_from_arb --output-dir=lib/l10n --no-use-deferred-loading lib/localization.dart $(find lib/l10n -name intl_*.arb)
dartfmt --overwrite --line-length 120 --fix-optional-new lib/l10n
